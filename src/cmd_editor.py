from subprocess import Popen, PIPE, check_call, CalledProcessError
import subprocess
from dialog import Dialog

import pygtk
import pango
import subprocess
pygtk.require('2.0')
import gtk


class CmdEditor():

    def exit(self, widget, data=None):
        self.window.destroy()

    def verify_and_save(self, widget):
        if self.verify(widget):
            start, end = self.tBuffer.get_bounds()
            self.cmd = self.tBuffer.get_text(start, end, False)
            md = Dialog()
            md.on_info("Command saved!")

    def verify(self, widget):
        start, end = self.tBuffer.get_bounds()
        to_verify = self.tBuffer.get_text(start, end, False)

        args = to_verify.split()

        for index, arg in enumerate(args):
            if arg == "$dataset":
                args[index] = "data/test.nii.gz"
            elif arg == "$prefix_nii":
                args[index] = "ppp.nii.gz"
            elif arg == "$prefix_b+h":
                args[index] = "ppp+orig"
            elif arg == "$dataset_directory":
                args[index] = "data/"
            elif arg == "$output_directory":
                args[index] = "./"
            elif arg == "$output_filename":
                args[index] = "ppp"
        try:
            process = Popen(args, stdout=PIPE, stderr=PIPE)
            out, err = Popen.communicate(process)
            if process.returncode != 0 or "ERROR" in err:
                raise AttributeError
            subprocess.call("rm *.nii* *.json *BRIK *HEAD", shell=True)
            md = Dialog()
            md.on_info("Command verified!\n\nOutput Message:\n" + err)
            self.tView.modify_bg(
                gtk.STATE_NORMAL, gtk.gdk.color_parse("#a2e46c"))
            return True
        except OSError as os:
            md = Dialog()
            md.on_warn("OSError: No such file or directory.\n\nCheck to " +
                "ensure command is entered correctly and referenced library " +
                "functions are installed on your machine.")
            self.tView.modify_bg(
                gtk.STATE_NORMAL, gtk.gdk.color_parse("#dd1326"))
            return False
        except ValueError as ve:
            md = Dialog()
            md.on_warn(
                "Value Error: Non-zero return code.\n\nError Message:\n" + err)
            self.tView.modify_bg(
                gtk.STATE_NORMAL, gtk.gdk.color_parse("#dd1326"))
            return False
        except AttributeError as e:
            md = Dialog()
            md.on_warn(
                "AttributeError: Attribute not identified.\n\nError Message:\n" + err)
            self.tView.modify_bg(
                gtk.STATE_NORMAL, gtk.gdk.color_parse("#dd1326"))
            return False
        except CalledProcessError as e:
            md = Dialog()
            md.on_warn(
                "CalledProcessError: Non-zero return code.\n\nError Message:\n" + err)
            self.tView.modify_bg(
                gtk.STATE_NORMAL, gtk.gdk.color_parse("#dd1326"))
            return False

    def __init__(self, cmd):

        self.window = gtk.Window()
        self.window.connect("delete_event", self.exit)
        self.window.set_title('Command Editor')
        self.window.set_size_request(600, 200)
        self.window.set_border_width(0)
        self.window.set_icon_from_file("img/epitome_icon.png")
        self.cmd = ""
        main_vbox = gtk.VBox(False, 1)
        hbox = gtk.HBox(True, 0)
        toolbar = gtk.Toolbar()
        toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
        toolbar.set_style(gtk.TOOLBAR_ICONS)

        save = gtk.ToolButton(gtk.STOCK_SAVE)
        save.connect("clicked", self.verify_and_save)
        save.set_tooltip_markup(
            '<b>Verify &amp; Save: </b> Verify entered command, save if valid.')
        toolbar.insert(save, 0)

        verify = gtk.ToolButton(gtk.STOCK_APPLY)
        verify.connect("clicked", self.verify)
        verify.set_tooltip_markup(
            '<b>Verify: </b> Verify input command is valid.')
        toolbar.insert(verify, 1)

        space = gtk.SeparatorToolItem()
        space.set_draw(False)
        space.set_expand(True)
        toolbar.insert(space, 2)

        exit = gtk.ToolButton(gtk.STOCK_QUIT)
        exit.connect("clicked", self.exit)
        exit.set_tooltip_markup('<b>Close: </b> Close editor window')
        toolbar.insert(exit, 3)

        tMenu = gtk.Menu()
        afni_menu = gtk.Menu()
        conversion_menu = gtk.Menu()
        
        afni_menu_item = gtk.MenuItem("AFNI")
        afni_menu_item.set_submenu(afni_menu)
        conversion_menu_item = gtk.MenuItem("Conversion")
        conversion_menu_item.set_submenu(conversion_menu)
        
        afni_menu_item.show()
        conversion_menu_item.show()
        
        tMenu.append(afni_menu_item)
        tMenu.append(conversion_menu_item)
        
        # AFNI menu
        
        dataset = gtk.MenuItem("$dataset")
        prefix_nii = gtk.MenuItem("$prefix__nii")
        prefix_bh = gtk.MenuItem("$prefix__b+h")
                
        dataset.connect("activate", self.on_menu_item_activated)
        prefix_nii.connect("activate", self.on_menu_item_activated)
        prefix_bh.connect("activate", self.on_menu_item_activated)
        
        dataset.show()
        prefix_nii.show()
        prefix_bh.show()
        
        afni_menu.append(dataset)
        afni_menu.append(prefix_nii)
        afni_menu.append(prefix_bh)
        
        # Conversion menu
        
        dataset_directory = gtk.MenuItem("$dataset__directory")
        output_directory = gtk.MenuItem("$output__directory")
        output_filename = gtk.MenuItem("$output__filename")
        
        dataset_directory.connect("activate", self.on_menu_item_activated)
        output_directory.connect("activate", self.on_menu_item_activated)
        output_filename.connect("activate", self.on_menu_item_activated)
        
        dataset_directory.show()
        output_directory.show()
        output_filename.show()
        
        conversion_menu.append(dataset_directory)
        conversion_menu.append(output_directory)
        conversion_menu.append(output_filename)

        self.tBuffer = gtk.TextBuffer(None)
        swH = gtk.ScrolledWindow()
        swH.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.tBuffer.set_text(cmd)
        self.tView = gtk.TextView(self.tBuffer)
        self.tView.set_border_width(5)
        self.tView.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        self.tView.modify_font(pango.FontDescription("Monospace 10"))
        self.tView.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.tView.connect("key-press-event", self.entry_completion, tMenu)
        swH.add(self.tView)
        hbox.pack_start(toolbar, False, True, 0)
        main_vbox.pack_start(hbox, False, True, 0)
        main_vbox.pack_start(swH, True, True, 0)

        self.window.add(main_vbox)

        self.window.show_all()

    def entry_completion(self, tView, event, menu):
        if event.keyval == 36:
            completion_menu = self.build_completion_menu(
                menu, event.get_time())

    def build_completion_menu(self, menu, event_time):
        menu.popup(None, None, self.set_menu_position, 0, event_time)

    def set_menu_position(self, menu, data=None):
        iterr = self.tBuffer.get_iter_at_offset(
            self.tBuffer.props.cursor_position)
        rectangle = self.tView.get_iter_location(iterr)
        win_x, win_y = self.window.get_position()
        bw = self.tView.get_border_width()
        return (win_x + rectangle.x + bw, win_y + 86 + rectangle.y + bw, True)

    def on_menu_item_activated(self, menu_item, data=None):
        self.tBuffer.insert_at_cursor(menu_item.get_label().replace("$", "").replace("__", "_"))

