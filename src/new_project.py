#!/usr/bin/env python

import pygtk
import gtk
import pickle
import os
import pwd
import subprocess
import gobject
import glib
import time
import datetime
pygtk.require('2.0')

import sys
sys.path += ['src']

from lib.Project import Project
from util.db import DB
from project_editor import ProjectEditor
from dialog import Dialog

import pdb

gobject.threads_init()


class NewProject():

    def delete_event(self, widget, data=None):
        self.window.destroy()

    def on_warn(self, message):
        md = gtk.MessageDialog( parent=None,
                                flags=gtk.DIALOG_DESTROY_WITH_PARENT,
                                type=gtk.MESSAGE_WARNING,
                                buttons=gtk.BUTTONS_OK,
                                message_format=message)
        self.warn_response = md.run()
        md.destroy()

    def on_info(self, message):
        md = gtk.MessageDialog( parent=None,
                                flags=gtk.DIALOG_DESTROY_WITH_PARENT,
                                type=gtk.MESSAGE_INFO,
                                buttons=gtk.BUTTONS_OK,
                                message_format=message)
        md.run()
        md.destroy()

    def get_children(self, root_iter):
        children = []
        for i in range(0, self.treestore.iter_n_children(root_iter)):
            children.append(self.treestore.iter_nth_child(root_iter, i))
        return children

    def get_destination(self, file_iter):
        #pdb.set_trace()
        path = self.treestore.get_path(file_iter)
        if self.project_directory is not None:
            filepath = self.project_directory + "/"
            current_path = ()
            index = 0
            if len(path) == 5:
                for i in path:
                    current_path += (i,)
                    current_iter = self.treestore.get_iter(current_path)
                    filepath += self.treestore.get_value(current_iter, 0)
                    index += 1
                return filepath
        else:
            md = Dialog()
            md.on_warn("Please specify project directory.")

    def select_project_directory(self, widget):
        dialog = gtk.FileChooserDialog("Select Project Directory",
                                       None, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                       (gtk.STOCK_CANCEL,
                                        gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN,
                                        gtk.RESPONSE_OK))
        dialog.set_default_response(gtk.RESPONSE_OK)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.project_directory = dialog.get_filename()
            self.directory_entry.set_text(self.project_directory)
            root = self.project_directory.split("/")
            root = root[len(root) - 1]
            self.set_project_root(root)
        dialog.destroy()

    def set_project_root(self, root):
        self.project_root = root
        self.project_root_entry.set_text(root)
    
    def set_project_name(self, widget, data=None):
        self.project_name = self.project_name_entry.get_text()
        if self.treestore.get_iter_root() is None:
            self.project_name_iter = self.treestore.insert(
                None, 0, [self.project_name, None, None, True, True])
        else:
            self.treestore.set(self.project_name_iter, 0,
                               self.project_name + "/")
        self.treeview.expand_all()

    def add_group(self, widget):
        group_name = self.group.get_text()
        error = 0
        if self.project_name_iter is None:
            self.on_warn("Please enter a Project Name before creating groups.")
        elif group_name == "":
            self.on_warn("Please enter a group name.")
        else:
            children = self.get_children(self.project_name_iter)
            for child in children:
                ''' No groups can have the same name. As soon as we find a match, break
                from loop and throw warning message
                '''
                if self.treestore.get_value(child, 0).replace("/", "") == group_name:
                    error = 1
                    break
            if error == 0:
                self.group_iter = self.treestore.append(
                    self.project_name_iter, [group_name + "/", None, None, True, True])
            else:
                self.on_warn("Group names must be unique.")
        self.treeview.expand_all()

    def remove_group(self, widget):
        self.treeselection = self.treeview.get_selection()
        if self.treeselection.count_selected_rows() > 0:  # ensure user has made a selection
            self.treeselection.set_mode(gtk.SELECTION_SINGLE)
            (model, iter) = self.treeselection.get_selected()
            path = self.treestore.get_path(iter)
            if len(path) != 2:              # ensure user has selected a group
                self.on_warn("Please ensure you have selected a group.")
            else:
                if iter is not None:
                    self.treestore.remove(iter)
                    self.treeview.expand_all()
        else:
            self.on_warn("Please select the group you wish to delete.")

    def update_participants(self, widget):
        no_of_participants = self.no_of_participants.get_text()
        starting_index = self.starting_index.get_text()
        if no_of_participants == "":
            self.on_warn("Please enter the number of participants " + 
                "you would like to add.")
        elif starting_index == "":
            self.on_warn("Please enter starting index.")
        elif not no_of_participants.isdigit():
            self.on_warn("Please enter numbers only.")
        elif int(no_of_participants) > 500:
            self.on_warn("A group may not exceed 500 participants.")
        else:
            no_of_participants = int(no_of_participants)
            starting_index = int(starting_index)
            if self.APPLY_TO_ALL == 1:
                self.treestore.foreach(self.update_participant_iters, 
                    (no_of_participants, starting_index))
                self.treeview.expand_all()
            elif self.APPLY_TO_ALL == 0:
                self.treeselection = self.treeview.get_selection()
                if self.treeselection.count_selected_rows() > 0:
                    self.treeselection.set_mode(gtk.SELECTION_SINGLE)
                    model, iterr = self.treeselection.get_selected()
                    path = self.treestore.get_path(iterr)
                    if len(path) != 2:
                        self.on_warn("Please ensure you have " +
                        "selected a group.")
                    else:
                        no_of_children = self.treestore.iter_n_children(iterr)
                        children = self.get_children(iterr)
                        for i in children:
                            self.treestore.remove(i)
                        for i in range(starting_index, no_of_participants + starting_index):
                            self.treestore.append(iterr, [str(i).zfill(3) + "/", None, None, True, True])
                        self.treeview.expand_all()
                else:
                    self.on_warn(
                        "Please select a group to which you " +
                        "would like to add participants.")

    def update_participant_iters(self, model, path, iterr, data):
        no_of_participants = data[0]
        starting_index = data[1]
        if len(path) == 2:
            children = self.get_children(iterr)
            for i in children:
                self.treestore.remove(i)
            for i in range(starting_index, no_of_participants + starting_index):
                self.treestore.append(iterr, [str(i).zfill(3) + "/", None, None, True, True])

    def add_task(self, widget):
        task = self.task.get_text()
        error = 0
        flag = 0
        if task == "":
            self.on_warn("Please enter a task name.")
        else:
            if self.APPLY_TO_ALL == 1:
                # array of all group iters
                children = self.get_children(self.project_name_iter)
                for child in children:
                    # array of all participant iters
                    grandchildren = self.get_children(child)
                    for grandchild in grandchildren:
                        error = 0
                        greatgrandchildren = self.get_children(grandchild)
                        for greatgrandchild in greatgrandchildren:
                            if task == self.treestore.get_value(greatgrandchild, 0).replace("/", ""):
                                error = 1
                        if error == 0:
                            flag = 1                    # if we error at every greatgrandchild print warning
                            self.next_iter = self.treestore.append(
                                grandchild, [task + "/", None, None, True, True])
                    self.treeview.expand_all()
                if flag == 0:
                    self.on_warn("Task names must be unique.")

            elif self.APPLY_TO_ALL == 0:
                self.treeselection = self.treeview.get_selection()
                if self.treeselection.count_selected_rows() > 0:
                    self.treeselection.set_mode(gtk.SELECTION_SINGLE)
                    (model, iter) = self.treeselection.get_selected()
                    path = self.treestore.get_path(iter)
                    if len(path) != 3:
                        self.on_warn(
                            "Please select the participant for which you wish to add a task.")
                    else:
                        greatgrandchildren = self.get_children(iter)
                        for greatgrandchild in greatgrandchildren:
                            if task == self.treestore.get_value(greatgrandchild, 0).replace("/", ""):
                                error = 1
                                break
                        if error == 0:
                            self.next_iter = self.treestore.append(
                                iter, [task + "/", None, None, True, True])
                        else:
                            self.on_warn("Task names must be unique.")
                    self.treeview.expand_all()
                else:
                    self.on_warn(
                        "Please select the group to which you wish to add a task.")

    def remove_task(self, widget):
        task = self.task.get_text()
        error = 0
        if task == "":
            self.on_warn("Please enter a task name.")
        else:
            if self.APPLY_TO_ALL == 1:
                error = 0
                # array of all group iters
                children = self.get_children(self.project_name_iter)
                for child in children:
                    # array of all participant iters
                    grandchildren = self.get_children(child)
                    for grandchild in grandchildren:
                        greatgrandchildren = self.get_children(grandchild)
                        for greatgrandchild in greatgrandchildren:
                            if task == self.treestore.get_value(greatgrandchild, 0).replace("/", ""):
                                self.treestore.remove(greatgrandchild)
                                error = 1
                    self.treeview.expand_all()
                if error == 0:
                    self.on_warn(
                        "Please ensure task exists prior to deletion.")
            elif self.APPLY_TO_ALL == 0:
                self.treeselection = self.treeview.get_selection()
                if self.treeselection.count_selected_rows() > 0:
                    self.treeselection.set_mode(gtk.SELECTION_SINGLE)
                    (model, iter) = self.treeselection.get_selected()
                    self.treestore.remove(iter)
                    self.treeview.expand_all()
    
    def update_run(self, widget):
        no_of_runs = self.run.get_text()
        if no_of_runs == "":
            self.on_warn("Please enter the number of runs " + 
                "you would like to add.")
        elif not no_of_runs.isdigit():
            self.on_warn("Please enter numbers only.")
        elif int(no_of_runs) >= 50:
            self.on_warn("A task may not exceed 50 runs.")
        else:
            no_of_runs = int(no_of_runs)
            if self.APPLY_TO_ALL == 1:
                self.treestore.foreach(self.update_run_iters, no_of_runs)
                self.treeview.expand_all()
            elif self.APPLY_TO_ALL == 0:
                self.treeselection = self.treeview.get_selection()
                if self.treeselection.count_selected_rows() > 0:
                    self.treeselection.set_mode(gtk.SELECTION_SINGLE)
                    model, iterr = self.treeselection.get_selected()
                    path = self.treestore.get_path(iterr)
                    if len(path) != 4:
                        self.on_warn("Please ensure you have " +
                        "selected a task.")
                    else:
                        no_of_children = self.treestore.iter_n_children(iterr)
                        if no_of_runs > no_of_children:
                            for i in range(self.treestore.iter_n_children(iterr) + 1, no_of_runs + 1):
                                self.treestore.append(iterr, ["RUN" + str(i).zfill(2) + "/", None, None, True, True])
                        elif no_of_runs < no_of_children:
                            for iteer in self.get_children(iterr):
                                 if int(self.treestore.get_value(iteer, 0).replace("RUN","")) > no_of_runs:
                                    self.treestore.remove(iteer)
                        self.treeview.expand_all()
                else:
                    self.on_warn(
                        "Please select a task to which you " +
                        "would like to add runs.")

    def update_run_iters(self, model, path, iterr, no_of_runs):
        if len(path) == 4 and no_of_runs > self.treestore.iter_n_children(iterr):
            for i in range(self.treestore.iter_n_children(iterr) + 1, no_of_runs + 1):
                self.treestore.append(iterr, ["RUN" + str(i).zfill(2) + "/", None, None, True, True])
        elif len(path) == 5 and no_of_runs < self.treestore.iter_n_children(self.treestore.iter_parent(iterr)):
            for iterr in self.get_children(self.treestore.iter_parent(iterr)):
                 if int(self.treestore.get_value(iterr, 0).replace("RUN","")) > no_of_runs:
                    self.treestore.remove(iterr)

    def select_data_clicked(self, widget, data=None):
        # Ensure directory structure has been created
        if self.has_created_directory_structure() == 1:
            self.treeselection = self.treeview.get_selection()  # get selection
            # allow only for single selections
            self.treeselection.set_mode(gtk.SELECTION_SINGLE)
            (model, iter) = self.treeselection.get_selected()
            dialog = gtk.FileChooserDialog("Open Project...", None,
                                           gtk.FILE_CHOOSER_ACTION_OPEN,
                                           (gtk.STOCK_CANCEL,
                                            gtk.RESPONSE_CANCEL,
                                            gtk.STOCK_OPEN,
                                            gtk.RESPONSE_OK))
            if self.last_opened_location is not None:
                dialog.set_current_folder_file(self.last_opened_location)
            dialog.set_default_response(gtk.RESPONSE_OK)
            dialog.set_select_multiple(True) 
            
            filter = gtk.FileFilter()
            filter.set_name("All files")
            filter.add_pattern("*")
            dialog.add_filter(filter)
            """
            filter = gtk.FileFilter()
            filter.set_name("DICOM")
            filter.add_pattern("*.dcm")
            dialog.add_filter(filter)
            """
            response = dialog.run()
            if response == gtk.RESPONSE_OK:
                filenames = dialog.get_filenames()
                self.last_opened_location = dialog.get_current_folder_file()
                for filepath in filenames:
                    myfile = filepath.split("/")
                    myfile = myfile[len(myfile) - 1]
                    self.next_iter = self.treestore.append(
                        iter, [myfile, filepath, self.get_destination(iter), True, True])
                    self.treeview.expand_all()
            dialog.destroy()
        else:
            self.on_warn(
                "Please complete construction of directory structure prior to importing data.")

    def select_batch_clicked(self, widget, data=None):
        # Ensure directory structure has been created
        if self.has_created_directory_structure() == 1:

            # Create a new window
            self.batch_window = gtk.Window()
            self.batch_window.connect("delete_event", self.close_batch_window)
            self.batch_window.set_title("Batch Import Wizard")
            self.batch_window.set_geometry_hints(self.batch_window,
                                                 min_width=400,
                                                 min_height=-1,
                                                 max_width=-1,
                                                 max_height=-1,
                                                 base_width=-1,
                                                 base_height=-1,
                                                 width_inc=-1,
                                                 height_inc=-1,
                                                 min_aspect=-1.0,
                                                 max_aspect=-1.0)
            self.batch_window.set_border_width(10)
            self.batch_window.set_icon_from_file("img/epitome_icon.png")

            self.batch_vbox = gtk.VBox(False, 1)
            self.label = gtk.Label("Step 1: Select root directory")
            self.label.set_alignment(xalign=0, yalign=0.5)
            self.batch_vbox.pack_start(self.label, True, True, 2)

            description = "Please select the root directory from which you wish to import files."
            self.label = gtk.Label(description)
            self.label.set_alignment(xalign=0, yalign=0.5)
            self.batch_vbox.pack_start(self.label, True, True, 4)

            children = self.get_children(self.project_name_iter)
            description = "This directory should contain group folders corresponding to:"
            self.label = gtk.Label(description)
            self.label.set_alignment(xalign=0, yalign=0.5)
            self.batch_vbox.pack_start(self.label, True, True, 0)
            description = ""
            i = 0
            for child in children:
                if i > 2:
                    break
                elif i == 2 and len(children) > 2:
                    description += self.treestore.get_value(
                        child, 0).replace("/", "") + "..."
                elif i == 2 or i == len(children) - 1:
                    description += self.treestore.get_value(
                        child, 0).replace("/", "")
                else:
                    description += self.treestore.get_value(
                        child, 0).replace("/", "") + ", "
                i += 1

            self.label = gtk.Label(description)
            self.label.set_alignment(xalign=0, yalign=0.5)
            self.batch_vbox.pack_start(self.label, True, True, 0)

            self.batch_directory_hbox = gtk.HBox(False, 1)
            self.batch_directory_entry = gtk.Entry()
            self.batch_directory_entry.set_editable(False)
            self.batch_directory_entry.set_sensitive(0)
            self.batch_directory_hbox.pack_start(
                self.batch_directory_entry, True, True, 2)
            self.select_directory = gtk.Button("  Select  ")
            self.select_directory.connect(
                "clicked", self.batch_select_directory)
            self.batch_directory_hbox.pack_start(
                self.select_directory, False, False, 2)
            self.batch_vbox.pack_start(
                self.batch_directory_hbox, False, False, 6)

            self.buttons = gtk.HBox(False, 1)
            self.create = gtk.Button("      Next       ")
            self.create.set_alignment(xalign=1, yalign=0.5)
            self.create.connect("clicked", self.next_clicked)
            self.cancel = gtk.Button("      Cancel      ")
            self.cancel.connect("clicked", self.close_batch_window)
            self.cancel.set_alignment(xalign=1, yalign=0.5)
            self.buttons.pack_end(self.create, False, False, 2)
            self.buttons.pack_end(self.cancel, False, False, 2)
            self.batch_vbox.pack_start(self.buttons, False, False, 2)

            self.batch_window.add(self.batch_vbox)

            self.batch_window.show_all()
        else:
            self.on_warn(
                "Please complete construction of directory structure prior to importing data.")

    def next_clicked(self, widget):
        self.batch_window.hide()
        # Create a new window
        self.batch_step2_window = gtk.Window()
        self.batch_step2_window.connect("delete_event", self.close_batch_window)
        self.batch_step2_window.set_title("Batch Import Wizard")
        self.batch_step2_window.set_geometry_hints(self.batch_step2_window,
                                                   min_width=400,
                                                   min_height=300,
                                                   max_width=400,
                                                   max_height=-1,
                                                   base_width=-1,
                                                   base_height=-1,
                                                   width_inc=-1,
                                                   height_inc=-1,
                                                   min_aspect=-1.0,
                                                   max_aspect=-1.0)
        self.batch_step2_window.set_border_width(10)
        self.batch_step2_window.set_icon_from_file("img/epitome_icon.png")

        self.batch_step2_vbox = gtk.VBox(False, 1)
        self.label = gtk.Label("Step 2: Select import method")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.batch_step2_vbox.pack_start(self.label, True, True, 2)

        self.regex_import_button = gtk.RadioButton(
            None, "Option #1: Regex Import")
        self.regex_import_button.connect("toggled", self.callback)
        self.batch_step2_vbox.pack_start(
            self.regex_import_button, True, True, 0)
        self.regex_import_button.set_active(False)

        self.label = gtk.Label("To use this option, ensure each folder in import directory contains string corresponding to user-defined name in project directory. For example, if you named a group YA, folder from import directory must contain the string YA. Acceptable folder names include 001YA etc.  ")
        self.label.set_alignment(xalign=0.3, yalign=0.5)
        self.label.set_line_wrap(True)
        self.batch_step2_vbox.pack_start(self.label, True, True, 2)

        self.manual_import_button = gtk.RadioButton(
            self.regex_import_button, "Option #2: Manual Import")
        self.manual_import_button.connect("toggled", self.callback)
        self.batch_step2_vbox.pack_start(
            self.manual_import_button, True, True, 0)
        self.manual_import_button.set_active(False)

        self.label = gtk.Label("Ideas?")
        self.label.set_alignment(xalign=0.3, yalign=0.5)
        self.label.set_line_wrap(True)
        self.batch_step2_vbox.pack_start(self.label, True, True, 2)

        self.buttons = gtk.HBox(False, 1)
        self.next = gtk.Button("      Next       ")
        self.next.set_alignment(xalign=1, yalign=0.5)
        self.next.connect("clicked", self.step2_next_clicked)
        self.cancel = gtk.Button("      Cancel      ")
        self.cancel.connect("clicked", self.delete_event)
        self.cancel.set_alignment(xalign=1, yalign=0.5)
        self.buttons.pack_end(self.next, False, False, 2)
        self.buttons.pack_end(self.cancel, False, False, 2)
        self.batch_step2_vbox.pack_start(self.buttons, False, False, 2)

        self.batch_step2_window.add(self.batch_step2_vbox)

        self.batch_step2_window.show_all()

    def callback(self, widget):
        print("HI")

    def has_created_directory_structure(self):
        self.error = False
        self.treestore.foreach(self.check_hierarchy, 5)
        return self.error

    def has_completed_project_hierarchy(self):
        self.error = False
        self.treestore.foreach(self.check_hierarchy, 6)
        return self.error
        
    def check_hierarchy(self, model, path, iter, depth):
        if len(path) == depth:
            self.error = True
            return True   

    def batch_select_directory(self, widget, data=None):
        dialog = gtk.FileChooserDialog("Open Project...", None,
                                       gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                       (gtk.STOCK_CANCEL,
                                        gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN,
                                        gtk.RESPONSE_OK))
        dialog.set_default_response(gtk.RESPONSE_OK)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.import_root_directory = dialog.get_filename()
            self.batch_directory_entry.set_text(self.import_root_directory)
        dialog.destroy()

    def step2_next_clicked(self, widget):
        self.treestore.foreach(self.import_files, None)
        self.batch_step2_window.destroy()
        self.on_info("Import completed successfully.")

    def close_batch_window(self, widget, data=None):
        self.batch_window.destroy()

    def import_files(self, model, path, iterr, data=None):
        """Import Files
        
        """
        filepath = "/"
        current_path = (path[0],)
        if len(path) == 4:
            for i in path[1:4]:
                current_path += (i,)
                current_iter = self.treestore.get_iter(current_path)
                filepath += str(self.treestore.get_value(current_iter, 0))
            for filee in os.listdir(self.import_root_directory + filepath):
                self.treestore.append(iterr, [str(filee), 
                    self.import_root_directory + filepath + str(filee), 
                    self.get_destination(iterr), 
                    True, 
                    True])
            self.treeview.expand_all()
            
    def create_clicked(self, widget, data=None):
        if self.project_group_exists():
            if self.has_completed_project_hierarchy():
                self.dialog = Dialog()
                response = self.dialog.on_confirmation(
                    "Confirm Project Hierarchy is correct and create project?",
                    "Confirm Project Hierarchy")
                if response == gtk.RESPONSE_ACCEPT:
                    self.create_project()
                    self.ID = 0
                    self.DB = DB(self.project_directory + "/" + self.project_name, 
                        self.project_name, "files")
                    self.filecount = 0
                    self.moving_avg = []
                    self.treestore.foreach(self.count_files, None)
                    self.dataset = []
                    self.tag = gobject.idle_add(self.copy_files_generator().next)
            else:
                self.on_info(
                    """Please ensure Project Hierarchy is complete before 
                    proceeding."""
                )
        else:
            md = Dialog()
            md.on_info("Please ensure UNIX group exists that matches project name.")
        
    def project_group_exists(self):
        group = open("/etc/group", "r")
        exists = False
        self.project_members = []
        directory = self.project_directory.split("/")
        directory_name = directory[len(directory) - 1]
        for line in group:
            if directory_name.lower() in line:
                exists = True
                ids = line.split(":")
                ids = ids[3].split(",")
                for Id in ids:
                    Id = str(Id).replace("\n","")
                    self.project_members.append(Id)
                return exists
        return exists
                     
    
    def apply_to_all(self, widget):
        if self.APPLY_TO_ALL == 1:
            self.check.set_active(False)
            self.APPLY_TO_ALL = 0
        elif self.APPLY_TO_ALL == 0:
            self.check.set_active(True)
            self.APPLY_TO_ALL = 1

    def update_pbar(self, current_iter):
        self.progress_bar.set_text("Copying " + self.treestore.get_value(current_iter, 0) + "...")
        self.fraction = self.fraction + (1 / float(self.filecount))
        if self.fraction > 1:
            self.fraction = 0
        self.progress_bar.set_fraction(self.fraction)

    def count_files(self, model, path, iterr, data=None):
        if len(path) == 6:
            self.filecount += 1
    
    def copy_files_generator(self):
        #pdb.set_trace()
        for self.index in range(1, self.filecount + 1):
            self.treestore.foreach(self.copy_files, None)
            yield self.index * 0 + 1
        else:
            self.progress_bar.set_text("Finishing up ...")
            self.progress_bar.set_fraction(1)
            self.create_project_window.destroy()
            self.DB.close()
            self.window.destroy()
            # Create a new Project Object
            project = Project(self.project_name, 
                self.project_directory, 
                self.project_Id,
                self.project_root,
                self.project_members,
                self.get_username())
            project.select_data_treestore = self.treestore
            project.save_project()
            self.on_info("Project setup complete.\n\nOwner: " + 
                self.get_username() +
                "\nMembers: " + ", ".join([str(x) for x in self.project_members]))
            new_project = ProjectEditor(project)
            #gobject.source_remove(self.tag)
            yield False    
    
    def copy_files(self, model, path, iterr, data=None):
        """Copy Files
        
        """
        if len(path) == 6:
            filepath = self.treestore.get_value(iterr, 2) + self.treestore.get_value(iterr, 0)
            #pdb.set_trace()
            if filepath not in self.dataset:
                group = self.treestore.get_value(self.treestore.get_iter(path[0:1]), 0)
                participant = self.treestore.get_value(self.treestore.get_iter(path[0:2]), 0)
                task = self.treestore.get_value(self.treestore.get_iter(path[0:3]), 0)
                run = self.treestore.get_value(self.treestore.get_iter(path[0:4]), 0)
                subprocess.call(["mkdir", "-p", str(self.treestore.get_value(iterr, 2))])
                timer_start = time.time()
                subprocess.call(["rsync", 
                    "-zh", 
                    self.treestore.get_value(iterr, 1), 
                    self.treestore.get_value(iterr, 2)])
                elapsed = time.time() - timer_start
                self.moving_avg.append(elapsed)
                elapsed = sum(self.moving_avg) / len(self.moving_avg)
                remaining = int(elapsed * (self.filecount - self.index))
                if remaining > 3600:
                    hours = int(remaining / 3600)
                    minutes = int((remaining - (hours * 3600))/60)
                    seconds = int(remaining - (hours * 3600) - (minutes * 60))
                    self.progress.set_text("Time remaining: {0:d} hours {1:d} minutes {2:d} seconds".format(hours, minutes, seconds))
                
                elif remaining > 60:
                    minutes = int(remaining / 60)
                    seconds = int(remaining - (60 * minutes))
                    self.progress.set_text("Time remaining: {0:d} minutes {1:d} seconds".format(minutes, seconds))
                else:
                    seconds = remaining
                    self.progress.set_text("Time remaining: {0:d} seconds".format(seconds))               
                self.DB.entry(self.ID,
                    group, 
                    participant, 
                    task,
                    self.treestore.get_value(iterr, 2) + "/" + self.treestore.get_value(iterr, 0), None, None)
                self.ID += 1
                self.update_pbar(iterr)
                self.dataset.append(self.treestore.get_value(iterr, 2) + self.treestore.get_value(iterr, 0))
                return True
        return False

    def get_username(self):
        return pwd.getpwuid(os.getuid())[0]
    
    def create_project(self):
        # Create a new window
        self.create_project_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.create_project_window.connect("delete_event", self.close_batch_window)
        self.create_project_window.set_title("Create Project")
        self.create_project_window.set_geometry_hints(self.create_project_window,
                                                      min_width=380,
                                                      min_height=110,
                                                      max_width=300,
                                                      max_height=-1,
                                                      base_width=-1,
                                                      base_height=-1,
                                                      width_inc=-1,
                                                      height_inc=-1,
                                                      min_aspect=-1.0,
                                                      max_aspect=-1.0)
        self.create_project_window.set_border_width(10)
        self.create_project_window.set_icon_from_file("img/epitome_icon.png")

        self.create_project_vbox = gtk.VBox(False, 1)
        
        self.info = gtk.Label("Please wait for project setup to complete.")
        self.info.set_alignment(xalign=0, yalign=0.5) 
        self.create_project_vbox.pack_start(self.info, False, False, 2)
        
        self.progress = gtk.Label("Time remaining: Unknown")
        self.progress.set_alignment(xalign=0, yalign=0.5) 
        self.create_project_vbox.pack_start(self.progress, False, False, 2)
        
        self.progress_bar = gtk.ProgressBar()
        self.progress_bar.set_fraction(0.0)
        self.fraction = 0.0
        self.progress_bar.set_orientation(gtk.PROGRESS_LEFT_TO_RIGHT)
        self.progress_bar.set_text("Initializing project...")
        self.create_project_vbox.pack_start(self.progress_bar, True, True, 6)

        #self.buttons = gtk.HBox(False, 1)
        #self.cancel = gtk.Button("            Cancel            ")
        #self.cancel.connect("clicked", self.delete_event)
        #self.cancel.set_alignment(xalign=1, yalign=0.5)
        #self.buttons.pack_end(self.cancel, False, False, 2)
        #self.create_project_vbox.pack_start(self.buttons, True, True, 2)

        self.create_project_window.add(self.create_project_vbox)

        self.create_project_window.show_all()

    def __init__(self, project_Id):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.delete_event)
        self.window.set_title("New Project")
        self.window.set_position(gtk.WIN_POS_CENTER)
        self.window.set_geometry_hints(self.window,
                                       min_width=600,
                                       min_height=-1,
                                       max_width=-1,
                                       max_height=-1,
                                       base_width=-1,
                                       base_height=-1,
                                       width_inc=-1,
                                       height_inc=-1,
                                       min_aspect=-1.0,
                                       max_aspect=-1.0)
        self.window.set_border_width(3)
        self.window.set_icon_from_file("img/epitome_icon.png")

        self.wrapper = gtk.VBox(False, 1)
        self.main_vbox = gtk.VBox(False, 1)
        self.main_hbox = gtk.HBox(False, 1)

        self.swH = gtk.ScrolledWindow()
        self.swH.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        
        self.treestore = gtk.TreeStore(str, str, str, bool, bool)
        self.project_Id = project_Id

        self.treeview = gtk.TreeView(self.treestore)
        self.treeview.set_enable_tree_lines(True)

        self.column0 = gtk.TreeViewColumn('Project Hierarchy')

        self.treeview.append_column(self.column0)

        self.cell0 = gtk.CellRendererText()
        self.column0.pack_start(self.cell0, True)
        self.column0.add_attribute(self.cell0, 'text', 0)

        self.swH.add(self.treeview)

        self.label = gtk.Label("Project Directory:")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.main_vbox.pack_start(self.label, True, True, 2)

        self.project_directory = None

        self.directory_hbox = gtk.HBox(False, 1)
        self.directory_entry = gtk.Entry()
        self.directory_entry.set_editable(False)
        self.directory_entry.set_sensitive(0)
        self.directory_hbox.pack_start(self.directory_entry, True, True, 2)

        self.select_directory = gtk.Button("  Select  ")
        self.select_directory.connect("clicked", self.select_project_directory)
        self.directory_hbox.pack_start(self.select_directory, False, False, 2)
        self.main_vbox.pack_start(self.directory_hbox, False, False, 2)

        self.name_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("Project Root:")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.main_vbox.pack_start(self.label, False, False, 2)

        self.project_root_entry = gtk.Entry()
        self.project_root_entry.set_editable(False)
        self.project_root_entry.set_sensitive(0)
        #self.project_name_entry.connect("changed", self.set_project_name)
        self.name_hbox.pack_start(self.project_root_entry, True, True, 2)
        self.main_vbox.pack_start(self.name_hbox, False, False, 2)

        self.name_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("Project Name:")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.main_vbox.pack_start(self.label, False, False, 2)
        
        self.project_name_entry = gtk.Entry()
        self.project_name_entry.connect("changed", self.set_project_name)
        self.name_hbox.pack_start(self.project_name_entry, True, True, 2)
        self.main_vbox.pack_start(self.name_hbox, False, False, 2)

        self.group_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("Group Name:")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.main_vbox.pack_start(self.label, False, False, 2)
        self.group = gtk.Entry()
        self.group.set_max_length(10)
        self.group_iter = None
        self.ITERS_group = []

        self.plus = gtk.Button("  +  ")
        self.plus.connect("clicked", self.add_group)
        self.minus = gtk.Button("  -  ")
        self.minus.connect("clicked", self.remove_group)

        self.group_hbox.pack_start(self.group, True, True, 2)
        self.group_hbox.pack_start(self.plus, False, False, 0)
        self.group_hbox.pack_start(self.minus, False, False, 0)
        self.main_vbox.pack_start(self.group_hbox, False, False, 2)

        self.lhbox = gtk.HBox(False, 1)
        self.participants_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("No. of Participants:")
        self.label.set_size_request(130, 16)
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.lhbox.pack_start(self.label, False, False, 2)
        self.label = gtk.Label("Index:")
        self.label.set_size_request(50, 16)
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.lhbox.pack_start(self.label, False, False, 2)
        
        self.main_vbox.pack_start(self.lhbox, False, False, 2)

        self.no_of_participants = gtk.Entry()
        self.no_of_participants.set_size_request(130, 24)
        self.no_of_participants.set_max_length(3)
        
        self.starting_index = gtk.Entry()
        self.starting_index.set_text("1")
        self.starting_index.set_size_request(50, 24)
        self.starting_index.set_max_length(3)

        self.update = gtk.Button(" Update ")
        self.update.connect("clicked", self.update_participants)
        self.participants_hbox.pack_start(
            self.no_of_participants,  False, False, 2)
        self.participants_hbox.pack_start(
            self.starting_index, False, False, 2)
        self.participants_hbox.pack_start(self.update, False, False, 2)
        self.main_vbox.pack_start(self.participants_hbox, False, False, 2)

        self.task_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("Task Name: ")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.main_vbox.pack_start(self.label, False, False, 2)
        self.task = gtk.Entry()
        self.task.set_max_length(10)

        self.task_iter = None
        self.ITERS_task = []
        self.ADDED_TASKS = 0

        self.plus2 = gtk.Button("  +  ")
        self.plus2.connect("clicked", self.add_task)
        self.minus2 = gtk.Button("  -  ")
        self.minus2.connect("clicked", self.remove_task)

        self.task_hbox.pack_start(self.task, True, True, 2)
        self.task_hbox.pack_start(self.plus2, False, False, 0)
        self.task_hbox.pack_start(self.minus2, False, False, 0)
        self.main_vbox.pack_start(self.task_hbox, False, False, 2)
        
        self.run_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("No. of Runs: ")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.main_vbox.pack_start(self.label, False, False, 2)
        self.run = gtk.Entry()
        self.run.set_max_length(2)

        self.update = gtk.Button(" Update ")
        self.update.connect("clicked", self.update_run)

        self.run_hbox.pack_start(self.run, True, True, 2)
        self.run_hbox.pack_start(self.update, False, False, 2)
        self.main_vbox.pack_start(self.run_hbox, False, False, 2)

        self.check_label_hbox = gtk.HBox(False, 1)
        self.label = gtk.Label("Options: ")
        self.label.set_alignment(xalign=0, yalign=0.5)
        self.check_label_hbox.pack_start(self.label, False, False, 2)
        self.main_vbox.pack_start(self.check_label_hbox, False, False, 2)

        self.check_hbox = gtk.HBox(False, 1)
        self.check = gtk.CheckButton("Apply to all")
        self.check.set_active(True)
        self.APPLY_TO_ALL = 1
        self.check.connect("clicked", self.apply_to_all)
        self.check_hbox.pack_start(self.check, True, True, 2)
        self.main_vbox.pack_start(self.check_hbox, False, False, 2)

        self.data_hbox = gtk.HBox(False, 1)
        self.select_data = gtk.Button("  Select Data ...  ")
        self.select_data.connect("clicked", self.select_data_clicked)
        self.data_hbox.pack_start(self.select_data, True, True, 2)

        self.batch_hbox = gtk.HBox(False, 1)
        self.select_batch = gtk.Button("  Batch Import ...  ")
        self.select_batch.connect("clicked", self.select_batch_clicked)
        self.data_hbox.pack_start(self.select_batch, True, True, 2)
        self.main_vbox.pack_start(self.data_hbox, False, False, 2)

        self.main_hbox.pack_start(self.main_vbox, False, False, 10)
        self.main_hbox.pack_start(self.swH, True, True, 10)

        self.wrapper.pack_start(self.main_hbox, False, False, 12)

        self.buttons = gtk.HBox(False, 1)
        self.create = gtk.Button("             Create             ")
        self.create.set_alignment(xalign=1, yalign=0.5)
        self.create.connect("clicked", self.create_clicked)
        self.cancel = gtk.Button("             Cancel             ")
        self.cancel.connect("clicked", self.delete_event)
        self.cancel.set_alignment(xalign=1, yalign=0.5)
        self.buttons.pack_end(self.create, False, False, 12)
        self.buttons.pack_end(self.cancel, False, False, 2)

        self.wrapper.pack_start(self.buttons, False, False, 10)

        self.window.add(self.wrapper)

        self.window.show_all()
        
        self.last_opened_location = None

