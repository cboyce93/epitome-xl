import pygtk
import gtk
import dill
pygtk.require('2.0')


class TreeStoreFunctions():

    def get_children(self, root_iter):
        children = []
        for i in range(0, self.treestore.iter_n_children(root_iter)):
            children.append(self.treestore.iter_nth_child(root_iter, i))
        return children
