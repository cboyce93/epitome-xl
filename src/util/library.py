import sqlite3

from os.path import expanduser
import subprocess

class Library:

    def __init__(self, table):
        subprocess.call(['mkdir', '-p', expanduser('~') + '/epitome/bin/'])
        self.conn = sqlite3.connect(expanduser('~') + "/epitome/bin/library.db")
        self.c = self.conn.cursor()
        self.table = table
        self.create_table()
        self.init = True

    def create_table(self):
        if self.table == "library":
            self.c.execute("""CREATE TABLE IF NOT EXISTS library(
                                        Id integer PRIMARY KEY,
                                        library text NOT NULL
                                    )""")
        elif self.table == "category":
            self.c.execute("""CREATE TABLE IF NOT EXISTS category (
                                        Id integer PRIMARY KEY,
                                        library text NOT NULL,
                                        category text NOT NULL
                                    )""")
        elif self.table == "function":
            self.c.execute("""CREATE TABLE IF NOT EXISTS function (
                                        Id integer PRIMARY KEY,
                                        library text NOT NULL,
                                        category text NOT NULL,
                                        function text NOT NULL
                                    )""")

    # Library entry handlers
    
    def library_entry(self, Id, library):
        self.c.execute("INSERT INTO library(Id, library) VALUES (?, ?)",
                       (Id, library))
        self.conn.commit()

    def update_library_entry(self, Id, library):
        self.c.execute("UPDATE library SET library=? WHERE Id=?", (library, Id))
        self.conn.commit()
    
    def delete_library_entry(self, Id):
        self.c.execute("DELETE FROM library WHERE Id=?", (Id,))
        self.conn.commit()
    
    # Category entry handlers
    
    def category_entry(self, Id, library, category):
        self.c.execute("INSERT INTO category(Id, library, category) VALUES (?, ?, ?)",
                       (Id, library, category))
        self.conn.commit()

    def update_category_entry(self, Id, library, category):
        self.c.execute("UPDATE category SET library=? WHERE Id=?", (library, Id))
        self.c.execute("UPDATE category SET category=? WHERE Id=?", (category, Id))
        self.conn.commit()
    
    def delete_category_entry(self, Id):
        self.c.execute("DELETE FROM category WHERE Id=?", (Id,))
        self.conn.commit()
        
    # Function entry handlers
    
    def function_entry(self, Id, library, category, function):
        self.c.execute("INSERT INTO function(Id, library, category, function) VALUES (?, ?, ?, ?)",
                       (Id, library, category, function))
        self.conn.commit()

    def update_function_entry(self, Id, library, category, function):
        self.c.execute("UPDATE function SET library=? WHERE Id=?", (library, Id))
        self.c.execute("UPDATE function SET category=? WHERE Id=?", (category, Id))
        self.c.execute("UPDATE function SET function=? WHERE Id=?", (function, Id))
        self.conn.commit()
    
    def delete_function_entry(self, Id):
        self.c.execute("DELETE FROM function WHERE Id=?", (Id,))
        self.conn.commit() 

    def read(self):
        self.c.execute("SELECT * from " + self.table)
        return self.c.fetchall()

    def close(self):
        self.conn.close()

