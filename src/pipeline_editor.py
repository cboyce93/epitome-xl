#!/usr/bin/env python

import pygtk
import gtk
import dill
import pango
import os
import gobject
import time
import sqlite3
from subprocess import Popen, PIPE, call
from os.path import expanduser
pygtk.require('2.0')

from cmd_editor import CmdEditor
from dialog import Dialog

from lib.Project import Project
from lib.Module import Module

from util.db import DB
from util.library import Library
from util.open import Open

import pdb

class PipelineEditor():

    # General Window handlers

    def __close_window(self, widget, window):
        self.pipeline.save()
        window.destroy()
    
    def __close_subwindow(self, window, data=None):
        window.destroy()

    def exit(self, window, data=None):
        self.pipeline.save()
        window.destroy()

    # Utility methods

    def concatenate_suffix(self, mod_suffix):
        """Concatenate suffix
        
        Keyword arguments:
            mod_suffix(str) - current module suffix
        
        Returns:
            suffix(str) - concatenated suffix
        
        Concatenate the suffix of module being processed with suffixes
        of all prior processed modules.
        """
        suffix = ""
        for index in range(0, self.treestore.iter_n_children(None)):
            iterr = self.treestore.get_iter(index,)
            op = Open()
            mod = op.open_module(self.treestore.get_value(iterr, 6))
            if mod.state == 2:
                suffix += self.treestore.get_value(iterr, 5)
        return suffix + mod_suffix
    
    def __get_pixbuf(self, state):
        """Get Pixbuf
        
        Keyword arguments:
            state(int) - state of module being assessed
        
        Returns:
            gtk.gdk.pixbuf corresponding to module state
        
        Change the pixbuf in treeview to visually reflect the state of module.
        
        Possible states:
        0 - unprocessed
        1 - in progress
        2 - processed/complete
        3 - error
        """
        if state == 0:
            return gtk.gdk.pixbuf_new_from_file("img/unlocked.png")
        elif state == 1:
            return gtk.gdk.pixbuf_new_from_file("img/in_progress.png")
        elif state == 2:
            return gtk.gdk.pixbuf_new_from_file("img/completed.png")
        elif state == 3:
            return gtk.gdk.pixbuf_new_from_file("img/error.png")
            
    def _get_output_filename(self, rule, module, filepath, index):
        """ Get Output filename
        
        Keyword arguments:
            rule(str) - Filenaming rule attribute from Project class
            module(Module) - instance of module being run
            filepath(str) - the filepath of current file/directory we are 
                processing for which we want to generate a new name.
            index(int) - index of file in dataset
                
        Returns:
            output_filename(str) - output filename of processed file/directory
        
        Generate the output filename for current file being processed by 
        selected module in pipeline based on filenaming rule supplied by user.
        """
        #pdb.set_trace()
        output_filename = rule
        # find index of group in path
        group_index = len(self.pipeline.project_path.split("/"))
        # Cases based on filenaming rule!
        if "$project" in output_filename:
            output_filename = output_filename.replace("$project", 
                self.pipeline.project)
        if "$pipeline" in output_filename:
            output_filename = output_filename.replace("$pipeline", 
                self.pipeline.name)
        if "$modulecategory" in output_filename:
            output_filename.replace("$modulecategory", 
                module.category)
        if "$modulefunction" in output_filename:
            output_filename = output_filename.replace("$modulefunction", 
                module.function)
        if "$modulename" in output_filename:
            output_filename = output_filename.replace("$modulename", 
                module.name)
        if "$modulesuffix" in output_filename:
            output_filename = output_filename.replace("$modulesuffix", 
                self.concatenate_suffix(module.suffix))
        if "$date" in output_filename:
            output_filename = output_filename.replace("$date", 
                time.strftime("%b-%m-%Y"))
        if "$group" in output_filename:
            group = filepath.split("/")
            output_filename = output_filename.replace("$group", 
                group[group_index])
        if "$participant" in output_filename:
            participant = filepath.split("/")
            output_filename = output_filename.replace("$participant", 
                participant[group_index + 1])
        if "$task" in output_filename:
            task = filepath.split("/")
            output_filename = output_filename.replace("$task", 
                task[group_index + 2])
        if "$run" in output_filename:
            runn = filepath.split("/")
            output_filename = output_filename.replace("$run", 
                runn[group_index + 3])
        extension = ""
        if module.prefix_type == "nii":
            extension = ".nii.gz"
        elif module.prefix_type == "bh":
            extension = "+orig.BRIK"
        for filee in self.pipeline.current_dataset:
            if str(output_filename + extension) in filee:
                output_filename += "(" + str(index).zfill(3) + ")"
                break
        return output_filename 

    # Toolbar handlers
    
    def __insert_module(self, widget, data=None):
        self.im_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.im_window.connect("delete_event", self.exit)
        self.im_window.set_title('Insert Module')
        self.im_window.set_size_request(620, 560)
        self.im_window.set_border_width(0)
        self.im_window.set_icon_from_file("img/epitome_icon.png")

        main_vbox = gtk.VBox(False, 1)

        self.vpane = gtk.VPaned()

        self.treeview_hbox = gtk.HBox(True, 6)

        # Library Column        

        self.swH0 = gtk.ScrolledWindow()
        self.swH0.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH0.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.library_liststore = gtk.ListStore(int, str)
        ls = Library("library")
        data = ls.read()
        for library in data:
            Id = library[0]
            library = library[1].encode('ascii', 'ignore')
            self.library_liststore.append([Id, library])
        self.sorted_library_liststore = gtk.TreeModelSort(
            self.library_liststore)
        self.sorted_library_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)     
        self.library_treeview = gtk.TreeView(self.sorted_library_liststore)
        self.library_selection = self.library_treeview.get_selection()
        self.library_selection.connect("changed", self.refilter_library)
        self.library_column = gtk.TreeViewColumn('Library')
        self.library_treeview.append_column(self.library_column)
        self.cell0 = gtk.CellRendererText()
        self.library_column.pack_start(self.cell0, True)
        self.library_column.add_attribute(self.cell0, 'text', 1)
        self.library_column.set_sort_column_id(1)
        self.swH0.add(self.library_treeview)

        '''
        Category Column
        '''

        self.swH1 = gtk.ScrolledWindow()
        self.swH1.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH1.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.category_liststore = gtk.ListStore(int, str, str)
        cls = Library("category")
        data = cls.read()
        for category in data:
            Id = category[0]
            library = category[1].encode('ascii', 'ignore')
            cat = category[2].encode('ascii', 'ignore')
            self.category_liststore.append([Id, library, cat]) 

        self.catfilter = self.category_liststore.filter_new()
        self.catfilter.set_visible_func(self.visible_cat)
        
        self.sorted_cat_liststore = gtk.TreeModelSort(self.catfilter)
        self.sorted_cat_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)

        self.category_treeview = gtk.TreeView(self.sorted_cat_liststore)

        self.category_selection = self.category_treeview.get_selection()
        self.category_selection.connect("changed", self.refilter_category)

        # create col 0 to display Pipeline Object name
        self.category_column = gtk.TreeViewColumn('Category')

        # append columns to TreeView
        self.category_treeview.append_column(self.category_column)

        # create a CellRendererText to render the data
        self.cell1 = gtk.CellRendererText()

        self.category_column.pack_start(self.cell1, True)
        self.category_column.add_attribute(self.cell1, 'text', 2)

        # allow search on columns
        # self.treeview.set_search_column(1)
        # self.treeview.set_search_column(2)

        # Allow sorting on both columns
        self.category_column.set_sort_column_id(2)

        self.swH1.add(self.category_treeview)

        '''
        
        Function Column
        '''

        self.swH2 = gtk.ScrolledWindow()
        self.swH2.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH2.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.function_liststore = gtk.ListStore(int, str, str, str)

        fls = Library("function")
        data = fls.read()
        for tup in data:
            Id = tup[0]
            library = tup[1].encode('ascii', 'ignore')
            category = tup[2].encode('ascii', 'ignore')
            function = tup[3].encode('ascii', 'ignore')
            self.function_liststore.append([Id, library, category, function])

        self.functionfilter = self.function_liststore.filter_new()
        self.functionfilter.set_visible_func(self.visible_function)
        
        self.sorted_function_liststore = gtk.TreeModelSort(self.functionfilter)
        self.sorted_function_liststore.set_sort_column_id(0, 
            gtk.SORT_ASCENDING)
        
        self.function_treeview = gtk.TreeView(self.sorted_function_liststore)

        self.function_selection = self.function_treeview.get_selection()
        self.function_selection.connect("changed", self.refilter_function)

        # create col 0 to display Pipeline Object name
        self.function_column = gtk.TreeViewColumn('Function')

        # append columns to TreeView
        self.function_treeview.append_column(self.function_column)

        # create a CellRendererText to render the data
        self.cell2 = gtk.CellRendererText()

        self.function_column.pack_start(self.cell2, True)
        self.function_column.add_attribute(self.cell2, 'text', 3)

        # allow search on columns
        self.function_treeview.set_search_column(3)

        # Allow sorting on both columns
        self.function_column.set_sort_column_id(3)

        self.swH2.add(self.function_treeview)

        '''
        Module Column
        '''

        self.swH3 = gtk.ScrolledWindow()
        self.swH3.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH3.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.module_liststore = gtk.ListStore(str, str, str, str, str)

        for root, dirs, files in os.walk('/home/cboyce/epitome/modules/'):
            for filepath in files:
                op = Open()
                module = op.open_module(
                    '/home/cboyce/epitome/modules/' + filepath)
                self.module_liststore.append([
                    module.library, 
                    module.category, 
                    module.function, 
                    module.name, 
                    module.filename
                ])
        
        self.modulefilter = self.module_liststore.filter_new()
        self.modulefilter.set_visible_func(self.visible_mod)

        self.sorted_module_liststore = gtk.TreeModelSort(self.modulefilter)
        self.sorted_module_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)
        
        self.module_treeview = gtk.TreeView(self.sorted_module_liststore)

        self.module_selection = self.module_treeview.get_selection()
        self.module_selection.connect("changed", self.update_module_textview)

        # create col 0 to display Pipeline Object name
        self.module_column = gtk.TreeViewColumn('Module')

        # append columns to TreeView
        self.module_treeview.append_column(self.module_column)

        # create a CellRendererText to render the data
        self.cell3 = gtk.CellRendererText()

        self.module_column.pack_start(self.cell3, True)
        self.module_column.add_attribute(self.cell3, 'text', 3)

        # allow search on columns
        self.module_treeview.set_search_column(3)
        # self.treeview.set_search_column(1)
        # self.treeview.set_search_column(2)

        # Allow sorting on both columns
        self.module_column.set_sort_column_id(3)

        self.swH3.add(self.module_treeview)

        self.treeview_hbox.pack_start(self.swH0, True, True, 0)
        self.treeview_hbox.pack_start(self.swH1, True, True, 0)
        self.treeview_hbox.pack_start(self.swH2, True, True, 0)
        self.treeview_hbox.pack_start(self.swH3, True, True, 0)

        '''
        Label
        '''

        #self.label = gtk.Label("Module Description")

        '''
        TextView
        '''
        self.textview_hbox = gtk.HBox(False, 0)

        self.textview_hbox.set_size_request(500, 200)
        self.swH3 = gtk.ScrolledWindow()
        self.swH3.set_shadow_type(gtk.SHADOW_ETCHED_IN)

        self.swH3.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.buffer = gtk.TextBuffer(None)
        self.buffer.set_text("(no module selected)")
        self.textview = gtk.TextView(self.buffer)
        self.textview.set_border_width(10)
        self.textview.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        self.textview.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.textview.set_justification(gtk.JUSTIFY_FILL)

        self.swH3.add(self.textview)

        self.textview_hbox.pack_start(self.swH3, True, True, 0)

        self.vpane.add1(self.treeview_hbox)
        #main_vbox.pack_start(self.label, False, False, 5)
        self.vpane.add2(self.textview_hbox)
        self.vpane.set_position(160)
        self.vpane.set_border_width(6)

        # Buttons
        hbox = gtk.HBox(False, 3)
        cancel = gtk.Button("        Cancel        ")
        cancel.set_size_request(160, 30)
        cancel.set_alignment(0.5, 0.5)
        cancel.connect("clicked", self.__close_window, self.im_window)
        insert = gtk.Button("        Insert Module        ")
        insert.set_size_request(160, 30)
        insert.set_alignment(0.5, 0.5)
        insert.connect("clicked", self.__insert_module_clicked, 
            self.module_selection)

        # pack buttons
        hbox.pack_end(insert, False, True, 10)
        hbox.pack_end(cancel, False, True, 4)

        main_vbox.pack_start(self.vpane, True, True, 0)
        main_vbox.pack_start(hbox, False, False, 12)
        self.im_window.add(main_vbox)

        self.im_window.show_all()
        
    def refilter_library(self, model, data=None):
        self.catfilter.refilter()
        if self.catfilter.iter_n_children(None) > 0:
            root_iter = self.sorted_cat_liststore.convert_child_iter_to_iter(None, self.catfilter.get_iter_first())
            if root_iter is not None:
                self.category_selection.select_iter(root_iter)

    def refilter_category(self, model, data=None):
        self.functionfilter.refilter()
        if self.functionfilter.iter_n_children(None) > 0:
            root_iter = self.sorted_function_liststore.convert_child_iter_to_iter(None, self.functionfilter.get_iter_first())
            if root_iter is not None:
                self.function_selection.select_iter(root_iter)

    def refilter_function(self, model, data=None):
        self.modulefilter.refilter()
        if self.modulefilter.iter_n_children(None) > 0:
            root_iter = self.sorted_module_liststore.convert_child_iter_to_iter(None, self.modulefilter.get_iter_first())
            if root_iter is not None:
                self.module_selection.select_iter(root_iter)

    def match_func(self, model, iterr, data=None):
        return True
    
    def visible_cat(self, model, iterr, data=None):
        treeselection = self.library_treeview.get_selection()

        if treeselection.count_selected_rows() > 0:
            m, i = treeselection.get_selected()
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            if(model.get_value(iterr, 1) == self.library_liststore.get_value(i, 1)):
                return True
        return False

    def visible_function(self, model, iterr, data=None):
        lib_treeselection = self.library_treeview.get_selection()
        cat_treeselection = self.category_treeview.get_selection()
        if lib_treeselection.count_selected_rows() > 0 and cat_treeselection.count_selected_rows() > 0:
            m, i = lib_treeselection.get_selected()
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            n, j = cat_treeselection.get_selected()
            j = self.sorted_cat_liststore.convert_iter_to_child_iter(None, j)
            if model.get_value(iterr, 1) == self.library_liststore.get_value(i, 1) and model.get_value(iterr, 2) == self.catfilter.get_value(j, 2):
                return True
        return False

    def visible_mod(self, model, iterr, data=None):
        lib_treeselection = self.library_treeview.get_selection()
        cat_treeselection = self.category_treeview.get_selection()
        function_treeselection = self.function_treeview.get_selection()
        if lib_treeselection.count_selected_rows() > 0 and cat_treeselection.count_selected_rows() > 0 and function_treeselection.count_selected_rows() > 0:
            m, i = lib_treeselection.get_selected()
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            n, j = cat_treeselection.get_selected()
            j = self.sorted_cat_liststore.convert_iter_to_child_iter(None, j)
            o, k = function_treeselection.get_selected()
            k = self.sorted_function_liststore.convert_iter_to_child_iter(None, k)
            if model.get_value(iterr, 0) == self.library_liststore.get_value(i, 1) and model.get_value(iterr, 1) == self.catfilter.get_value(j, 2) and model.get_value(iterr, 2) == self.functionfilter.get_value(k, 3):
                return True
        return False

    def update_module_textview(self, widget, data=None):
        selection = self.module_treeview.get_selection()
        m, i = selection.get_selected()
        if i is not None:
            i = self.sorted_module_liststore.convert_iter_to_child_iter(None, i)
            name = self.modulefilter.get_value(i, 3)
            try:
                filehandler = open(
                    expanduser("~") + '/epitome/modules/' + name + '.mod', 'rb')
                module = dill.load(filehandler)
                self.buffer.set_text('')
                # pango tags
                bold = self.buffer.create_tag(weight=pango.WEIGHT_BOLD)
                title = self.buffer.create_tag(size=18000)
                code = self.buffer.create_tag(font="Monospace 10")
                crumbs = self.buffer.create_tag(font="Italic 8")

                self.buffer.insert_with_tags(self.buffer.get_start_iter(
                ), module.library + ' > ' + module.category + ' > ' + module.function + '\n\n', crumbs)
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, name + '\n', bold, title)
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Author: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.author + '\n')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Date Created: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.date_created + '\t')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Last Edited: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.date_updated + '\n')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, '\nSuffix: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.suffix + '\n\n')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Description:\n', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.desc + '\n\n')

                start, end = self.buffer.get_bounds()
                anchor = self.buffer.create_child_anchor(end)
                button = gtk.LinkButton(
                    module.url, "Click here to access online resource...")
                button.show()
                self.textview.add_child_at_anchor(button, anchor)

                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, '\n\nCommand:\n', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, module.cmd + '\n\n', code)
            except:
                print("")
        else:
            self.buffer.set_text("(no module selected)")
        
    def __insert_module_clicked(self, widget, selection):
        """ Insert module clicked
        
        Keyword arguments:
            selection(gtk.TreeSelection) - current selection in treeview
        
        Copy selected module from library directory into local project
        direcotory. Create module object.
        """
        mod, iterr = selection.get_selected()
        if iterr is not None:
            iterr = self.sorted_module_liststore.convert_iter_to_child_iter(None, iterr)
            library_module_path = self.modulefilter.get_value(iterr, 4)
            # grab module filename from library_module_path
            module_filename = library_module_path.split("/")
            module_filename = module_filename[len(module_filename) - 1]
            index = 0
            for root, dirs, files in os.walk(self.pipeline.path):
                for filee in files:
                    if module_filename.replace(".mod","") in filee:
                        index += 1
            destination_path = self.pipeline.path
            if index > 0:
                library_module_path2 = str(library_module_path.replace(".mod","") + 
                    "(" + str(index).zfill(2) + ")") + ".mod"
                process = call(['cp', library_module_path, library_module_path2])
                process = call(['cp', library_module_path2, destination_path])
                call(['rm', library_module_path2])
                filename = str(module_filename.replace(".mod","") + 
                     "(" + str(index).zfill(2) + ")" + ".mod")
            else:
                process = call(['cp', library_module_path, destination_path])
                filename = module_filename
            op = Open()
            module = op.open_module(destination_path + filename)
            module.reset()
            module.filename = self.pipeline.path + filename + '.mod'
            module.stage = self.treestore.iter_n_children(None) + 1
            # update DB
            mods = DB(self.pipeline.project_path, 
                self.pipeline.project, 
                self.pipeline.name)
            #pdb.set_trace()
            if module.Id is None:
                module.Id = self.pipeline.Id_gen
                self.pipeline.Id_gen += 1
                mods.module_entry(module.Id, 
                    module.stage, 
                    module.state, 
                    module.function, 
                    module.name, 
                    module.suffix, 
                    module.filename,
                    module.keepinter,
                    module.sensitive)
            else:
                mods.update_module(module.Id, 
                    module.stage, 
                    module.state, 
                    module.function, 
                    module.name, 
                    module.suffix, 
                    module.filename,
                    module.keepinter,
                    module.sensitive)
            self.treestore.append(None, [module.Id, 
                module.stage, 
                self.__get_pixbuf(module.state), 
                module.function, 
                module.name, 
                module.suffix, 
                module.filename,
                module.keepinter,
                module.sensitive])
            module.save()
            self.pipeline.save()
            self.im_window.destroy()
        else:
            md = Dialog()
            md.on_warn("Please select a module.")

    def __run_all(self, widget, data=None):
        pass
            
    def __run_next(self, widget, data=None):
        """ Run Next
        
        Run dataset through the next module in pipeline.
        """
        #pdb.set_trace()
        iterr = self.treestore.get_iter(self.pipeline.module_index,)
        filepath = self.treestore.get_value(iterr, 6)  
        op = Open()
        module = op.open_module(filepath)
        # test module state
        md = Dialog()
        
        #AFNI parameters
        dataset_index = None
        prefix_index = None
        
        #conversion parameters
        dataset_dir_index = None
        output_filename_index = None
        output_dir_index = None
        
        self.moving_avg = []
        
        if module.state == 0:
            args = module.cmd.split(" ")
            # get index of $var tags
            for index, arg in enumerate(args):
                # AFNI menu
                if arg == "$dataset":
                    dataset_index = index
                elif arg == "$prefix_nii":
                    prefix_index = index
                    module.prefix_type = "nii"
                elif arg == "$prefix_b+h":
                    prefix_index = index
                    module.prefix_type = "bh"
                # Conversion menu
                elif arg == "$dataset_directory":
                    dataset_dir_index = index
                elif arg == "$output_filename":
                    output_filename_index = index
                elif arg == "$output_directory":
                    output_dir_index = index
            #pdb.set_trace()

            module.dataset = self.pipeline.current_dataset[:]
            module.save()
            self.__run_module_window(module)
            if dataset_index is None:
                # execute converion
                task = self.__execute_conversion_module(iterr, 
                    args, 
                    dataset_dir_index,
                    output_filename_index,
                    output_dir_index,
                    module)
                self.tag = gobject.idle_add(task.next)
            else:
                # execute AFNI cmd
                task = self.__execute_module(iterr, 
                    args, 
                    dataset_index, 
                    prefix_index,
                    module)
                self.tag = gobject.idle_add(task.next)
        # deal with other states on case-by-case basis
        elif module.state == 1:
            md.on_warn("Module already running!")
        elif module.state == 2:
            md.on_warn("Module execution complete.")
        elif module.state == 3:
            md.on_warn("Module encountered an error!")
        else:
            # no modules to run
            md.on_warn("No modules to run.")

    def __execute_module(self, 
        iterr, 
        args, 
        dataset_index, 
        prefix_index,
        module):
        # update treeview pixbuf to show module in progress
        module.state = 1
        mods = DB(self.pipeline.project_path, 
            self.pipeline.project, 
            self.pipeline.name)
        mods.update_module(module.Id, 
            module.stage, 
            module.state, 
            module.function, 
            module.name, 
            module.suffix, 
            module.filename,
            module.keepinter,
            module.sensitive)
        self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
        self.__fraction = 0.01
        try:            
            # grab rule
            op = Open()
            proj = op.open_project(self.pipeline.project_path + "/" + 
                self.pipeline.project + ".epi")        
            # store intermediary dataset in case deletion is selected            
            intermediaries_dataset = self.pipeline.current_dataset[:]             
            self.pipeline.current_dataset = []
            #pdb.set_trace()
            for index, datafile in enumerate(module.dataset):
                filepath = datafile.rsplit("/", 1)
                filepath = filepath[0]                    
                if module.prefix_type == "nii":
                    output_filename = self._get_output_filename(proj.rule, 
                        module, datafile, index)
                    extension = ".nii.gz"
                    self.pipeline.current_dataset.append(filepath + "/" + 
                        output_filename + extension)
                    args[prefix_index] = str(filepath + "/" + 
                        output_filename + extension)
                # only want one of BRIK/HEAD pair for first module, 
                # otherwise dataset will contain duplicates
                elif (module.stage == 1 and index%2 == 0) or (module.stage > 1):
                    output_filename = self._get_output_filename(proj.rule, 
                        module, datafile, index)
                    self.pipeline.current_dataset.append(filepath + "/" + 
                        output_filename + "+orig")
                    args[prefix_index] = filepath + "/" + output_filename
                else:
                    continue
                args[dataset_index] = datafile
                self.timer_start = time.time()
                process = Popen(args, stdout=PIPE, stderr=PIPE)
                out, err = Popen.communicate(process)
                self.__update_pbar(datafile, len(module.dataset), index)
                yield True    
            module.state = 2
            self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
            self.pipeline.module_index += 1
            self.pipeline.is_current_dataset_in_use = False
            self.pipeline.save()
            self.rm_window.destroy()
            # delete intermediaries
            if self.treestore.get_value(iterr, 8) and module.stage != 1:
                self.__delete_intermediaries(intermediaries_dataset, module.prefix_type)
            self.treestore.set(iterr, 8, False)
            module.sensitive = False
            mods.update_module(module.Id, 
                module.stage, 
                module.state, 
                module.function, 
                module.name, 
                module.suffix, 
                module.filename,
                module.keepinter,
                False)
            module.save()
            mods.close()
            md = Dialog()
            md.on_info("Module execution complete.")
            yield False
        except OSError as e:
            md = Dialog()
            module.state = 3
            mods.update_module(module.Id, 
                module.stage, 
                module.state, 
                module.function, 
                module.name, 
                module.suffix, 
                module.filename,
                module.keepinter,
                module.sensitive)
            module.save()
            self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
            self.rm_window.destroy()
            md.on_warn("An error occured.\n\n" + "Error message:\n" + err)
            yield False
        except sqlite3.Error as e:
            md = Dialog()
            module.state = 3
            mods.update_module(module.Id, 
                module.stage, 
                module.state, 
                module.function, 
                module.name, 
                module.suffix, 
                module.filename,
                module.keepinter,
                module.sensitive)
            module.save()
            self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
            self.rm_window.destroy()
            md.on_warn("A database error occured.\n\n" + "Error message:\n" + err)
            yield False

    def __execute_conversion_module(self, 
        iterr, 
        args, 
        dataset_dir_index, 
        output_filename_index, 
        output_directory_index, 
        module):
        # update treeview pixbuf to show module in progress
        module.state = 1
        mods = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
        mods.update_module(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename, module.keepinter, module.sensitive)
        self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
        self.__fraction = 1/float(len(self.pipeline.dataset_directories))
        try:            
            # grab rule
            op = Open()
            proj = op.open_project(self.pipeline.project_path + "/" + self.pipeline.project + ".epi")        
            self.pipeline.current_dataset = []
            for index, directory in enumerate(self.pipeline.dataset_directories):
                output_filename = proj.rule
                extension = ".nii.gz"
                # Cases based on filenaming rule!
                if "$project" in output_filename:
                    output_filename = output_filename.replace("$project", self.pipeline.project)
                if "$pipeline" in output_filename:
                    output_filename = output_filename.replace("$pipeline", self.pipeline.name)
                if "$modulecategory" in output_filename:
                    output_filename.replace("$modulecategory", module.category)
                if "$modulefunction" in output_filename:
                    output_filename = output_filename.replace("$modulefunction", module.function)
                if "$modulename" in output_filename:
                    output_filename = output_filename.replace("$modulename", module.name)
                if "$modulesuffix" in output_filename:
                    output_filename = output_filename.replace("$modulesuffix", self.concatenate_suffix(module.suffix))
                if "$date" in output_filename:
                    output_filename = output_filename.replace("$date", time.strftime("%b-%m-%Y"))
                if "$group" in output_filename:
                    group = directory.split("/")
                    output_filename = output_filename.replace("$group", group[4])
                if "$participant" in output_filename:
                    participant = directory.split("/")
                    output_filename = output_filename.replace("$participant", participant[5])
                if "$task" in output_filename:
                    task = directory.split("/")
                    output_filename = output_filename.replace("$task", task[6])
                if "$run" in output_filename:
                    runn = str(directory + "/").split("/")
                    output_filename = output_filename.replace("$run", runn[7])
                self.pipeline.current_dataset.append(directory + "/" + output_filename + extension)
                if output_filename_index is not None:
                    args[output_filename_index] = output_filename
                if output_directory_index is not None:
                    args[output_directory_index] = directory
                if dataset_dir_index is not None:
                    args[dataset_dir_index] = directory
                process = Popen(args, stdout=PIPE, stderr=PIPE)
                out, err = Popen.communicate(process)
                self.__update_pbar(directory)
                yield True    
            md = Dialog()
            md.on_info("Module execution complete.")
            module.state = 2
            mods.update_module(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename)
            module.save()
            self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
            self.pipeline.module_index += 1
            self.pipeline.save()
            self.pipeline.is_current_dataset_in_use = False
            self.rm_window.destroy()
            yield False
        except OSError as e:
            md = Dialog()
            module.state = 3
            mods.update_module(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename)
            module.save()
            self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
            self.rm_window.destroy()
            md.on_warn("An error occured.\n\n" + "Error message:\n" + err)
            yield False
        except sqlite3.Error as e:
            md = Dialog()
            module.state = 3
            mods.update_module(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename)
            module.save()
            self.treestore.set(iterr, 2, self.__get_pixbuf(module.state))
            self.rm_window.destroy()
            md.on_warn("A database error occured.\n\n" + "Error message:\n" + err)
            yield False
    

    def __run_module_window(self, module):
        # Create a new window
        self.rm_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.rm_window.connect("delete_event", self.exit)
        self.rm_window.set_title("Running " + module.name + "...")
        self.rm_window.set_geometry_hints(self.rm_window,
            min_width=480,
            min_height=140,
            max_width=400,
            max_height=-1,
            base_width=-1,
            base_height=-1,
            width_inc=-1,
            height_inc=-1,
            min_aspect=-1.0,
            max_aspect=-1.0)
        self.rm_window.set_border_width(10)
        self.rm_window.set_icon_from_file("img/epitome_icon.png")

        vbox = gtk.VBox(False, 1)

        self.mod_label = gtk.Label("Module " + str(self.pipeline.module_index + 1) +
            " of " + str(self.treestore.iter_n_children(None)))
        self.mod_label.set_alignment(xalign=0, yalign=0.5)
        vbox.pack_start(self.mod_label, False, False, 2)
        
        self.op_label = gtk.Label("Operation: " + module.name)
        self.op_label.set_alignment(xalign=0, yalign=0.5)
        vbox.pack_start(self.op_label, False, False, 2)
        
        self.tr_label = gtk.Label("Time remaining: calculating...")
        self.tr_label.set_alignment(xalign=0, yalign=0.5)
        vbox.pack_start(self.tr_label, False, False, 2)
        
        self.progress_bar = gtk.ProgressBar()
        self.progress_bar.set_fraction(0.0)
        self.progress_bar.set_orientation(gtk.PROGRESS_LEFT_TO_RIGHT)
        self.progress_bar.set_text("Initializing module...")
        vbox.pack_start(self.progress_bar, True, True, 2)

        #buttons = gtk.HBox(False, 1)
        #cancel = gtk.Button("            Cancel            ")
        #cancel.connect("clicked", self.__close_window, self.rm_window)
        #cancel.set_alignment(xalign=1, yalign=0.5)
        #buttons.pack_end(cancel, False, False, 2)
        #vbox.pack_start(buttons, False, False, 2)
        self.rm_window.add(vbox)
        self.rm_window.show_all()
    
    def __update_pbar(self, filename, filecount, index):
        elapsed = time.time() - self.timer_start
        self.moving_avg.append(elapsed)
        elapsed = sum(self.moving_avg) / len(self.moving_avg)
        remaining = int(elapsed * (filecount - index))
        if remaining > 3600:
            hours = int(remaining / 3600)
            minutes = int((remaining - (hours * 3600))/60)
            seconds = int(remaining - (hours * 3600) - (minutes * 60))
            self.tr_label.set_text(str("Time remaining: {0:d} hours {1:d} " +
                "minutes {2:d} seconds").format(hours, minutes, seconds))
        elif remaining > 60:
            minutes = int(remaining / 60)
            seconds = int(remaining - (60 * minutes))
            self.tr_label.set_text(str("Time remaining: {0:d} minutes {1:d} " +
                "seconds").format(minutes, seconds))
        else:
            seconds = remaining
            self.tr_label.set_text(str("Time remaining: {0:d} " +
                "seconds").format(seconds))
        self.progress_bar.set_text("Processing " + filename + "...")
        
        self.__fraction += 1/float(filecount)
        if self.__fraction > 1:
            self.__fraction = 1
        self.progress_bar.set_fraction(self.__fraction)
    
    def __delete_module(self,widget, data=None):
        model, self.iterr = self.treeselection.get_selected()
        op = Open()
        self.module = op.open_module(self.treestore.get_value(self.iterr, 6))
        md = Dialog()
        if self.module.state == 0:
            # quick delete if nothing has been processed yet
            call(['rm', '-r', self.module.filename])
            modules = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
            modules.delete_module_entry(self.treestore.get_value(self.iterr, 0))
            self.treestore.remove(self.iterr)
            self.pipeline.save()
        else:
            md.on_warn("You may only delete unprocessed modules or the last processed module.")
        
    
    def __delete_last_module(self, widget):
        """ Delete Module
        
        Keyword arguments:
        treeselection - gtk.TreeSelection()
        
        Delete dataset associated with selected module.        
        """
        #pdb.set_trace()
        self.iterr = self.treestore.get_iter((self.pipeline.module_index - 1,))
        op = Open()
        self.module = op.open_module(self.treestore.get_value(self.iterr, 6))
        md = Dialog()
        # identify last processed module
        if self.module.deletable:
            if self.pipeline.module_index == self.module.stage: 
                if self.module.state != 0:
                    # display confimation dialog
                    self.on_confirmation("Delete all intermediary files " +
                        "associated with " + self.module.name + "?\n")
                    if self.response == gtk.RESPONSE_ACCEPT:
                        self.on_confirmation("All files processed by this module will be lost! Proceed?\n")
                        if self.response == gtk.RESPONSE_ACCEPT:
                            self.no_of_files = len(self.module.dataset)
                            self.__fraction = 0
                            self.__delete_files_window(self.pipeline.name)
                            gobject.idle_add(self.__delete_files(self.pipeline.current_dataset, prefix_type).next)
            elif self.module.state == 0:
                # quick delete if nothing has been processed yet
                call(['rm', '-r', self.module.filename])
                modules = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
                mod, self.iterr = self.treeselection.get_selected()
                modules.delete_module_entry(self.treestore.get_value(self.iterr, 0))
                self.treestore.remove(self.iterr)
                md.on_info("Module deletion successful!")
            else:
                md.on_warn("You may only delete unprocessed modules or the last processed module.")
            self.pipeline.save()
        else:
            md.on_warn("Modules of both original and duplicated pipelines which have already been processed are locked and cannot be deleted.")
        
            
    def on_confirmation(self, message):
        label = gtk.Label(message)
        self.dialog = gtk.Dialog("Confirm",
                                 None,
                                 gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                                 (gtk.STOCK_NO, gtk.RESPONSE_REJECT,
                                  gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        self.dialog.vbox.pack_start(label)
        self.dialog.set_border_width(10)
        label.show()
        self.response = self.dialog.run()
        self.dialog.destroy()
    
    def __update_deletion_pbar(self, filename, dataset):
        self.progress_bar.set_text("Deleting " + filename + "...")
        self.__fraction += 1/float(self.no_of_files)
        self.progress_bar.set_fraction(self.__fraction)
    
    def __delete_files_window(self, filename):
        # Create a new window
        self.df_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.df_window.connect("delete_event", self.__close_subwindow)
        self.df_window.set_title("Deleting " + filename + "...")
        self.df_window.set_geometry_hints(self.df_window,
            min_width=400,
            min_height=100,
            max_width=400,
            max_height=-1,
            base_width=-1,
            base_height=-1,
            width_inc=-1,
            height_inc=-1,
            min_aspect=-1.0,
            max_aspect=-1.0)
        self.df_window.set_border_width(10)
        self.df_window.set_icon_from_file("img/epitome_icon.png")

        vbox = gtk.VBox(False, 1)

        self.progress_bar = gtk.ProgressBar()
        self.progress_bar.set_fraction(0.0)
        self.progress_bar.set_orientation(gtk.PROGRESS_LEFT_TO_RIGHT)
        self.progress_bar.set_text("Initializing module...")
        vbox.pack_start(self.progress_bar, True, True, 2)

        buttons = gtk.HBox(False, 1)
        cancel = gtk.Button("            Cancel            ")
        cancel.connect("clicked", self.__close_subwindow, self.df_window)
        cancel.set_alignment(xalign=1, yalign=0.5)
        buttons.pack_end(cancel, False, False, 2)
        vbox.pack_start(buttons, False, False, 2)
        self.df_window.add(vbox)
        self.df_window.show_all()
    
    def __delete_files(self, dataset, prefix_type):
        if prefix_type == "nii":
            for datafile in dataset:
                call(['rm', datafile])
                self.__update_deletion_pbar(datafile, dataset)
                yield True
        else:
            for datafile in dataset:
                call(['rm', datafile + ".BRIK", datafile + ".HEAD"])
                self.__update_deletion_pbar(datafile, dataset)
                yield True 
        # remove db reference
        modules = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
        modules.delete_module_entry(self.treestore.get_value(self.iterr, 0))
        modules.close()
        self.treestore.remove(self.iterr)
        self.__decrement_module_indices()
        self.pipeline.module_index -= 1
        self.pipeline.current = self.module.dataset
        call(['rm', '-r', self.module.filename])
        md = Dialog()
        md.on_info("Module deletion successful!")
        self.df_window.destroy() 
        yield False
    
    def __delete_intermediaries(self, dataset, prefix_type):
        #pdb.set_trace()
        if prefix_type == "nii":
            for datafile in dataset:
                call(['rm', datafile])
        else:
            for datafile in dataset:
                call(['rm', datafile + ".BRIK", datafile + ".HEAD"])
    
    def __decrement_module_indices(self):
        """ Decrement Module Indices
        
        After module deletion, decrement the stages of all modules that follow
        in the pipeline.
        """
        # !!! start debugging here !!!
        #pdb.set_trace()
        modules = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
        db = modules.read()
        for index, data in enumerate(db):
            if data[1] >= self.pipeline.module_index:
                stage = data[1] - 1
                op = Open()
                module = op.open_module(data[6])
                module.stage = stage
                module.save()
                modules.update_module(data[0], stage, data[2], data[3], data[4], data[5], data[6])
                iterr = self.treestore.get_iter((index,))
                self.treestore.set(iterr, 1, stage)
                
    def populate_treestore(self):
        #pdb.set_trace()
        pipes = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
        db = pipes.read()
        for index, data in enumerate(db):
            self.treestore.insert(None, index, [data[0], data[1], self.__get_pixbuf(data[2]), data[3], data[4], data[5], data[6], data[7], data[8]])
        pipes.close()
    
    def __init__(self, pipeline):
        
        self.pipeline = pipeline

        window = gtk.Window()
        window.connect("delete_event", self.exit)
        window.set_title(pipeline.name + ' - Pipeline Editor')
        window.set_size_request(500, 300)
        window.set_border_width(0)
        window.set_icon_from_file("img/epitome_icon.png")

        main_vbox = gtk.VBox(False, 1)

        # MenuBar
        
        menubar = gtk.MenuBar()

        filemenu = gtk.Menu()

        accelerator_01 = gtk.AccelGroup()
        window.add_accel_group(accelerator_01)
        
        menubar = gtk.MenuBar()

        accelerator_01 = gtk.AccelGroup()
        window.add_accel_group(accelerator_01)

        modulemenu = gtk.Menu()

        filem = gtk.MenuItem("_Module")
        filem.set_right_justified(False)
        filem.set_submenu(modulemenu)

        run_all_mi = gtk.ImageMenuItem(gtk.STOCK_EXECUTE, accelerator_01)
        run_all_mi.get_children()[0].set_label('Run All')

        key, mod = gtk.accelerator_parse("<Control>L")

        run_all_mi.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        run_all_mi.connect("activate", self.__run_all)
        modulemenu.append(run_all_mi)
        
        run_next_mi = gtk.ImageMenuItem(gtk.STOCK_GO_FORWARD, accelerator_01)
        run_next_mi.get_children()[0].set_label('Run Next')

        key, mod = gtk.accelerator_parse("<Control>R")

        run_next_mi.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        run_next_mi.connect("activate", self.__run_next)
        modulemenu.append(run_next_mi)
        
        sep = gtk.SeparatorMenuItem()
        modulemenu.append(sep)
        
        new_mi = gtk.ImageMenuItem(gtk.STOCK_NEW, accelerator_01)
        new_mi.get_children()[0].set_label('New')

        key, mod = gtk.accelerator_parse("<Control>N")

        new_mi.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        new_mi.connect("activate", self.__insert_module)
        modulemenu.append(new_mi)
        
        edit_mi = gtk.ImageMenuItem(gtk.STOCK_EDIT, accelerator_01)
        edit_mi.get_children()[0].set_label('Edit')

        key, mod = gtk.accelerator_parse("<Control>E")

        edit_mi.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        edit_mi.connect("activate", self.printt)
        modulemenu.append(edit_mi)
        
        delete_mi = gtk.ImageMenuItem(gtk.STOCK_DELETE, accelerator_01)
        delete_mi.get_children()[0].set_label('Delete')

        key, mod = gtk.accelerator_parse("<Control>D")

        delete_mi.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        delete_mi.connect("activate", self.printt)
        modulemenu.append(delete_mi)

        helpmenu = gtk.Menu()

        fileh = gtk.MenuItem("_Help")
        fileh.set_right_justified(True)
        fileh.set_submenu(helpmenu)

        MI__ABOUT = gtk.ImageMenuItem(gtk.STOCK_ABOUT, accelerator_01)
        MI__ABOUT.get_children()[0].set_label('About')

        key, mod = gtk.accelerator_parse("<Control>A")

        MI__ABOUT.add_accelerator("activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        MI__ABOUT.connect("activate", self.printt)
        helpmenu.append(MI__ABOUT)

        menubar.append(filem)
        menubar.append(fileh)

        filemenubox = gtk.VBox(False, 2)
        filemenubox.pack_start(menubar, True, True, 0)

        # Toolbar

        toolbar = gtk.Toolbar()
        toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
        toolbar.set_style(gtk.TOOLBAR_ICONS)

        run_all = gtk.ToolButton(gtk.STOCK_EXECUTE)
        run_all.connect("clicked", self.__run_all)
        run_all.set_tooltip_markup('<b>Run All: </b> Run dataset through all'
            + ' unexecuted modules in pipeline.')
        toolbar.insert(run_all, 0)

        run_next = gtk.ToolButton(gtk.STOCK_GO_FORWARD)
        run_next.connect("clicked", self.__run_next)
        run_next.set_tooltip_markup('<b>Run Next: </b> Run dataset through'
            + ' next module in pipeline.')
        toolbar.insert(run_next, 1)

        space = gtk.SeparatorToolItem()
        space.set_expand(False)
        space.set_draw(True)
        toolbar.insert(space, 2)

        new = gtk.ToolButton(gtk.STOCK_NEW)
        new.connect("clicked", self.__insert_module)
        new.set_tooltip_markup('<b>Insert Module: </b> Insert module'
            + ' from library.')
        toolbar.insert(new, 3)
        
        edit = gtk.ToolButton(gtk.STOCK_EDIT)
        edit.connect("clicked", self.printt)
        edit.set_tooltip_markup('<b>Edit Module: </b> Locally edit'
            + ' module parameters.')
        toolbar.insert(edit, 4)
        
        delete = gtk.ToolButton(gtk.STOCK_DELETE)
        delete.connect("clicked", self.__delete_last_module)
        delete.set_tooltip_markup('<b>Delete Module: </b>Delete last processed module.')
        toolbar.insert(delete, 5)

        swH = gtk.ScrolledWindow()
        swH.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        """ Treestore
        
        Columns:
        0 - Id
        1 - stage
        2 - state (pixbuf)
        3 - function
        4 - name
        5 - suffix
        6 - path
        7 - keepinter
        8 - sensitive
        """
        
        self.treestore = gtk.TreeStore(int, int, gtk.gdk.Pixbuf, str, str, str, str, bool, bool)
        self.populate_treestore()
        self.treeview = gtk.TreeView(self.treestore)
        self.treeselection = self.treeview.get_selection()

        stage_column = gtk.TreeViewColumn('Stage')
        state_column = gtk.TreeViewColumn('State')
        function_column = gtk.TreeViewColumn('Function')
        module_column = gtk.TreeViewColumn('Module')
        suffix_column = gtk.TreeViewColumn('Suffix')

        self.treeview.append_column(stage_column)
        self.treeview.append_column(state_column)
        self.treeview.append_column(function_column)
        self.treeview.append_column(module_column)
        self.treeview.append_column(suffix_column)

        cell0 = gtk.CellRendererText()
        cell1 = gtk.CellRendererPixbuf()
        cell2 = gtk.CellRendererText()
        cell3 = gtk.CellRendererText()
        cell4 = gtk.CellRendererText()

        stage_column.pack_start(cell0, False)
        stage_column.add_attribute(cell0, 'text', 1)
        state_column.pack_start(cell1, False)
        state_column.add_attribute(cell1, "pixbuf", 2)


        function_column.pack_start(cell2, False)
        function_column.add_attribute(cell2, 'text', 3)

        module_column.pack_start(cell3, False)
        module_column.add_attribute(cell3, 'text', 4)

        suffix_column.pack_start(cell4, False)
        suffix_column.add_attribute(cell4, 'text', 5)

        # Intermediaries

        self.cell3 = gtk.CellRendererToggle()
        self.cell3.set_radio(False)
        self.cell3.set_activatable(True)
        self.cell3.set_active(True)
        self.cell3.set_sensitive(True)
        self.cell3.set_visible(True)
        self.cell3.connect('toggled', self.toggled)
        self.cell3.set_alignment(xalign=0,yalign=0.5)

        self.column3 = gtk.TreeViewColumn('Keep Intermediaries', self.cell3)
        self.column3.set_fixed_width(6)
        self.column3.add_attribute(self.cell3, 'active', 7)
        self.column3.add_attribute(self.cell3, 'activatable', 8)
        self.column3.add_attribute(self.cell3, 'sensitive', 8)

        self.treeview.append_column(self.column3)        

        # Popup menu

        popup_menu = gtk.Menu()

        new = gtk.MenuItem("Insert Module")
        new.connect("activate", self.__insert_module)
        new.show()

        edit = gtk.MenuItem("Edit Module...")
        edit.connect("activate", self.printt)
        edit.show()

        delete = gtk.MenuItem("Delete Module")
        delete.connect("activate", self.__delete_module)
        delete.show()

        popup_menu.append(new)
        popup_menu.append(edit)
        popup_menu.append(delete)

        self.treeview.connect("button-press-event",
                              self.menu_activated, popup_menu)
        
        swH.add(self.treeview)

        main_vbox.pack_start(filemenubox, False, True, 0)
        main_vbox.pack_start(toolbar, False, True, 0)
        main_vbox.pack_start(swH, True, True, 0)
        window.add(main_vbox)

        window.show_all()

    def printt(self, widget, data=None):
        print("printt")
    
    def menu_activated(self, widget, event, menu):
        # right click
        if event.button == 3:
            menu.popup(None, None, None, event.button,
                       event.get_time(), data=None)
            pass
    
    def toggled(self, widget, data=None):
        mod, iterr = self.treeselection.get_selected()
        if iterr is not None and self.treestore.get_value(iterr, 8) and self.treestore.get_value(iterr, 1) != 1:
            op = Open()
            module = op.open_module(self.treestore.get_value(iterr, 6))
            module.keepinter = not self.treestore.get_value(iterr, 7)
            self.treestore.set(iterr, 7, module.keepinter)
            mods = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
            mods.update_module(module.Id, 
                module.stage, 
                module.state, 
                module.function, 
                module.name, 
                module.suffix, 
                module.filename, 
                module.keepinter, 
                module.sensitive)
            module.save()
            mods.close()
