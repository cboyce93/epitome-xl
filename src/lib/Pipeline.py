#!/usr/bin/env python

import pygtk
import gtk
import dill
import time
import datetime
from subprocess import Popen, call
pygtk.require('2.0')
import gobject
from gtk import gdk

import sys
sys.path += ['src']

from util.db import DB
import util.username as util_user

import pdb

class Pipeline:

    def __init__(self, Id, project, name, description, directory, select_data_treestore):
        self.Id = Id
        self.project = project
        self.project_path = directory
        self.name = name
        self.desc = description
        self.select_data_treestore = select_data_treestore
        self.dataset = None
        self.dataset_directories = []
        self.current_dataset = None
        self.date_created = str(datetime.datetime.fromtimestamp(time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        self.path = directory + "/pipelines/" + name + "/"
        call(['mkdir', '-p', self.path])
        self.filepath = self.path + self.name + ".pipe"
        self.module_index = 0
        # create unique Id for modules for DB reference
        self.Id_gen = 1
        self.save()
        self.duplicated = False
        # is current dataset in use by another pipeline?
        self.is_current_dataset_in_use = False

    @property
    def select_data_treestore(self):
        return self.__select_data_treestore

    @select_data_treestore.setter
    def select_data_treestore(self, select_data_treestore):
        self.__select_data_treestore = select_data_treestore

    def save(self):
        self.ser_treestore = []
        self.select_data_treestore.foreach(self.serialize_select_data_treestore, None)
        if self.dataset is not None:        
            self.dataset_old = self.dataset[:]
        self.dataset = []
        self.dataset_directories = []
        self.select_data_treestore.foreach(self.set_dataset, None)
        if self.current_dataset is None or self.current_dataset == self.dataset_old[:]:
            self.current_dataset = self.dataset[:]
        filehandler = open(self.filepath, 'wb')
        dill.dump(self, filehandler)
        pipes = DB(self.project_path, self.project, "pipelines")
        if len(pipes.search_for_pipeline(self.Id)) == 0:
            pipes.pipeline_entry(self.Id, self.name, self.desc, util_user.get_username(), self.date_created, self.filepath)
        else:
            pipes.update_pipeline(self.Id, self.name, self.desc, util_user.get_username(), self.date_created, self.filepath)
        pipes.close()

    def serialize_select_data_treestore(self, model, path, iter, data=None):
        tup = (path, len(path))
        for i in range(0, 5):
            tup += (self.select_data_treestore.get_value(iter, i),)
        self.ser_treestore.append(tup)
        
    def set_dataset(self, model, path, iterr, data=None):
        if model.get_value(iterr, 3) and len(path) == 6:
            filename = model.get_value(iterr, 0)
            #filename_wo_ext = filename.split(".")
            self.dataset.append(model.get_value(iterr, 2) + filename)
        elif model.get_value(iterr, 3) and len(path) == 5:
            filepath = str(self.project_path) + "/"
            current_path = ()
            for index, i in enumerate(path):
                current_path += (i,)
                if index >= 1:
                    current_iter = model.get_iter(current_path)
                    filepath += model.get_value(current_iter, 0)
            self.dataset_directories.append(filepath)
                

