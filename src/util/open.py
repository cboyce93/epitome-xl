#!/usr/bin/env python

from operator import itemgetter, attrgetter, methodcaller

import pygtk
import gtk
import dill
import time
import datetime
pygtk.require('2.0')
import gobject
from gtk import gdk


class Open():

    def open_project(self, filepath):
        filehandler = open(filepath, 'rb')
        project = dill.load(filehandler)
        project.select_data_treestore = self.reconstruct(project.ser_treestore)
        return project
        
    def open_pipeline(self, filepath):
        filehandler = open(filepath, 'rb')
        pipeline = dill.load(filehandler)
        pipeline.select_data_treestore = self.reconstruct(pipeline.ser_treestore)
        return pipeline

    def open_module(self, filepath):
        filehandler = open(filepath, 'rb')
        module = dill.load(filehandler)
        return module

    def reconstruct(self, arr):
        treestore = gtk.TreeStore(str, str, str, bool, bool)
        treestore.clear()
        arr = sorted(arr, key=itemgetter(1))
        for i in range(0, len(arr)):
            path, len_path, str1, str2, str3, bool1, bool2 = arr[i]
            if len_path == 1:
                treestore.append(None, [str1, str2, str3, bool1, bool2])
            if len_path >= 2:
                path = tuple(path[0:len_path - 1])
                treestore.append(treestore.get_iter(path), [
                                 str1, str2, str3, bool1, bool2])
        return treestore
