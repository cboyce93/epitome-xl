#!/usr/bin/env python

import subprocess
import pdb
def send_email(emails, module_name, pipeline_name, completion_time):
    #pdb.set_trace()
    emails = emails.split()
    if len(emails) > 0:
        fromm = "epitome-no-reply@cannlab.ca"
        subject = "EPItome-xl: " + module_name + " complete."
        if completion_time > 3600:
            hours = int(completion_time / 3600)
            minutes = int((completion_time - (hours * 3600))/60)
            seconds = int(completion_time - (hours * 3600) - (minutes * 60))
            time_str = "{0:d} hours {1:d} minutes {2:d} seconds".format(hours, minutes, seconds)       
        elif completion_time > 60:
            minutes = int(completion_time / 60)
            seconds = int(completion_time - (60 * minutes))
            time_str = "{0:d} minutes {1:d} seconds".format(minutes, seconds)
        else:
            seconds = int(completion_time)
            time_str = "{0:d} seconds".format(seconds) 
        
        body = str(module_name + " completed successfully in " +
            time_str + ".\n\n"
            "***This is an auto-generated message from EPItome-xl.***")
        
        for email in emails:
            p1 = subprocess.Popen(['echo', body], stdout=subprocess.PIPE)
            p2 = subprocess.Popen(['mail', '-r', fromm, '-s', subject, email], stdin=p1.stdout, stdout=subprocess.PIPE)
    
