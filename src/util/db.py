import sqlite3
import subprocess
import time
import datetime


class DB:

    def __init__(self, path, project_name, table):
        subprocess.call(["mkdir", "-p", path + "/bin/"])
        self.conn = sqlite3.connect(path + "/bin/" + project_name + ".db")
        self.c = self.conn.cursor()
        self.table = table
        self.create_table()

    def create_table(self):
        if self.table == "files":
            self.c.execute("""CREATE TABLE IF NOT EXISTS files (
                                        id integer PRIMARY KEY,
                                        grp text NOT NULL,
                                        participant integer,
                                        task text NOT NULL,
                                        path text NOT NULL,
                                        pipeline text,
                                        visible integer
                                    )""")
        elif self.table == "pipelines":
            self.c.execute("""CREATE TABLE IF NOT EXISTS pipelines (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL,
                                        desc text NOT NULL,
                                        edited text NOT NULL,
                                        lastuser text NOT NULL,
                                        created text NOT NULL,
                                        filepath text NOT NULL
                                    )""")
        else:
            # pipeline tables to store modules
            cmd = "CREATE TABLE IF NOT EXISTS " + str(self.table) + """ (
                                        id integer PRIMARY KEY,
                                        stage integer NOT NULL,
                                        state integer NOT NULL,
                                        function text NOT NULL,
                                        name text NOT NULL,
                                        suffix text NOT NULL,
                                        path text NOT NULL,
                                        keepinter integer NOT NULL,
                                        sensitive integer NOT NULL
                                    )"""
            self.c.execute(cmd)


    def entry(self, Id, group, participant, task, path, pipeline, visible):
        self.c.execute("INSERT INTO files(id, grp, participant, task, path, pipeline, visible) VALUES (?, ?, ?, ?, ?, ?, ?)",
                       (Id, group, participant, task, path, pipeline, visible))
        self.conn.commit()
    
    def pipeline_entry(self, Id, name, desc, lastuser, created, filepath):
        edited = str(datetime.datetime.fromtimestamp(
            time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        self.c.execute("INSERT INTO pipelines(Id, name, desc, edited, lastuser, created, filepath) VALUES (?, ?, ?, ?, ?, ?, ?)",
                       (Id, name, desc, edited, lastuser, created, filepath))
        self.conn.commit()
        
    def delete_pipeline_entry(self, Id):
        self.c.execute("DELETE FROM pipelines WHERE Id=?", (Id,))
        self.conn.commit()   
        
    def search_for_pipeline(self, Id):
        self.c.execute("SELECT * FROM pipelines WHERE id IN ('" + str(Id) + "')")
        return self.c.fetchall()
    
    def update_pipeline(self, Id, name, desc, lastuser, edited, filepath):
        edited = str(datetime.datetime.fromtimestamp(
            time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        self.c.execute("UPDATE pipelines SET name=? WHERE Id=?", (name, Id))
        self.c.execute("UPDATE pipelines SET desc=? WHERE Id=?", (desc, Id))
        self.c.execute("UPDATE pipelines SET edited=? WHERE Id=?", (edited, Id))
        self.c.execute("UPDATE pipelines SET filepath=? WHERE Id=?", (filepath, Id))
        self.c.execute("UPDATE pipelines SET lastuser=? WHERE Id=?", (lastuser, Id))
        self.conn.commit()
        
    
    def module_entry(self, Id, stage, state, function, name, suffix, path, keepinter, sensitive):
        self.c.execute("INSERT INTO " + self.table + "(Id, stage, state, function, name, suffix, path, keepinter, sensitive) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                       (Id, stage, state, function, name, suffix, path, keepinter, sensitive))
        self.conn.commit()
        
    def delete_module_entry(self, Id):
        self.c.execute("DELETE FROM " + self.table + " WHERE Id=?", (Id,))
        self.conn.commit()   
        
    def search_for_module(self, stage):
        self.c.execute("SELECT * FROM " + self.table + " WHERE id IN ('" + str(Id) + "')")
        return self.c.fetchall()
    
    def update_module(self, Id, stage, state, function, name, suffix, path, keepinter, sensitive):
        self.c.execute("UPDATE " + self.table + " SET stage=? WHERE Id=?", (stage, Id))
        self.c.execute("UPDATE " + self.table + " SET state=? WHERE Id=?", (state, Id))
        self.c.execute("UPDATE " + self.table + " SET function=? WHERE Id=?", (function, Id))
        self.c.execute("UPDATE " + self.table + " SET name=? WHERE Id=?", (name, Id))
        self.c.execute("UPDATE " + self.table + " SET suffix=? WHERE Id=?", (suffix, Id))
        self.c.execute("UPDATE " + self.table + " SET path=? WHERE Id=?", (path, Id))
        self.c.execute("UPDATE " + self.table + " SET keepinter=? WHERE Id=?", (keepinter, Id))
        self.c.execute("UPDATE " + self.table + " SET sensitive=? WHERE Id=?", (sensitive, Id))
        self.conn.commit()
    
    def read(self):
        self.c.execute("SELECT * from " + str(self.table))
        return self.c.fetchall()

    def close(self):
        self.conn.close()
