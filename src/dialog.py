'''
Author: Cameron Boyce

COGNITION & AGING NEUROSCIENCE
AND NEUROINTERVENTION LABS

YORK UNIVERSITY

2017
'''

#!/usr/bin/env python

import pygtk
import gtk
pygtk.require('2.0')


class Dialog():

    ''' Warning Dialog

    Open warning dialog populated with message passed as single argument
    '''

    def on_warn(self, message):
        md = gtk.MessageDialog( parent=None,
                                flags=gtk.DIALOG_DESTROY_WITH_PARENT,
                                type=gtk.MESSAGE_WARNING,
                                buttons=gtk.BUTTONS_OK,
                                message_format=message)
        self.warn_response = md.run()
        md.destroy()

    ''' Info Dialog
        
        Open info dialog populated with message passed as single argument
        '''

    def on_info(self, message):
        md = gtk.MessageDialog( parent=None,
                                flags=gtk.DIALOG_DESTROY_WITH_PARENT,
                                type=gtk.MESSAGE_INFO,
                                buttons=gtk.BUTTONS_OK,
                                message_format=message)
        md.run()
        md.destroy()
    
    def on_confirmation(self, message, title):
        label = gtk.Label(message)
        self.dialog = gtk.Dialog(title,
                                 None,
                                 gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                                 (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                  gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        self.dialog.set_border_width(10)
        self.dialog.vbox.pack_start(label, True, True, 8)
        label.show()
        self.response = self.dialog.run()
        self.dialog.destroy()
        return self.response

