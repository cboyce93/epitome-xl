#!/usr/bin/env python

import pygtk
import gtk
import dill
import pango
import os
import subprocess
import time
pygtk.require('2.0')
from os.path import expanduser

from lib.Project import Project
from pipeline_editor import PipelineEditor
from lib.Module import Module
from dialog import Dialog
from util.library import Library
from util.open import Open
from cmd_editor import CmdEditor

import pdb


class LibraryManager():

    def exit(self, widget, data=None):
        self.window.destroy()

    def on_NEW_LIBRARY_activated(self, widget, data=None):

        self.new_library_window = gtk.Window()
        self.new_library_window.connect("delete_event", self.on_delete_event)
        self.new_library_window.set_title('New Library')
        self.new_library_window.set_position(gtk.WIN_POS_CENTER)
        self.new_library_window.set_size_request(280, 100)
        self.new_library_window.set_border_width(10)
        self.new_library_window.set_resizable(False)

        self.new_library_window_vbox = gtk.VBox(False, 0)

        self.nl_hbox = gtk.HBox(False, 0)

        self.label = gtk.Label("Library Name:")
        self.label.set_alignment(0, 0.5)
        self.nl_hbox.pack_start(self.label, False, False, 10)
        self.name = gtk.Entry()
        self.nl_hbox.pack_start(self.name, True, True, 0)
        self.new_library_window_vbox.pack_start(self.nl_hbox, False, False, 10)

        # Create HBox wrapper for lower content
        self.new_library_window_hbox = gtk.HBox(True, 3)
        self.cancel = gtk.Button("Cancel")
        self.cancel.connect(
            "clicked", self.on_cancel, self.new_library_window)

        #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
        self.create = gtk.Button("Create")

        self.create.connect("clicked", self.on_create_library_clicked)

        # pack buttons
        self.new_library_window_hbox.add(self.cancel)
        self.new_library_window_hbox.add(self.create)

        self.new_library_window_vbox.pack_start(
            self.new_library_window_hbox, False, False, 2)

        self.new_library_window.add(self.new_library_window_vbox)
        self.new_library_window.show_all()

    def on_delete_event(self, widget, data=None):
        widget.destroy()

    def on_cancel(self, widget, window):
        window.destroy()
    
    def on_new_library_window_cancel_clicked(self, widget, data=None):
        self.save_library()
        self.new_library_window.destroy()

    def on_create_library_clicked(self, widget, data=None):
        library = self.name.get_text()
        if self.library_liststore.iter_n_children(None) == 0:
            next_index = 0        
        else:
            next_index = int(self.library_liststore.get_value(
                self.library_liststore.iter_nth_child(None, 
                self.library_liststore.iter_n_children(None) - 1), 0)) + 1
        self.library_liststore.append([next_index, library])
        ls = Library("library")
        ls.library_entry(next_index, library)
        ls.close()
        self.new_library_window.destroy()

    def on_EDIT_LIBRARY_activated(self, widget, data=None):
        model, iterr = self.library_treeview.get_selection().get_selected()
        iterr = self.sorted_library_liststore.convert_iter_to_child_iter(None, iterr)
        name = self.library_liststore.get_value(iterr, 1)
        
        self.new_library_window = gtk.Window()
        self.new_library_window.connect("delete_event", self.on_delete_event)
        self.new_library_window.set_title('Edit ' + name)
        self.new_library_window.set_position(gtk.WIN_POS_CENTER)
        self.new_library_window.set_size_request(280, 100)
        self.new_library_window.set_border_width(10)
        self.new_library_window.set_resizable(False)

        self.new_library_window_vbox = gtk.VBox(False, 0)

        self.nl_hbox = gtk.HBox(False, 0)

        self.label = gtk.Label("Library Name:")
        self.label.set_alignment(0, 0.5)
        self.nl_hbox.pack_start(self.label, False, False, 10)
        self.name = gtk.Entry()
        self.name.set_text(name)
        self.nl_hbox.pack_start(self.name, True, True, 0)
        self.new_library_window_vbox.pack_start(self.nl_hbox, False, False, 10)

        # Create HBox wrapper for lower content
        self.new_library_window_hbox = gtk.HBox(True, 3)
        self.cancel = gtk.Button("Cancel")
        self.cancel.connect(
            "clicked", self.on_cancel, self.new_library_window)

        #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
        self.save = gtk.Button("Save")

        self.save.connect("clicked", self.on_save_library_clicked)

        # pack buttons
        self.new_library_window_hbox.add(self.cancel)
        self.new_library_window_hbox.add(self.save)

        self.new_library_window_vbox.pack_start(
            self.new_library_window_hbox, False, False, 2)

        self.new_library_window.add(self.new_library_window_vbox)
        self.new_library_window.show_all()
    
    def on_save_library_clicked(self, widget, data=None):
        m, i = self.library_selection.get_selected()
        i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
        ls = Library("library")
        name = self.name.get_text()
        ls.update_library_entry(self.library_liststore.get_value(i, 0), name)
        ls.close()
        self.library_liststore.set(i, 1, name)
        self.new_library_window.destroy()
        md = Dialog()
        md.on_info(name + " updated successfully.")
    
    def on_DELETE_LIBRARY_activated(self, widget, data=None):
        m, i = self.library_selection.get_selected()
        i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
        md = Dialog()
        name = self.library_liststore.get_value(i, 1)
        response = md.on_confirmation("Delete " + name +
            " and all associated modules?", "Delete " + name)
        if response == gtk.RESPONSE_ACCEPT:
            response = md.on_confirmation("Are you sure? Deleted modules cannot be recovered.", "Delete " + name)
            if response == gtk.RESPONSE_ACCEPT:
                self.category_iters = []
                self.function_iters = []
                self.module_iters= []
                self.cat = Library("category")
                self.sorted_cat_liststore.foreach(self.remove_category, None)
                self.cat.close()
                self.lib = Library("library")
                self.lib.delete_library_entry(self.library_liststore.get_value(i, 0))
                self.lib.close()
                self.library_liststore.remove(i)
                for iterr in self.category_iters:
                    self.category_liststore.remove(iterr)
                for iterr in self.function_iters:
                    self.function_liststore.remove(iterr)
                for iterr in self.module_iters:
                    self.module_liststore.remove(iterr)
                self.update_module_textview(None, None)
                md = Dialog()
                md.on_info(name + " deleted successfully.")

    def remove_category(self, model, path, iterr, data):
        self.category_selection.select_iter(iterr)
        iterr = model.convert_iter_to_child_iter(None, iterr)
        model1 = model.get_model()
        iterr = model1.convert_iter_to_child_iter(iterr)
        model2 = model1.get_model()
        self.func = Library("function")
        self.sorted_function_liststore.foreach(self.remove_function, None)
        self.func.close()
        self.category_iters.append(iterr)
        self.cat.delete_category_entry(model2.get_value(iterr, 0))
    
    def remove_function(self, model, path, iterr, data):
        self.function_selection.select_iter(iterr)
        iterr = model.convert_iter_to_child_iter(None, iterr)
        model1 = model.get_model()
        iterr = model1.convert_iter_to_child_iter(iterr)
        model2 = model1.get_model()
        self.sorted_module_liststore.foreach(self.remove_module, None)
        self.function_iters.append(iterr)
        self.func.delete_function_entry(model2.get_value(iterr, 0))
    
    def remove_module(self, model, path, iterr, data):
        iterr = model.convert_iter_to_child_iter(None, iterr)
        model1 = model.get_model()
        iterr = model1.convert_iter_to_child_iter(iterr)
        model2 = model1.get_model()
        filepath = model2.get_value(iterr, 4)
        self.module_iters.append(iterr)
        subprocess.call(['rm', filepath])

    
    def on_NEW_CATEGORY_activated(self, widget, data=None):

        if self.library_liststore.iter_n_children(None) > 0:

            self.add_category_window = gtk.Window()
            self.add_category_window.connect(
                "delete_event", self.on_delete_event)
            self.add_category_window.set_title('New Category')
            self.add_category_window.set_size_request(320, 132)
            self.add_category_window.set_border_width(10)
            self.add_category_window.set_resizable(False)

            self.add_category_window_vbox = gtk.VBox(False, 0)

            self.ac_hbox = gtk.HBox(False, 0)

            self.label = gtk.Label("    Library Name: ")
            self.label.set_alignment(0, 0.5)
            self.ac_hbox.pack_start(self.label, False, False, 10)
            self.library_combobox = gtk.ComboBox(self.library_liststore)
            self.library_combobox.connect(
                "changed", self.on_library_combobox_changed)

            m, i = self.library_selection.get_selected()
            if i is not None:
                i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
                self.library_combobox.set_active_iter(i)
            else:
                md = Dialog()
                md.on_warn("Please select a library.")
                return None
            cell = gtk.CellRendererText()

            self.library_combobox.pack_start(cell, True)
            self.library_combobox.add_attribute(cell, 'text', 1)
            self.ac_hbox.pack_start(self.library_combobox, True, True, 0)
            self.add_category_window_vbox.pack_start(
                self.ac_hbox, False, False, 4)

            self.ac_hbox2 = gtk.HBox(False, 0)

            self.label = gtk.Label("Category Name: ")
            self.label.set_alignment(0, 0.5)
            self.ac_hbox2.pack_start(self.label, False, False, 10)
            self.name = gtk.Entry()
            self.ac_hbox2.pack_start(self.name, True, True, 0)
            self.add_category_window_vbox.pack_start(
                self.ac_hbox2, False, False, 4)

            # Create HBox wrapper for lower content
            self.add_category_window_hbox = gtk.HBox(True, 3)
            self.cancel = gtk.Button("Close")
            self.cancel.connect(
                "clicked", self.on_cancel, self.add_category_window)

            #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
            self.add = gtk.Button("Add Category")

            self.add.connect("clicked", self.on_add_category_clicked)

            # pack buttons
            self.add_category_window_hbox.add(self.cancel)
            self.add_category_window_hbox.add(self.add)

            self.add_category_window_vbox.pack_start(
                self.add_category_window_hbox, False, False, 6)

            self.add_category_window.add(self.add_category_window_vbox)
            self.add_category_window.show_all()

        else:
            md = Dialog()
            md.on_warn(
                "Please create at least one library prior to adding categories.")

    def on_add_category_clicked(self, widget, data=None):
        library = self.library_liststore.get_value(
            self.library_combobox.get_active_iter(), 1)
        category = self.name.get_text()
        if self.category_liststore.iter_n_children(None) == 0:
            next_index = 0        
        else:
            next_index = int(self.category_liststore.get_value(
                self.category_liststore.iter_nth_child(None, 
                self.category_liststore.iter_n_children(None) - 1), 0)) + 1
        self.category_liststore.append([next_index, library, category])
        cs = Library("category")
        cs.category_entry(next_index, library, category)
        cs.close()
        self.add_category_window.destroy()

    def on_add_category_window_cancel_clicked(self, widget, data=None):
        self.add_category_window.destroy()

    def on_library_combobox_changed(self, combobox, data=None):
        i = self.sorted_library_liststore.convert_child_iter_to_iter(None, combobox.get_active_iter())
        if i is not None:
            self.library_selection.select_iter(i)

    def on_EDIT_CATEGORY_activated(self, widget, data=None):
        if self.library_liststore.iter_n_children(None) > 0:
            model, iterr = self.category_treeview.get_selection().get_selected()
            self.category_selection.select_iter(iterr)
            iterr = self.sorted_cat_liststore.convert_iter_to_child_iter(None, iterr)
            iterr = self.catfilter.convert_iter_to_child_iter(iterr)
            name = self.category_liststore.get_value(iterr, 2)
            
            self.add_category_window = gtk.Window()
            self.add_category_window.connect(
                "delete_event", self.on_delete_event)
            self.add_category_window.set_title('Edit ' + name)
            self.add_category_window.set_size_request(320, 132)
            self.add_category_window.set_border_width(10)
            self.add_category_window.set_resizable(False)

            self.add_category_window_vbox = gtk.VBox(False, 0)

            self.ac_hbox = gtk.HBox(False, 0)

            self.label = gtk.Label("    Library Name: ")
            self.label.set_alignment(0, 0.5)
            self.ac_hbox.pack_start(self.label, False, False, 10)
            self.library_combobox = gtk.ComboBox(self.library_liststore)
            self.library_combobox.connect(
                "changed", self.on_library_combobox_changed)

            m, i = self.library_selection.get_selected()
            if i is not None:
                i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
                self.library_combobox.set_active_iter(i)
            else:
                md = Dialog()
                md.on_warn("Please select a library.")
                return None
            cell = gtk.CellRendererText()

            self.library_combobox.pack_start(cell, True)
            self.library_combobox.add_attribute(cell, 'text', 1)
            self.ac_hbox.pack_start(self.library_combobox, True, True, 0)
            self.add_category_window_vbox.pack_start(
                self.ac_hbox, False, False, 4)

            self.ac_hbox2 = gtk.HBox(False, 0)

            self.label = gtk.Label("Category Name: ")
            self.label.set_alignment(0, 0.5)
            self.ac_hbox2.pack_start(self.label, False, False, 10)
            self.name = gtk.Entry()
            self.name.set_text(name)
            self.ac_hbox2.pack_start(self.name, True, True, 0)
            self.add_category_window_vbox.pack_start(
                self.ac_hbox2, False, False, 4)

            # Create HBox wrapper for lower content
            self.add_category_window_hbox = gtk.HBox(True, 3)
            self.cancel = gtk.Button("Close")
            self.cancel.connect(
                "clicked", self.on_cancel, self.add_category_window)

            #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
            self.save = gtk.Button("Save")

            self.save.connect("clicked", self.on_save_category_clicked)

            # pack buttons
            self.add_category_window_hbox.add(self.cancel)
            self.add_category_window_hbox.add(self.save)

            self.add_category_window_vbox.pack_start(
                self.add_category_window_hbox, False, False, 6)

            self.add_category_window.add(self.add_category_window_vbox)
            self.add_category_window.show_all()

        else:
            md = Dialog()
            md.on_warn(
                "Please create at least one library prior to adding categories.")
        

    def on_save_category_clicked(self, widget, date=None):
        m, i = self.category_selection.get_selected()
        i = self.sorted_cat_liststore.convert_iter_to_child_iter(None, i)
        i = self.catfilter.convert_iter_to_child_iter(i)
        cs = Library("category")
        name = self.name.get_text()
        cs.update_category_entry(self.category_liststore.get_value(i, 0),
            self.category_liststore.get_value(i, 1),
            name)
        cs.close()
        self.category_liststore.set(i, 2, name)
        self.add_category_window.destroy()
        md = Dialog()
        md.on_info(name + " updated successfully.")
    
    def on_DELETE_CATEGORY_activated(self, widget, data=None):
        m, i = self.category_selection.get_selected()
        i = self.sorted_cat_liststore.convert_iter_to_child_iter(None, i)
        i = self.catfilter.convert_iter_to_child_iter(i)
        md = Dialog()
        name = self.category_liststore.get_value(i, 2)
        response = md.on_confirmation("Delete " + name +
            " and all associated modules?", "Delete " + name)
        if response == gtk.RESPONSE_ACCEPT:
            response = md.on_confirmation("Are you sure? Deleted modules cannot be recovered.", "Delete " + name)
            if response == gtk.RESPONSE_ACCEPT:
                self.function_iters = []
                self.module_iters= []
                self.func = Library("function")
                self.sorted_function_liststore.foreach(self.remove_function, None)
                self.func.close()
                self.cat = Library("category")
                self.cat.delete_category_entry(self.category_liststore.get_value(i, 0))
                self.cat.close()
                self.category_liststore.remove(i)
                for iterr in self.function_iters:
                    self.function_liststore.remove(iterr)
                for iterr in self.module_iters:
                    self.module_liststore.remove(iterr)
                self.update_module_textview(None, None)
                md = Dialog()
                md.on_info(name + " deleted successfully.")
    
    
    def on_NEW_FUNCTION_activated(self, widget, data=None):
        if self.library_liststore.iter_n_children(None) > 0 and self.category_liststore.iter_n_children(None) > 0:
            self.nf_window = gtk.Window()
            self.nf_window.connect("delete_event", self.on_delete_event)
            self.nf_window.set_title('New Function')
            self.nf_window.set_size_request(320, 170)
            self.nf_window.set_position(gtk.WIN_POS_CENTER)
            self.nf_window.set_border_width(10)
            self.nf_window.set_resizable(False)

            self.nf_window_vbox = gtk.VBox(False, 0)

            self.nf_hbox = gtk.HBox(False, 0)

            self.nf_label = gtk.Label("    Select Library: ")
            self.nf_label.set_alignment(0, 0.5)
            self.nf_hbox.pack_start(self.nf_label, False, False, 10)
            self.nf_library_combobox = gtk.ComboBox(self.library_liststore)
            self.nf_library_combobox.connect(
                "changed", self.on_library_combobox_changed)

            m, i = self.library_selection.get_selected()
            if i is not None:
                i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
                self.nf_library_combobox.set_active_iter(i)
            else: 
                md = Dialog()
                md.on_warn("Please select a library.")
                return None

            self.nf_cell = gtk.CellRendererText()

            self.nf_library_combobox.pack_start(self.nf_cell, True)
            self.nf_library_combobox.add_attribute(self.nf_cell, 'text', 1)
            self.nf_hbox.pack_start(self.nf_library_combobox, True, True, 0)
            self.nf_window_vbox.pack_start(self.nf_hbox, False, False, 4)

            self.nf_hbox2 = gtk.HBox(False, 0)

            self.nf_label = gtk.Label("Select Category: ")
            self.nf_label.set_alignment(0, 0.5)
            self.nf_hbox2.pack_start(self.nf_label, False, False, 10)
            self.nf_category_combobox = gtk.ComboBox(self.catfilter)
            self.nf_category_combobox.connect(
                "changed", self.on_category_combobox_changed)

            m, i = self.category_selection.get_selected()
            if i is not None:
                i = self.sorted_cat_liststore.convert_iter_to_child_iter(None, i)
                self.nf_category_combobox.set_active_iter(i)
            else: 
                md = Dialog()
                md.on_warn("Please select a category.")
                return None

            cell2 = gtk.CellRendererText()

            self.nf_category_combobox.pack_start(cell2, True)
            self.nf_category_combobox.add_attribute(cell2, 'text', 2)
            self.nf_hbox2.pack_start(self.nf_category_combobox, True, True, 0)
            self.nf_window_vbox.pack_start(self.nf_hbox2, False, False, 4)

            self.nf_hbox3 = gtk.HBox(False, 0)

            self.label3 = gtk.Label(" Function Name: ")
            self.label3.set_alignment(0, 0.5)
            self.nf_hbox3.pack_start(self.label3, False, False, 10)
            self.function_name = gtk.Entry()
            self.nf_hbox3.pack_start(self.function_name, True, True, 0)
            self.nf_window_vbox.pack_start(self.nf_hbox3, False, False, 4)

            # Create HBox wrapper for lower content
            self.nf_lower_hbox = gtk.HBox(True, 3)
            self.nf_cancel = gtk.Button("Close")
            self.nf_cancel.connect(
                "clicked", self.on_cancel, self.nf_window)

            #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
            self.add = gtk.Button("Add Function")

            self.add.connect("clicked", self.on_new_function_clicked)

            # pack buttons
            self.nf_lower_hbox.add(self.nf_cancel)
            self.nf_lower_hbox.add(self.add)

            self.nf_window_vbox.pack_start(self.nf_lower_hbox, False, False, 6)

            self.nf_window.add(self.nf_window_vbox)
            self.nf_window.show_all()

        else:
            md = Dialog()
            md.on_warn(
                "Please create at least one library category prior to adding function subcategories.")

    def on_category_combobox_changed(self, combobox, data=None):
        i = self.sorted_cat_liststore.convert_child_iter_to_iter(None, combobox.get_active_iter())
        self.category_selection.select_iter(i)

    def on_new_function_clicked(self, widget, data=None):
        library = self.library_liststore.get_value(self.nf_library_combobox.get_active_iter(), 1)
        category = self.catfilter.get_value(
            self.nf_category_combobox.get_active_iter(), 2) 
        function = self.function_name.get_text()
        if self.function_liststore.iter_n_children(None) == 0:
            next_index = 0        
        else:
            next_index = int(self.function_liststore.get_value(
                self.function_liststore.iter_nth_child(None, 
                self.function_liststore.iter_n_children(None) - 1), 0)) + 1
        self.function_liststore.append([next_index, library, category, function])
        fs = Library("function")
        fs.function_entry(next_index, library, category, function)
        fs.close()
        self.nf_window.destroy()

    def on_new_function_window_cancel_clicked(self, widget, data=None):
        self.nf_window.destroy()

    def refilter_library(self, model, data=None):
        self.catfilter.refilter()
        if self.catfilter.iter_n_children(None) > 0:
            root_iter = self.sorted_cat_liststore.convert_child_iter_to_iter(None, self.catfilter.get_iter_first())
            if root_iter is not None:
                self.category_selection.select_iter(root_iter)

    def refilter_category(self, model, data=None):
        self.functionfilter.refilter()
        if self.functionfilter.iter_n_children(None) > 0:
            root_iter = self.sorted_function_liststore.convert_child_iter_to_iter(None, self.functionfilter.get_iter_first())
            if root_iter is not None:
                self.function_selection.select_iter(root_iter)

    def refilter_function(self, model, data=None):
        self.modulefilter.refilter()
        if self.modulefilter.iter_n_children(None) > 0:
            root_iter = self.sorted_module_liststore.convert_child_iter_to_iter(None, self.modulefilter.get_iter_first())
            if root_iter is not None:
                self.module_selection.select_iter(root_iter)

    def match_func(self, model, iterr, data=None):
        return True

    def on_EDIT_FUNCTION_activated(self, widget, data=None):
        if self.library_liststore.iter_n_children(None) > 0 and self.category_liststore.iter_n_children(None) > 0:
            model, iterr = self.function_treeview.get_selection().get_selected()
            self.function_selection.select_iter(iterr)
            iterr = self.sorted_function_liststore.convert_iter_to_child_iter(None, iterr)
            iterr = self.functionfilter.convert_iter_to_child_iter(iterr)
            name = self.function_liststore.get_value(iterr, 3)
            
            self.nf_window = gtk.Window()
            self.nf_window.connect("delete_event", self.on_delete_event)
            self.nf_window.set_title('Edit ' + name)
            self.nf_window.set_size_request(320, 170)
            self.nf_window.set_position(gtk.WIN_POS_CENTER)
            self.nf_window.set_border_width(10)
            self.nf_window.set_resizable(False)

            self.nf_window_vbox = gtk.VBox(False, 0)

            self.nf_hbox = gtk.HBox(False, 0)

            self.nf_label = gtk.Label("    Select Library: ")
            self.nf_label.set_alignment(0, 0.5)
            self.nf_hbox.pack_start(self.nf_label, False, False, 10)
            self.nf_library_combobox = gtk.ComboBox(self.library_liststore)
            self.nf_library_combobox.connect(
                "changed", self.on_library_combobox_changed)

            m, i = self.library_selection.get_selected()
            if i is not None:
                i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
                self.nf_library_combobox.set_active_iter(i)
            else: 
                md = Dialog()
                md.on_warn("Please select a library.")
                return None

            self.nf_cell = gtk.CellRendererText()

            self.nf_library_combobox.pack_start(self.nf_cell, True)
            self.nf_library_combobox.add_attribute(self.nf_cell, 'text', 1)
            self.nf_hbox.pack_start(self.nf_library_combobox, True, True, 0)
            self.nf_window_vbox.pack_start(self.nf_hbox, False, False, 4)

            self.nf_hbox2 = gtk.HBox(False, 0)

            self.nf_label = gtk.Label("Select Category: ")
            self.nf_label.set_alignment(0, 0.5)
            self.nf_hbox2.pack_start(self.nf_label, False, False, 10)
            self.nf_category_combobox = gtk.ComboBox(self.catfilter)
            self.nf_category_combobox.connect(
                "changed", self.on_category_combobox_changed)

            m, i = self.category_selection.get_selected()
            if i is not None:
                i = self.sorted_cat_liststore.convert_iter_to_child_iter(None, i)
                self.nf_category_combobox.set_active_iter(i)
            else: 
                md = Dialog()
                md.on_warn("Please select a category.")
                return None

            cell2 = gtk.CellRendererText()

            self.nf_category_combobox.pack_start(cell2, True)
            self.nf_category_combobox.add_attribute(cell2, 'text', 2)
            self.nf_hbox2.pack_start(self.nf_category_combobox, True, True, 0)
            self.nf_window_vbox.pack_start(self.nf_hbox2, False, False, 4)

            self.nf_hbox3 = gtk.HBox(False, 0)

            self.label3 = gtk.Label(" Function Name: ")
            self.label3.set_alignment(0, 0.5)
            self.nf_hbox3.pack_start(self.label3, False, False, 10)
            self.function_name = gtk.Entry()
            self.function_name.set_text(name)
            self.nf_hbox3.pack_start(self.function_name, True, True, 0)
            self.nf_window_vbox.pack_start(self.nf_hbox3, False, False, 4)

            # Create HBox wrapper for lower content
            self.nf_lower_hbox = gtk.HBox(True, 3)
            self.nf_cancel = gtk.Button("Close")
            self.nf_cancel.connect(
                "clicked", self.on_cancel, self.nf_window)

            #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
            self.save = gtk.Button("Save")

            self.save.connect("clicked", self.on_save_function_clicked)

            # pack buttons
            self.nf_lower_hbox.add(self.nf_cancel)
            self.nf_lower_hbox.add(self.save)

            self.nf_window_vbox.pack_start(self.nf_lower_hbox, False, False, 6)

            self.nf_window.add(self.nf_window_vbox)
            self.nf_window.show_all()

        else:
            md = Dialog()
            md.on_warn(
                "Please create at least one library category prior to adding function subcategories.")
    
    def on_save_function_clicked(self, widget, date=None):
        m, i = self.function_selection.get_selected()
        i = self.sorted_function_liststore.convert_iter_to_child_iter(None, i)
        i = self.functionfilter.convert_iter_to_child_iter(i)
        fs = Library("function")
        name = self.function_name.get_text()
        fs.update_function_entry(self.function_liststore.get_value(i, 0),
            self.function_liststore.get_value(i, 1),
            self.function_liststore.get_value(i, 2),
            name)
        fs.close()
        self.function_liststore.set(i, 3, name)
        self.nf_window.destroy()
        md = Dialog()
        md.on_info(name + " updated successfully.")
    
    def on_DELETE_FUNCTION_activated(self, widget, data=None):
        m, i = self.function_selection.get_selected()
        i = self.sorted_function_liststore.convert_iter_to_child_iter(None, i)
        i = self.functionfilter.convert_iter_to_child_iter(i)
        md = Dialog()
        name = self.function_liststore.get_value(i, 3)
        response = md.on_confirmation("Delete " + name +
            " and all associated modules?", "Delete " + name)
        if response == gtk.RESPONSE_ACCEPT:
            response = md.on_confirmation("Are you sure? Deleted modules cannot be recovered.", "Delete " + name)
            if response == gtk.RESPONSE_ACCEPT:
                self.module_iters= []
                self.sorted_module_liststore.foreach(self.remove_module, None)
                fs = Library("function")
                fs.delete_function_entry(self.function_liststore.get_value(i, 0))
                fs.close()
                self.function_liststore.remove(i)
                for iterr in self.module_iters:
                    self.module_liststore.remove(iterr)
                self.update_module_textview(None, None)
                md = Dialog()
                md.on_info(name + " deleted successfully.")
    
    
    def on_NEW_MODULE_activated(self, widget, data=None):

        self.create_module_window = gtk.Window()
        self.create_module_window.connect("delete_event", self.on_delete_event)
        self.create_module_window.set_title('New Module')
        #self.create_module_window.set_size_request(440, 594)
        self.create_module_window.set_border_width(16)
        self.create_module_window.set_position(gtk.WIN_POS_CENTER)
        self.create_module_window.set_resizable(False)
        self.create_module_window.set_icon_from_file("img/epitome_icon.png")

        hbox_wrapper = gtk.HBox(False, 30)

        self.create_module_window_vbox = gtk.VBox(False, 0)

        vbox_right = gtk.VBox(False, 0)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Select Library: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(200, 30)
        hbox.pack_start(label, False, False, 10)
        library_combobox = gtk.ComboBox(self.library_liststore)
        library_combobox.connect("changed", self.on_library_combobox_changed)
        cell = gtk.CellRendererText()
        library_combobox.pack_start(cell, True)
        library_combobox.add_attribute(cell, 'text', 1)

        m, i = self.library_selection.get_selected()
        if i is not None:
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            library_combobox.set_active_iter(i)
        else:
            md = Dialog()
            md.on_warn("Please select a library.")
            return None

        hbox.pack_start(library_combobox, True, True, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Select Category: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(200, 30)
        hbox.pack_start(label, False, False, 10)
        category_combobox = gtk.ComboBox(self.catfilter)
        cell = gtk.CellRendererText()
        category_combobox.pack_start(cell, True)
        category_combobox.add_attribute(cell, 'text', 2)

        m, i = self.category_selection.get_selected()
        if i is not None:
            i = self.sorted_cat_liststore.convert_iter_to_child_iter(None, i)
            category_combobox.set_active_iter(i)
        else:
            md = Dialog()
            md.on_warn("Please select a category.")
            return None

        hbox.pack_start(category_combobox, True, True, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Select Function: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(200, 30)
        hbox.pack_start(label, False, False, 10)
        function_combobox = gtk.ComboBox(self.functionfilter)
        cell = gtk.CellRendererText()
        function_combobox.pack_start(cell, True)
        function_combobox.add_attribute(cell, 'text', 3)

        m, i = self.function_selection.get_selected()
        if i is not None:
            i = self.sorted_function_liststore.convert_iter_to_child_iter(None, i)
            function_combobox.set_active_iter(i)
        else:
            md = Dialog()
            md.on_warn("Please select a function.")
            return None

        hbox.pack_start(function_combobox, True, True, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        self.cm_hbox3 = gtk.HBox(False, 0)

        label = gtk.Label("Module Name: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox3.pack_start(label, False, False, 10)
        self.module_name_entry = gtk.Entry()
        self.module_name_entry.set_max_length(30)
        self.cm_hbox3.pack_start(self.module_name_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox3, False, False, 4)

        self.cm_hbox4 = gtk.HBox(False, 0)

        label = gtk.Label("Suffix: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox4.pack_start(label, False, False, 10)
        self.suffix_entry = gtk.Entry()
        self.suffix_entry.set_max_length(1)
        self.cm_hbox4.pack_start(self.suffix_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox4, False, False, 4)

        self.cm_hbox5 = gtk.HBox(False, 0)

        label = gtk.Label("Author: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox5.pack_start(label, False, False, 10)
        self.author_entry = gtk.Entry()
        self.cm_hbox5.pack_start(self.author_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox5, False, False, 4)

        self.cm_hbox7 = gtk.HBox(False, 0)

        label = gtk.Label("Documentation URL: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox7.pack_start(label, False, False, 10)
        self.doc_url_entry = gtk.Entry()
        self.cm_hbox7.pack_start(self.doc_url_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox7, False, False, 4)


        hbox = gtk.HBox(False, 10)
        label = gtk.Label("Description: ")
        label.set_alignment(0, 0.5)
        label.set_size_request(200, 20)
        hbox.pack_start(label, False, False, 0)
        vbox_right.pack_start(hbox, False, False, 4)

        self.cm_hbox6 = gtk.HBox(False, 0)
        self.swH = gtk.ScrolledWindow()
        self.swH.set_size_request(400, 200)
        self.swH.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.desc_entry = gtk.TextView()
        self.desc_entry.set_editable(True)
        self.desc_entry.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.swH.add(self.desc_entry)
        self.cm_hbox6.pack_start(self.swH, True, True, 0)
        vbox_right.pack_start(self.cm_hbox6, True, True, 4)

        self.space = gtk.HBox(False, 4)
        self.create_module_window_vbox.pack_start(self.space, False, False, 8)

        self.cm_hbox9 = gtk.HBox(False, 10)
        label = gtk.Label("Command: ")
        label.set_alignment(0, 0.5)
        label.set_size_request(200, 20)
        self.cm_hbox9.pack_start(label, False, False, 0)
        self.create_module_window_vbox.pack_start(self.cm_hbox9, False, False, 4)

        self.cm_hbox10 = gtk.HBox(False, 0)
        self.swH2 = gtk.ScrolledWindow()
        self.swH2.set_size_request(400, 100)
        self.swH2.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH2.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.tBuffer = gtk.TextBuffer(None)
        self.cmd_entry = gtk.TextView(self.tBuffer)
        self.cmd_entry.set_editable(False)
        self.cmd_entry.set_cursor_visible(False)
        self.cmd_entry.set_wrap_mode(gtk.WRAP_WORD_CHAR)

        self.cmd_entry.modify_font(pango.FontDescription("Monospace 10"))
        self.cmd_entry.modify_bg(
            self.cmd_entry.get_state(), gtk.gdk.color_parse("#444"))

        self.swH2.add(self.cmd_entry)
        self.cm_hbox10.pack_start(self.swH2, True, True, 0)
        self.create_module_window_vbox.pack_start(self.cm_hbox10, False, False, 4)

        hbox = gtk.HBox(False, 3)
        button = gtk.Button("Edit Command ...")
        button.connect("clicked", self.on_edit_command_clicked)
        button.set_alignment(0.5, 0.5)
        button.set_size_request(200, 30)
        hbox.pack_end(button, False, False, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        # Create HBox wrapper for lower content
        self.create_module_window_hbox = gtk.HBox(True, 3)
        self.create_module_window_hbox.set_size_request(400, 30)
        self.cancel = gtk.Button("Cancel")
        self.cancel.set_size_request(200, 30)
        self.cancel.connect(
            "clicked", self.on_create_module_window_cancel_clicked)

        #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
        self.add = gtk.Button("Create Module")
        self.add.set_size_request(200, 30)

        self.add.connect("clicked", self.on_create_module_clicked,
                         (library_combobox, category_combobox, function_combobox))

        # pack buttons
        self.create_module_window_hbox.add(self.cancel)
        self.create_module_window_hbox.add(self.add)

        vbox_right.pack_start(self.create_module_window_hbox, False, False, 4)
        hbox_wrapper.pack_start(
            self.create_module_window_vbox, False, False, 0)
        hbox_wrapper.pack_start(vbox_right, False, False, 0)
        self.create_module_window.add(hbox_wrapper)
        self.create_module_window.show_all()
    
    def on_edit_command_clicked(self, widget):
        start, end = self.tBuffer.get_bounds()
        ce = CmdEditor(self.tBuffer.get_text(start, end, False))
        self.create_module_window.connect("set-focus", self.set_cmd, ce)

    def set_cmd(self, window, widget, ce):
        self.tBuffer.set_text(ce.cmd)

    def on_create_module_window_cancel_clicked(self, widget, data=None):
        self.create_module_window.destroy()

    def on_create_module_clicked(self, widget, combobox):
        error = 0
        msg = "Module creation failed. Please enter the following:"
        if combobox[0].get_active_iter() is None:
            error = 1
            msg += "\n- Library"
        if combobox[1].get_active_iter() is None:
            error = 1
            msg += "\n- Category"
        if combobox[2].get_active_iter() is None:
            error = 1
            msg += "\n- Function"
        if self.module_name_entry.get_text_length() == 0:
            error = 1
            msg += "\n- Module Name"
        if self.suffix_entry.get_text_length() == 0:
            error = 1
            msg += "\n- Suffix"
        if self.author_entry.get_text_length() == 0:
            error = 1
            msg += "\n- Author"
        if self.desc_entry.get_buffer().get_char_count() == 0:
            error = 1
            msg += "\n- Description"
        if self.cmd_entry.get_buffer().get_char_count() == 0:
            error = 1
            msg += "\n- Command"

        if error == 1:
            md = Dialog()
            md.on_warn(msg)
        else:
            descbuff = self.desc_entry.get_buffer()
            desc_start, desc_end = descbuff.get_bounds()
            cmdbuff = self.cmd_entry.get_buffer()
            cmd_start, cmd_end = cmdbuff.get_bounds()
            name = self.module_name_entry.get_text()
            lib = self.library_liststore.get_value(combobox[0].get_active_iter(), 1)
            cat = self.catfilter.get_value(combobox[1].get_active_iter(), 2)
            func = self.functionfilter.get_value(combobox[2].get_active_iter(), 3)
            url = self.doc_url_entry.get_text()
            
            module = Module(lib,
                            cat,
                            func,
                            name,
                            self.suffix_entry.get_text(),
                            self.author_entry.get_text(),
                            url,
                            descbuff.get_text(desc_start, desc_end),
                            cmdbuff.get_text(cmd_start, cmd_end))
            iterr = self.module_liststore.append([lib, cat, func, name, module.filename])
            iterr = self.modulefilter.convert_child_iter_to_iter(iterr)
            iterr = self.sorted_module_liststore.convert_child_iter_to_iter(None, iterr)
            self.module_selection.select_iter(iterr)
            md = Dialog()
            self.create_module_window.destroy()
            md.on_info("Module created successfully.")
            module.save()
            self.update_module_textview(None, None)

    def on_EDIT_MODULE_activated(self, widget, data=None):
        op = Open()
        #pdb.set_trace()
        model, iterr = self.module_treeview.get_selection().get_selected()
        iterr = self.sorted_module_liststore.convert_iter_to_child_iter(None, iterr)
        module_edit = op.open_module(self.modulefilter.get_value(iterr, 4))
    
        self.create_module_window = gtk.Window()
        self.create_module_window.connect("delete_event", self.on_delete_event)
        self.create_module_window.set_title('Edit ' + module_edit.name)
        #self.create_module_window.set_size_request(440, 594)
        self.create_module_window.set_border_width(16)
        self.create_module_window.set_position(gtk.WIN_POS_CENTER)
        self.create_module_window.set_resizable(False)
        self.create_module_window.set_icon_from_file("img/epitome_icon.png")

        hbox_wrapper = gtk.HBox(False, 30)

        self.create_module_window_vbox = gtk.VBox(False, 0)

        vbox_right = gtk.VBox(False, 0)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Select Library: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(200, 30)
        hbox.pack_start(label, False, False, 10)
        library_combobox = gtk.ComboBox(self.library_liststore)
        library_combobox.connect("changed", self.on_library_combobox_changed)
        cell = gtk.CellRendererText()
        library_combobox.pack_start(cell, True)
        library_combobox.add_attribute(cell, 'text', 1)

        m, i = self.library_selection.get_selected()
        if i is not None:
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            library_combobox.set_active_iter(i)
        else:
            md = Dialog()
            md.on_warn("Please select a library.")
            return None

        hbox.pack_start(library_combobox, True, True, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Select Category: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(200, 30)
        hbox.pack_start(label, False, False, 10)
        category_combobox = gtk.ComboBox(self.catfilter)
        cell = gtk.CellRendererText()
        category_combobox.pack_start(cell, True)
        category_combobox.add_attribute(cell, 'text', 2)

        m, i = self.category_selection.get_selected()
        if i is not None:
            i = self.sorted_cat_liststore.convert_iter_to_child_iter(None, i)
            category_combobox.set_active_iter(i)
        else:
            md = Dialog()
            md.on_warn("Please select a category.")
            return None

        hbox.pack_start(category_combobox, True, True, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Select Function: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(200, 30)
        hbox.pack_start(label, False, False, 10)
        function_combobox = gtk.ComboBox(self.functionfilter)
        cell = gtk.CellRendererText()
        function_combobox.pack_start(cell, True)
        function_combobox.add_attribute(cell, 'text', 3)

        m, i = self.function_selection.get_selected()
        if i is not None:
            i = self.sorted_function_liststore.convert_iter_to_child_iter(None, i)
            function_combobox.set_active_iter(i)
        else:
            md = Dialog()
            md.on_warn("Please select a function.")
            return None

        hbox.pack_start(function_combobox, True, True, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        self.cm_hbox3 = gtk.HBox(False, 0)

        label = gtk.Label("Module Name: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox3.pack_start(label, False, False, 10)
        self.module_name_entry = gtk.Entry()
        self.module_name_entry.set_text(module_edit.name)
        self.module_name_entry.set_max_length(30)
        self.cm_hbox3.pack_start(self.module_name_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox3, False, False, 4)

        self.cm_hbox4 = gtk.HBox(False, 0)

        label = gtk.Label("Suffix: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox4.pack_start(label, False, False, 10)
        self.suffix_entry = gtk.Entry()
        self.suffix_entry.set_text(module_edit.suffix)
        self.suffix_entry.set_max_length(1)
        self.cm_hbox4.pack_start(self.suffix_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox4, False, False, 4)

        self.cm_hbox5 = gtk.HBox(False, 0)

        label = gtk.Label("Author: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox5.pack_start(label, False, False, 10)
        self.author_entry = gtk.Entry()
        self.author_entry.set_text(module_edit.author)
        self.cm_hbox5.pack_start(self.author_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox5, False, False, 4)

        self.cm_hbox7 = gtk.HBox(False, 0)

        label = gtk.Label("Documentation URL: ")
        label.set_alignment(1, 0.5)
        label.set_size_request(130, 30)
        self.cm_hbox7.pack_start(label, False, False, 10)
        self.doc_url_entry = gtk.Entry()
        self.doc_url_entry.set_text(module_edit.url)
        self.cm_hbox7.pack_start(self.doc_url_entry, True, True, 0)
        self.create_module_window_vbox.pack_start(
            self.cm_hbox7, False, False, 4)


        hbox = gtk.HBox(False, 10)
        label = gtk.Label("Description: ")
        label.set_alignment(0, 0.5)
        label.set_size_request(200, 20)
        hbox.pack_start(label, False, False, 0)
        vbox_right.pack_start(hbox, False, False, 4)

        self.cm_hbox6 = gtk.HBox(False, 0)
        self.swH = gtk.ScrolledWindow()
        self.swH.set_size_request(400, 200)
        self.swH.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.desc_entry = gtk.TextView()
        bufferr = gtk.TextBuffer(None)
        bufferr.set_text(module_edit.desc)
        self.desc_entry.set_buffer(bufferr)
        self.desc_entry.set_editable(True)
        self.desc_entry.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.swH.add(self.desc_entry)
        self.cm_hbox6.pack_start(self.swH, True, True, 0)
        vbox_right.pack_start(self.cm_hbox6, True, True, 4)

        self.space = gtk.HBox(False, 4)
        self.create_module_window_vbox.pack_start(self.space, False, False, 8)

        self.cm_hbox9 = gtk.HBox(False, 10)
        label = gtk.Label("Command: ")
        label.set_alignment(0, 0.5)
        label.set_size_request(200, 20)
        self.cm_hbox9.pack_start(label, False, False, 0)
        self.create_module_window_vbox.pack_start(self.cm_hbox9, False, False, 4)

        self.cm_hbox10 = gtk.HBox(False, 0)
        self.swH2 = gtk.ScrolledWindow()
        self.swH2.set_size_request(400, 100)
        self.swH2.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH2.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.tBuffer = gtk.TextBuffer(None)
        self.tBuffer.set_text(module_edit.cmd)
        self.cmd_entry = gtk.TextView(self.tBuffer)
        self.cmd_entry.set_editable(False)
        self.cmd_entry.set_cursor_visible(False)
        self.cmd_entry.set_wrap_mode(gtk.WRAP_WORD_CHAR)

        self.cmd_entry.modify_font(pango.FontDescription("Monospace 10"))
        self.cmd_entry.modify_bg(
            self.cmd_entry.get_state(), gtk.gdk.color_parse("#444"))

        self.swH2.add(self.cmd_entry)
        self.cm_hbox10.pack_start(self.swH2, True, True, 0)
        self.create_module_window_vbox.pack_start(self.cm_hbox10, False, False, 4)

        hbox = gtk.HBox(False, 3)
        button = gtk.Button("Edit Command ...")
        button.connect("clicked", self.on_edit_command_clicked)
        button.set_alignment(0.5, 0.5)
        button.set_size_request(200, 30)
        hbox.pack_end(button, False, False, 0)
        self.create_module_window_vbox.pack_start(hbox, False, False, 4)

        # Create HBox wrapper for lower content
        self.create_module_window_hbox = gtk.HBox(True, 3)
        self.create_module_window_hbox.set_size_request(400, 30)
        self.cancel = gtk.Button("Cancel")
        self.cancel.set_size_request(200, 30)
        self.cancel.connect(
            "clicked", self.on_create_module_window_cancel_clicked)

        #close_without_saving.connect("clicked", self.save_and_close_unsaved_changes_window)
        self.save = gtk.Button("Save Module")
        self.save.set_size_request(200, 30)

        self.save.connect("clicked", self.on_save_module_clicked,
                         (library_combobox, category_combobox, function_combobox, module_edit, iterr))

        # pack buttons
        self.create_module_window_hbox.add(self.cancel)
        self.create_module_window_hbox.add(self.save)

        vbox_right.pack_start(self.create_module_window_hbox, False, False, 4)
        hbox_wrapper.pack_start(
            self.create_module_window_vbox, False, False, 0)
        hbox_wrapper.pack_start(vbox_right, False, False, 0)
        self.create_module_window.add(hbox_wrapper)
        self.create_module_window.show_all()

    def on_save_module_clicked(self, widget, data):
        module = data[3]
        iterr = self.sorted_module_liststore.convert_iter_to_child_iter(None, data[4])
        iterr = self.modulefilter.convert_iter_to_child_iter(iterr)
        
        error = 0
        msg = "Module creation failed. Please enter the following:"
        if data[0].get_active_iter() is None:
            error = 1
            msg += "\n- Library"
        if data[1].get_active_iter() is None:
            error = 1
            msg += "\n- Category"
        if data[2].get_active_iter() is None:
            error = 1
            msg += "\n- Function"
        if self.module_name_entry.get_text_length() == 0:
            error = 1
            msg += "\n- Module Name"
        if self.suffix_entry.get_text_length() == 0:
            error = 1
            msg += "\n- Suffix"
        if self.author_entry.get_text_length() == 0:
            error = 1
            msg += "\n- Author"
        if self.desc_entry.get_buffer().get_char_count() == 0:
            error = 1
            msg += "\n- Description"
        if self.cmd_entry.get_buffer().get_char_count() == 0:
            error = 1
            msg += "\n- Command"

        if error == 1:
            md = Dialog()
            md.on_warn(msg)
        else:
            descbuff = self.desc_entry.get_buffer()
            desc_start, desc_end = descbuff.get_bounds()
            cmdbuff = self.cmd_entry.get_buffer()
            cmd_start, cmd_end = cmdbuff.get_bounds()
            name = self.module_name_entry.get_text()
            lib = self.library_liststore.get_value(data[0].get_active_iter(), 1)
            cat = self.catfilter.get_value(data[1].get_active_iter(), 2)
            func = self.functionfilter.get_value(data[2].get_active_iter(), 3)
            url = self.doc_url_entry.get_text()
            
            module.library = lib
            module.category = cat
            module.function = func
            module.name = name
            module.suffix = self.suffix_entry.get_text()
            module.author = self.author_entry.get_text()
            module.url = url
            module.desc = descbuff.get_text(desc_start, desc_end)
            module.cmd = cmdbuff.get_text(cmd_start, cmd_end)
            last_filename = module.filename
            module.filename = expanduser("~") + '/epitome/modules/' + name + '.mod'
            self.module_liststore.set(iterr, 0, lib, 1, cat, 2, func, 3, name, 4, module.filename)
            md = Dialog()
            self.create_module_window.destroy()
            md.on_info("Module " + name + ".mod updated successfully.")
            module.save()
            if last_filename != module.filename:
                subprocess.call(['rm', last_filename])
            self.update_module_textview(None, None)
    
    def on_DELETE_MODULE_activated(self, widget, data=None):
        model, iterr = self.module_treeview.get_selection().get_selected()
        iterr = self.sorted_module_liststore.convert_iter_to_child_iter(None, iterr)
        iterr = self.modulefilter.convert_iter_to_child_iter(iterr)
        name = self.module_liststore.get_value(iterr, 3)
        filename = self.module_liststore.get_value(iterr, 4)
        md = Dialog()
        response = md.on_confirmation("Are you sure you wish to delete " + name + ".mod?", "Delete " + name + ".mod")
        if response == gtk.RESPONSE_ACCEPT:
            subprocess.call(['rm', filename])
            self.module_liststore.remove(iterr)
            md.on_info("Successfully removed " + name + ".mod from library.")

    
    def __init__(self):

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.exit)
        self.window.set_title('Library Manager')
        self.window.set_size_request(620, 560)
        self.window.set_border_width(0)
        self.window.set_icon_from_file("img/epitome_icon.png")

        main_vbox = gtk.VBox(False, 1)

        '''
        Library popup menu
        '''

        library_popup_menu = gtk.Menu()

        new = gtk.MenuItem("New Library")
        new.connect("activate", self.on_NEW_LIBRARY_activated)
        new.show()

        edit = gtk.MenuItem("Edit Library...")
        edit.connect("activate", self.on_EDIT_LIBRARY_activated)
        edit.show()

        delete = gtk.MenuItem("Delete Library")
        delete.connect("activate", self.on_DELETE_LIBRARY_activated)
        delete.show()

        library_popup_menu.append(new)
        library_popup_menu.append(edit)
        library_popup_menu.append(delete)

        category_popup_menu = gtk.Menu()

        new = gtk.MenuItem("New Category")
        new.connect("activate", self.on_NEW_CATEGORY_activated)
        new.show()

        edit = gtk.MenuItem("Edit Category...")
        edit.connect("activate", self.on_EDIT_CATEGORY_activated)
        edit.show()

        delete = gtk.MenuItem("Delete Category")
        delete.connect("activate", self.on_DELETE_CATEGORY_activated)
        delete.show()

        category_popup_menu.append(new)
        category_popup_menu.append(edit)
        category_popup_menu.append(delete)

        function_popup_menu = gtk.Menu()

        new = gtk.MenuItem("New Function")
        new.connect("activate", self.on_NEW_FUNCTION_activated)
        new.show()

        edit = gtk.MenuItem("Edit Function...")
        edit.connect("activate", self.on_EDIT_FUNCTION_activated)
        edit.show()

        delete = gtk.MenuItem("Delete Function")
        delete.connect("activate", self.on_DELETE_FUNCTION_activated)
        delete.show()

        function_popup_menu.append(new)
        function_popup_menu.append(edit)
        function_popup_menu.append(delete)

        module_popup_menu = gtk.Menu()

        new = gtk.MenuItem("New Module")
        new.connect("activate", self.on_NEW_MODULE_activated)
        new.show()

        edit = gtk.MenuItem("Edit Module...")
        edit.connect("activate", self.on_EDIT_MODULE_activated)
        edit.show()

        delete = gtk.MenuItem("Delete Module")
        delete.connect("activate", self.on_DELETE_MODULE_activated)
        delete.show()

        module_popup_menu.append(new)
        module_popup_menu.append(edit)
        module_popup_menu.append(delete)

        self.vpane = gtk.VPaned()

        self.treeview_hbox = gtk.HBox(True, 6)

        '''
        Library Column
        '''

        self.swH0 = gtk.ScrolledWindow()
        self.swH0.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH0.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.library_liststore = gtk.ListStore(int, str)
        ls = Library("library")
        data = ls.read()
        for library in data:
            Id = library[0]
            library = library[1].encode('ascii', 'ignore')
            self.library_liststore.append([Id, library])

        self.sorted_library_liststore = gtk.TreeModelSort(self.library_liststore)
        self.sorted_library_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)
        
        self.library_treeview = gtk.TreeView(self.sorted_library_liststore)

        # self.library_treeview.set_show_expanders(False)
        self.library_selection = self.library_treeview.get_selection()
        self.library_selection.connect("changed", self.refilter_library)
        # create col 0 to display Pipeline Object name
        self.library_column = gtk.TreeViewColumn('Library')

        # append columns to TreeView
        self.library_treeview.append_column(self.library_column)

        # create a CellRendererText to render the data
        self.cell0 = gtk.CellRendererText()

        self.library_column.pack_start(self.cell0, True)
        self.library_column.add_attribute(self.cell0, 'text', 1)

        # allow search on columns
        # self.project_treeview.set_search_column(0)
        # self.treeview.set_search_column(1)
        # self.treeview.set_search_column(2)

        # Allow sorting on both columns
        # self.project_column.set_sort_column_id(0)
        
        self.library_column.set_sort_column_id(1)

        self.library_treeview.connect(
            "button-press-event", self.menu_activated, library_popup_menu)

        self.swH0.add(self.library_treeview)

        '''
        Category Column
        '''

        self.swH1 = gtk.ScrolledWindow()
        self.swH1.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH1.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.category_liststore = gtk.ListStore(int, str, str)
        cls = Library("category")
        data = cls.read()
        for category in data:
            Id = category[0]
            library = category[1].encode('ascii', 'ignore')
            cat = category[2].encode('ascii', 'ignore')
            self.category_liststore.append([Id, library, cat]) 

        self.catfilter = self.category_liststore.filter_new()
        self.catfilter.set_visible_func(self.visible_cat)
        
        self.sorted_cat_liststore = gtk.TreeModelSort(self.catfilter)
        self.sorted_cat_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)

        self.category_treeview = gtk.TreeView(self.sorted_cat_liststore)

        self.category_selection = self.category_treeview.get_selection()
        self.category_selection.connect("changed", self.refilter_category)

        # create col 0 to display Pipeline Object name
        self.category_column = gtk.TreeViewColumn('Category')

        # append columns to TreeView
        self.category_treeview.append_column(self.category_column)

        # create a CellRendererText to render the data
        self.cell1 = gtk.CellRendererText()

        self.category_column.pack_start(self.cell1, True)
        self.category_column.add_attribute(self.cell1, 'text', 2)

        # allow search on columns
        # self.treeview.set_search_column(1)
        # self.treeview.set_search_column(2)

        # Allow sorting on both columns
        self.category_column.set_sort_column_id(2)

        self.category_treeview.connect(
            "button-press-event", self.menu_activated, category_popup_menu)

        self.swH1.add(self.category_treeview)

        '''
        
        Function Column
        '''

        self.swH2 = gtk.ScrolledWindow()
        self.swH2.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH2.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.function_liststore = gtk.ListStore(int, str, str, str)

        fls = Library("function")
        data = fls.read()
        for tup in data:
            Id = tup[0]
            library = tup[1].encode('ascii', 'ignore')
            category = tup[2].encode('ascii', 'ignore')
            function = tup[3].encode('ascii', 'ignore')
            self.function_liststore.append([Id, library, category, function])

        self.functionfilter = self.function_liststore.filter_new()
        self.functionfilter.set_visible_func(self.visible_function)
        
        self.sorted_function_liststore = gtk.TreeModelSort(self.functionfilter)
        self.sorted_function_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)
        
        self.function_treeview = gtk.TreeView(self.sorted_function_liststore)

        self.function_selection = self.function_treeview.get_selection()
        self.function_selection.connect("changed", self.refilter_function)

        # create col 0 to display Pipeline Object name
        self.function_column = gtk.TreeViewColumn('Function')

        # append columns to TreeView
        self.function_treeview.append_column(self.function_column)

        # create a CellRendererText to render the data
        self.cell2 = gtk.CellRendererText()

        self.function_column.pack_start(self.cell2, True)
        self.function_column.add_attribute(self.cell2, 'text', 3)

        # allow search on columns
        self.function_treeview.set_search_column(3)

        # Allow sorting on both columns
        self.function_column.set_sort_column_id(3)

        self.function_treeview.connect(
            "button-press-event", self.menu_activated, function_popup_menu)

        self.swH2.add(self.function_treeview)

        '''
        Module Column
        '''

        self.swH3 = gtk.ScrolledWindow()
        self.swH3.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.swH3.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.module_liststore = gtk.ListStore(str, str, str, str, str)

        for root, dirs, files in os.walk('/home/cboyce/epitome/modules/'):
            for filepath in files:
                op = Open()
                module = op.open_module(
                    '/home/cboyce/epitome/modules/' + filepath)
                self.module_liststore.append(
                    [module.library, module.category, module.function, module.name, module.filename])
        
        self.modulefilter = self.module_liststore.filter_new()
        self.modulefilter.set_visible_func(self.visible_mod)

        self.sorted_module_liststore = gtk.TreeModelSort(self.modulefilter)
        self.sorted_module_liststore.set_sort_column_id(0, gtk.SORT_ASCENDING)
        
        self.module_treeview = gtk.TreeView(self.sorted_module_liststore)

        self.module_selection = self.module_treeview.get_selection()
        self.module_selection.connect("changed", self.update_module_textview)

        # create col 0 to display Pipeline Object name
        self.module_column = gtk.TreeViewColumn('Module')

        # append columns to TreeView
        self.module_treeview.append_column(self.module_column)

        # create a CellRendererText to render the data
        self.cell3 = gtk.CellRendererText()

        self.module_column.pack_start(self.cell3, True)
        self.module_column.add_attribute(self.cell3, 'text', 3)

        # allow search on columns
        self.module_treeview.set_search_column(3)
        # self.treeview.set_search_column(1)
        # self.treeview.set_search_column(2)

        # Allow sorting on both columns
        self.module_column.set_sort_column_id(3)

        self.module_treeview.connect(
            "button-press-event", self.menu_activated, module_popup_menu)

        self.swH3.add(self.module_treeview)

        self.treeview_hbox.pack_start(self.swH0, True, True, 0)
        self.treeview_hbox.pack_start(self.swH1, True, True, 0)
        self.treeview_hbox.pack_start(self.swH2, True, True, 0)
        self.treeview_hbox.pack_start(self.swH3, True, True, 0)

        '''
        Label
        '''

        #self.label = gtk.Label("Module Description")

        '''
        TextView
        '''
        self.textview_hbox = gtk.HBox(False, 0)

        self.textview_hbox.set_size_request(500, 200)
        self.swH3 = gtk.ScrolledWindow()
        self.swH3.set_shadow_type(gtk.SHADOW_ETCHED_IN)

        self.swH3.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.buffer = gtk.TextBuffer(None)
        self.buffer.set_text("(no module selected)")
        self.textview = gtk.TextView(self.buffer)
        self.textview.set_border_width(10)
        self.textview.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        self.textview.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.textview.set_justification(gtk.JUSTIFY_FILL)

        self.swH3.add(self.textview)

        self.textview_hbox.pack_start(self.swH3, True, True, 0)

        self.vpane.add1(self.treeview_hbox)
        #main_vbox.pack_start(self.label, False, False, 5)
        self.vpane.add2(self.textview_hbox)
        self.vpane.set_position(160)
        self.vpane.set_border_width(6)
        main_vbox.pack_start(self.vpane, True, True, 0)
        self.window.add(main_vbox)

        self.window.show_all()

    def menu_activated(self, widget, event, menu):
        # right click
        if event.button == 3:
            menu.popup(None, None, None, event.button,
                       event.get_time(), data=None)
            pass

    def visible_cat(self, model, iterr, data=None):
        treeselection = self.library_treeview.get_selection()

        if treeselection.count_selected_rows() > 0:
            m, i = treeselection.get_selected()
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            if(model.get_value(iterr, 1) == self.library_liststore.get_value(i, 1)):
                return True
        return False

    def visible_function(self, model, iterr, data=None):
        lib_treeselection = self.library_treeview.get_selection()
        cat_treeselection = self.category_treeview.get_selection()
        if lib_treeselection.count_selected_rows() > 0 and cat_treeselection.count_selected_rows() > 0:
            m, i = lib_treeselection.get_selected()
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            n, j = cat_treeselection.get_selected()
            j = self.sorted_cat_liststore.convert_iter_to_child_iter(None, j)
            if model.get_value(iterr, 1) == self.library_liststore.get_value(i, 1) and model.get_value(iterr, 2) == self.catfilter.get_value(j, 2):
                return True
        return False

    def visible_mod(self, model, iterr, data=None):
        lib_treeselection = self.library_treeview.get_selection()
        cat_treeselection = self.category_treeview.get_selection()
        function_treeselection = self.function_treeview.get_selection()
        if lib_treeselection.count_selected_rows() > 0 and cat_treeselection.count_selected_rows() > 0 and function_treeselection.count_selected_rows() > 0:
            m, i = lib_treeselection.get_selected()
            i = self.sorted_library_liststore.convert_iter_to_child_iter(None, i)
            n, j = cat_treeselection.get_selected()
            j = self.sorted_cat_liststore.convert_iter_to_child_iter(None, j)
            o, k = function_treeselection.get_selected()
            k = self.sorted_function_liststore.convert_iter_to_child_iter(None, k)
            if model.get_value(iterr, 0) == self.library_liststore.get_value(i, 1) and model.get_value(iterr, 1) == self.catfilter.get_value(j, 2) and model.get_value(iterr, 2) == self.functionfilter.get_value(k, 3):
                return True
        return False

    def update_module_textview(self, widget, data=None):
        selection = self.module_treeview.get_selection()
        m, i = selection.get_selected()
        if i is not None:
            i = self.sorted_module_liststore.convert_iter_to_child_iter(None, i)
            name = self.modulefilter.get_value(i, 3)
            try:
                filehandler = open(
                    expanduser("~") + '/epitome/modules/' + name + '.mod', 'rb')
                module = dill.load(filehandler)
                self.buffer.set_text('')
                # pango tags
                bold = self.buffer.create_tag(weight=pango.WEIGHT_BOLD)
                title = self.buffer.create_tag(size=18000)
                code = self.buffer.create_tag(font="Monospace 10")
                crumbs = self.buffer.create_tag(font="Italic 8")

                self.buffer.insert_with_tags(self.buffer.get_start_iter(
                ), module.library + ' > ' + module.category + ' > ' + module.function + '\n\n', crumbs)
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, name + '\n', bold, title)
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Author: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.author + '\n')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Date Created: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.date_created + '\t')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Last Edited: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.date_updated + '\n')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, '\nSuffix: ', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.suffix + '\n\n')
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, 'Description:\n', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert(end, module.desc + '\n\n')

                start, end = self.buffer.get_bounds()
                anchor = self.buffer.create_child_anchor(end)
                button = gtk.LinkButton(
                    module.url, "Click here to access online resource...")
                button.show()
                self.textview.add_child_at_anchor(button, anchor)

                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, '\n\nCommand:\n', bold)
                start, end = self.buffer.get_bounds()
                self.buffer.insert_with_tags(end, module.cmd + '\n\n', code)
            except:
                print("")
        else:
            self.buffer.set_text("(no module selected)")


def main():
    gtk.main()


if __name__ == "__main__":
    win = LibraryManager()
    main()
