import dill
import time
import datetime
import gtk
from os.path import expanduser
import subprocess
import pdb


class Module:
    def __init__(self, library, category, function, name, suffix, author, url, desc, cmd):
        self.library = library
        self.category = category
        self.function = function
        self.name = name
        self.suffix = suffix
        self.author = author
        self.url = url

        t = time.time()
        date = str(datetime.datetime.fromtimestamp(
            t).strftime('%b-%d-%Y %H:%M:%S'))

        self.date_created = date
        self.date_updated = date

        self.desc = desc
        self.cmd = cmd
        #pdb.set_trace()
        subprocess.call(['mkdir', '-p', expanduser("~") + '/epitome/modules/'])
        self.filename = expanduser("~") + '/epitome/modules/' + self.name + '.mod'
        self.stage = None
        self.state = 0
        self.dataset = None
        self.Id = None
        # lock module from deletion for duplicated pipelines by setting to false
        self.deletable = True
        self.keepinter = True
        self.sensitive = True
        self.prefix_type = None

    def save(self):
        filehandler = open(self.filename, 'wb')
        dill.dump(self, filehandler)
    
    def reset(self):
        self.state = 0
        self.dataset = None
        self.Id = None


