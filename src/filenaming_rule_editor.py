from subprocess import Popen, PIPE, check_call, CalledProcessError
import subprocess
from dialog import Dialog

import pygtk
import pango
pygtk.require('2.0')
import gtk


class FilenamingRuleEditor():

    def exit(self, widget, data=None):
        self.window.destroy()

    def verify_and_save(self, widget, project):
        start, end = self.tBuffer.get_bounds()
        to_verify = self.tBuffer.get_text(start, end, False)
        md = Dialog()
        if " " in to_verify:
            md.on_warn("No whitespaces allowed.")
        else:
            self.rule = to_verify
            project.rule = self.rule
            project.save_project()
            md.on_info("Rule Saved!")

    def __init__(self, project_name, project_rule, project):
        self.window = gtk.Window()
        self.rule = project_rule
        self.window.connect("delete_event", self.exit)
        self.window.set_title('Set Filenaming Rule for ' + project_name)
        self.window.set_size_request(500, 120)
        self.window.set_border_width(0)
        self.window.set_icon_from_file("img/epitome_icon.png")
        self.cmd = ""
        main_vbox = gtk.VBox(False, 1)
        hbox = gtk.HBox(True, 0)
        toolbar = gtk.Toolbar()
        toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
        toolbar.set_style(gtk.TOOLBAR_ICONS)

        save = gtk.ToolButton(gtk.STOCK_SAVE)
        save.connect("clicked", self.verify_and_save, project)
        save.set_tooltip_markup(
            '<b>Verify &amp; Save: </b> Verify rule is valid and apply to project.')
        toolbar.insert(save, 0)

        space = gtk.SeparatorToolItem()
        space.set_draw(False)
        space.set_expand(True)
        toolbar.insert(space, 1)

        exit = gtk.ToolButton(gtk.STOCK_QUIT)
        exit.connect("clicked", self.exit)
        exit.set_tooltip_markup('<b>Close: </b> Close editor window')
        toolbar.insert(exit, 2)

        tMenu = gtk.Menu()
        project_name = gtk.MenuItem("$project")
        pipeline_name = gtk.MenuItem("$pipeline")
        module_category = gtk.MenuItem("$modulecategory")
        module_function = gtk.MenuItem("$modulefunction")
        module_name = gtk.MenuItem("$modulename")
        module_suffix = gtk.MenuItem("$modulesuffix")
        date = gtk.MenuItem("$date")
        group = gtk.MenuItem("$group")
        participant = gtk.MenuItem("$participant")
        task = gtk.MenuItem("$task")
        run = gtk.MenuItem("$run")
        project_name.connect("activate", self.on_menu_item_activated)
        pipeline_name.connect("activate", self.on_menu_item_activated)
        module_category.connect("activate", self.on_menu_item_activated)
        module_function.connect("activate", self.on_menu_item_activated)
        module_name.connect("activate", self.on_menu_item_activated)
        module_suffix.connect("activate", self.on_menu_item_activated)
        date.connect("activate", self.on_menu_item_activated)
        participant.connect("activate", self.on_menu_item_activated)
        task.connect("activate", self.on_menu_item_activated)
        run.connect("activate", self.on_menu_item_activated)
        project_name.show()
        pipeline_name.show()
        module_category.show()
        module_function.show()
        module_name.show()
        module_suffix.show()
        date.show()
        group.show()
        participant.show()
        task.show()
        run.show()
        tMenu.append(project_name)
        tMenu.append(pipeline_name)
        tMenu.append(module_category)
        tMenu.append(module_function)
        tMenu.append(module_name)
        tMenu.append(module_suffix)
        tMenu.append(date)
        tMenu.append(group)
        tMenu.append(participant)
        tMenu.append(task)
        tMenu.append(run)

        self.tBuffer = gtk.TextBuffer(None)
        swH = gtk.ScrolledWindow()
        swH.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.tBuffer.set_text(project_rule)
        self.tView = gtk.TextView(self.tBuffer)
        self.tView.set_border_width(5)
        self.tView.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        self.tView.modify_font(pango.FontDescription("Monospace 10"))
        self.tView.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.tView.connect("key-press-event", self.entry_completion, tMenu)
        swH.add(self.tView)
        hbox.pack_start(toolbar, False, True, 0)
        main_vbox.pack_start(hbox, False, True, 0)
        main_vbox.pack_start(swH, True, True, 0)

        self.window.add(main_vbox)

        self.window.show_all()

    def entry_completion(self, tView, event, menu):
        if event.keyval == 36:
            completion_menu = self.build_completion_menu(
                menu, event.get_time())

    def build_completion_menu(self, menu, event_time):
        menu.popup(None, None, self.set_menu_position, 0, event_time)

    def set_menu_position(self, menu, data=None):
        iterr = self.tBuffer.get_iter_at_offset(
            self.tBuffer.props.cursor_position)
        rectangle = self.tView.get_iter_location(iterr)
        win_x, win_y = self.window.get_position()
        bw = self.tView.get_border_width()
        return (win_x + rectangle.x + bw, win_y + 86 + rectangle.y + bw, True)

    def on_menu_item_activated(self, menu_item, data=None):
        self.tBuffer.insert_at_cursor(menu_item.get_label().replace("$", ""))

