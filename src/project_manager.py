#!/usr/bin/env python

import pygtk
import gtk
import dill
import os
import pwd
from random import randint
pygtk.require('2.0')

import pdb

import sys
sys.path += ['src']

from lib.Project import Project
from pipeline_editor import PipelineEditor
from project_editor import ProjectEditor
from new_project import NewProject
from dialog import Dialog
from util.projects import Projects
from util.open import Open
from library_manager import LibraryManager


class ProjectManager():

    def __close_window(self, window, data=None):
        window.destroy()
        gtk.main_quit()
    
    def __exit(self, event, param, window):
        window.destroy()
        gtk.main_quit()

    def __on_about_clicked(self, widget, data=None):
        about = gtk.AboutDialog()
        about.set_program_name("EPItome-xl")
        about.set_version("1.0")
        about.set_copyright("Author: Cameron Boyce")
        about.set_comments("Workflow manager for MRI pre-processing.")
        about.set_website("http://www.cannlab.ca/epitome-xl")
        about.set_logo(gtk.gdk.pixbuf_new_from_file("img/epitome_logo.png"))
        about.run()
        about.destroy()

    def __on_library_manager_clicked(self, widget):
        lm = LibraryManager()

    def __on_new_project_clicked(self, widget, data=None):
        projs = Projects()
        db = projs.read()
        Ids = []
        for data in db:
            Ids.append(data[0])
        while (True):
            index = randint(0, 10000)
            if index not in Ids: 
                np = NewProject(index)
                return None

    def __on_open_project_clicked(self, widget, data):
        if data[0].count_selected_rows() > 0:
            model, iterr = data[0].get_selected()
            iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
            filepath = data[1].get_value(iterr, 6)
            opener = Open()
            project = opener.open_project(filepath)
            editor = ProjectEditor(project)
        else:
            md = Dialog()
            md.on_warn("Please ensure a project has been selected.")

    def populate_treestore(self):
        projs = Projects()
        db = projs.read()
        op = Open()
        #pdb.set_trace()
        self.db = db
        self.treestore.clear()
        for data in db: 
            project_members = self.get_project_members(data[2])
            if self.get_username() in project_members:
                self.treestore.append(None, [data[0], data[1], data[2], data[3], data[4], data[5], data[6]])
                project = op.open_project(data[6])
                project.members = project_members
                project.save_project_members()        
        projs.close()

    def get_project_members(self, root):
        group = open("/etc/group", "r")
        project_members = []
        for line in group:
            if root.lower() in line:
                ids = line.split(":")
                ids = ids[3].split(",")
                for Id in ids:
                    Id = str(Id).replace("\n","")
                    project_members.append(Id)
                return project_members
        return []
    
    def get_username(self):
        return pwd.getpwuid(os.getuid())[0]
    
    def set_focus_changed(self, window, wigdet, data=None):
        projs = Projects()
        db = projs.read()
        if len(self.db) != len(db):
            self.populate_treestore()

    def __init__(self):
        """Build window."""
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("delete_event", self.__close_window)
        window.set_title('EPItome-xl')
        window.set_size_request(660, 180)
        window.set_border_width(0)
        window.set_icon_from_file("img/epitome_icon.png")
        main_vbox = gtk.VBox(False, 1)

        swH = gtk.ScrolledWindow()
        swH.set_shadow_type(gtk.SHADOW_ETCHED_IN)        
        swH.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.treestore = gtk.TreeStore(int, str, str, str, str, str, str)
        self.populate_treestore()
        
        self.sorted_treestore = gtk.TreeModelSort(self.treestore)
        self.sorted_treestore.set_sort_column_id(3, gtk.SORT_DESCENDING)
        
        treeview = gtk.TreeView(self.sorted_treestore)

        window.connect("set-focus", self.set_focus_changed)

        name_column = gtk.TreeViewColumn('Project Name')
        name_column.set_min_width(100)
        last_edited_column = gtk.TreeViewColumn('Last Edited')
        last_user_column = gtk.TreeViewColumn('By')
        last_user_column.set_min_width(100)
        date_created_column = gtk.TreeViewColumn('Date Created')

        treeview.append_column(name_column)
        treeview.append_column(last_edited_column)
        treeview.append_column(last_user_column)
        treeview.append_column(date_created_column)

        cell0 = gtk.CellRendererText()
        cell1 = gtk.CellRendererText()
        cell2 = gtk.CellRendererText()
        cell3 = gtk.CellRendererText()

        name_column.pack_start(cell0, True)
        last_edited_column.pack_start(cell1, True)
        last_user_column.pack_start(cell3, True)
        date_created_column.pack_start(cell2, True)

        name_column.add_attribute(cell0, 'text', 1)
        last_edited_column.add_attribute(cell1, 'text', 3)
        last_user_column.add_attribute(cell3, 'text', 4)
        date_created_column.add_attribute(cell2, 'text', 5)
        
        name_column.set_sort_column_id(1)
        last_edited_column.set_sort_column_id(3)
        last_user_column.set_sort_column_id(4)
        date_created_column.set_sort_column_id(5)
        
        treeselection = treeview.get_selection()
        treeselection.set_mode(gtk.SELECTION_SINGLE)

        hbox = gtk.HBox(False, 0)

        buttons = gtk.VBox(False, 1)
        new_project_button = gtk.Button("    New Project    ")
        new_project_button.connect("clicked", self.__on_new_project_clicked)
        open_project_button = gtk.Button("    Open Project    ")
        open_project_button.connect("clicked", self.__on_open_project_clicked, [
                                    treeselection, self.treestore])
        buttons.pack_start(new_project_button, False, False, 0)
        buttons.pack_start(open_project_button, False, False, 0)

        menubar = gtk.MenuBar()

        filemenu = gtk.Menu()
        filem = gtk.MenuItem("_File")
        filem.set_submenu(filemenu)
        accelerator_01 = gtk.AccelGroup()
        window.add_accel_group(accelerator_01)

        new_project = gtk.ImageMenuItem(gtk.STOCK_NEW, accelerator_01)
        new_project.get_children()[0].set_label('New Project')
        key, mod = gtk.accelerator_parse("<Control>N")
        new_project.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        new_project.connect("activate", self.__on_new_project_clicked)
        filemenu.append(new_project)
        open_project = gtk.ImageMenuItem(gtk.STOCK_OPEN, accelerator_01)
        open_project.get_children()[0].set_label('Open Project')
        key, mod = gtk.accelerator_parse("<Control>O")

        open_project.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        open_project.connect("activate", self.__on_open_project_clicked, [
                             treeselection, self.treestore])
        filemenu.append(open_project)

        sep = gtk.SeparatorMenuItem()
        filemenu.append(sep)

        quit = gtk.ImageMenuItem(gtk.STOCK_QUIT, accelerator_01)
        quit.get_children()[0].set_label('Quit')

        key, mod = gtk.accelerator_parse("<Control>Q")
        quit.add_accelerator("activate", accelerator_01,
                             key, mod, gtk.ACCEL_VISIBLE)
        quit.connect("activate", self.__exit, quit, window)
        filemenu.append(quit)

        toolsmenu = gtk.Menu()

        filet = gtk.MenuItem("_Tools")
        filet.set_submenu(toolsmenu)

        library_manager = gtk.ImageMenuItem(
            gtk.STOCK_DND_MULTIPLE, accelerator_01)
        library_manager.get_children()[0].set_label('Library Manager')
        key, mod = gtk.accelerator_parse("L")
        library_manager.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        library_manager.connect("activate", self.__on_library_manager_clicked)
        toolsmenu.append(library_manager)

        helpmenu = gtk.Menu()

        fileh = gtk.MenuItem("_Help")
        fileh.set_right_justified(True)
        fileh.set_submenu(helpmenu)

        about = gtk.ImageMenuItem(gtk.STOCK_ABOUT, accelerator_01)
        about.get_children()[0].set_label('About')
        key, mod = gtk.accelerator_parse("<Control>A")
        about.add_accelerator("activate", accelerator_01,
                              key, mod, gtk.ACCEL_VISIBLE)
        about.connect("activate", self.__on_about_clicked)
        helpmenu.append(about)

        menubar.append(filem)
        menubar.append(filet)
        menubar.append(fileh)

        filemenubox = gtk.VBox(False, 2)
        filemenubox.pack_start(menubar, True, True, 0)

        swH.add(treeview)
        main_vbox.pack_start(filemenubox, False, True, 0)
        hbox.pack_start(buttons, False, True, 0)
        hbox.pack_start(swH, True, True, 4)

        main_vbox.pack_start(hbox, True, True, 2)
        window.add(main_vbox)
        window.show_all()


def main():
    gtk.main()


if __name__ == "__main__":
    win = ProjectManager()
    main()
