#!/usr/bin/env python

import pygtk
import gtk
import dill
import time
import datetime
pygtk.require('2.0')
import gobject
from subprocess import Popen
from gtk import gdk
import pwd
import os

import sys
sys.path += ['src']

from Pipeline import Pipeline
from util.projects import Projects

class Project:

    def __init__(self, name, directory, project_Id, root, members, owner):
        self.name = name
        self.directory = directory
        self.path = self.directory + "/" + self.name
        self.Id = project_Id
        self.date_created = str(datetime.datetime.fromtimestamp(
            time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        self.select_data_treestore = None
        self.rule = "$modulesuffix_$group_$participant_$task_$run"
        self.root = root
        self.members = members
        self.owner = owner
        self.email = ""
        Popen(["mkdir", "-p", self.path + "/pipelines"])
    
    @property
    def select_data_treestore(self):
        return self.__select_data_treestore
        
    @select_data_treestore.setter
    def select_data_treestore(self, select_data_treestore):
        self.__select_data_treestore = select_data_treestore

    def save_project(self):
        self.ser_treestore = []
        self.select_data_treestore.foreach(self.serialize_select_data_treestore, None)
        filehandler = open(self.path + "/" + self.name + ".epi", 'wb')
        dill.dump(self, filehandler)
        rc = Projects()
        if len(rc.search(self.Id)) == 0:
            rc.entry(self.Id, self.name, self.root,self.get_username(), self.date_created, self.path + "/" + self.name + ".epi")
        else:
            rc.update(self.Id, self.name, self.root, self.get_username(), self.date_created, self.path + "/" + self.name + ".epi")
        rc.close()
    
    def save_project_members(self):
        self.ser_treestore = []
        self.select_data_treestore.foreach(self.serialize_select_data_treestore, None)
        filehandler = open(self.path + "/" + self.name + ".epi", 'wb')
        dill.dump(self, filehandler)

    def serialize_select_data_treestore(self, model, path, iter, data=None):
        tup = (path, len(path))
        for i in range(0, 5):
            tup += (self.select_data_treestore.get_value(iter, i),)
        self.ser_treestore.append(tup)
    
    def get_username(self):
        return pwd.getpwuid(os.getuid())[0]

