#!/usr/bin/env python

import pygtk
import gtk
import dill
import time
import datetime
pygtk.require('2.0')

from lib.Project import Project
from lib.Pipeline import Pipeline
from pipeline_editor import PipelineEditor
from dialog import Dialog

from util.db import DB


class SelectData():

    def __close_window(self, window, data=None):
        window.destroy()
    
    def __init__(self, pipeline):

        window = gtk.Window()
        window.connect("delete_event", self.close_window)
        window.set_title(pipeline.name + " - Select Data")
        window.set_position(gtk.WIN_POS_CENTER)
        window.set_geometry_hints(window,
            min_width=300,
            min_height=400,
            max_width=-1,
            max_height=-1,
            base_width=-1,
            base_height=-1,
            width_inc=-1,
            height_inc=-1,
            min_aspect=-1.0,
            max_aspect=-1.0)
        window.set_border_width(3)
        window.set_icon_from_file("img/epitome_icon.png")

        vbox = gtk.VBox(False, 1)

        swH = gtk.ScrolledWindow()
        swH.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        swH.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

        self.select_data_treeview = gtk.TreeView(pipeline.select_data_treestore)
        self.select_data_treeview.set_enable_tree_lines(True)

        self.column0 = gtk.TreeViewColumn('Project Hierarchy')

        self.cell0 = gtk.CellRendererText()
        self.column0.pack_start(self.cell0, True)
        self.column0.add_attribute(self.cell0, 'text', 0)
        self.column0.set_min_width(30)

        self.select_data_treeview.append_column(self.column0)

        self.cell3 = gtk.CellRendererToggle()
        self.cell3.set_radio(False)
        self.cell3.set_activatable(True)
        self.cell3.set_active(True)
        self.cell3.set_sensitive(True)
        self.cell3.set_visible(True)
        self.cell3.connect('toggled', self.toggled)

        self.column3 = gtk.TreeViewColumn('Selection', self.cell3)
        self.column3.set_fixed_width(6)
        self.column3.add_attribute(self.cell3, 'active', 3)
        self.column3.add_attribute(self.cell3, 'visible', 4)
        self.select_data_treeview.append_column(self.column3)

        self.swH.add(self.select_data_treeview)

        self.buttons = gtk.HBox(False, 1)
        self.cancel = gtk.Button("   Cancel   ")
        self.cancel.connect("clicked", self.sd_cancel_button_clicked)
        self.cancel.set_alignment(xalign=1, yalign=0.5)
        self.apply = gtk.Button("   Apply   ")
        self.apply.connect("clicked", self.sd_apply_button_clicked)
        self.apply.set_alignment(xalign=1, yalign=0.5)

        self.buttons.pack_end(self.apply, False, False, 2)
        self.buttons.pack_end(self.cancel, False, False, 2)

        self.select_data_vbox.pack_start(self.swH, True, True, 0)
        self.select_data_vbox.pack_start(self.buttons, False, False, 0)

        self.select_data_window.add(self.select_data_vbox)

        self.select_data_treeview.expand_all()

        self.select_data_window.show_all()

    def sd_apply_button_clicked(self, widget, data=None):
        print("apply")

    def sd_cancel_button_clicked(self, widget, data=None):
        self.select_data_window.destroy()

    def get_children(self, root_iter):
        children = []
        for i in range(0, self.treestore.iter_n_children(root_iter)):
            children.append(self.treestore.iter_nth_child(root_iter, i))
        return children

    def toggled(self, widget, path, model=None):
        toggled_iter = self.CURRENT_PROJECT.select_data_treestore.get_iter(path)
        iter_check = not self.CURRENT_PROJECT.select_data_treestore.get_value(
            toggled_iter, 3)
        self.CURRENT_PROJECT.select_data_treestore.set(toggled_iter, 3, iter_check)
        # path passed as non-standard format i.e., ('0':'0':...)
        # convert to standard path format => (0:0:...)
        path = tuple(path.replace(":", ""))
        tup = ()
        for char in path:
            tup += (int(char),)
        print(tup)
        data = [iter_check, tup]
        # check tree position to determine if DFS required to toggle
        # children/parents
        if len(tup) < 5:
            self.CURRENT_PROJECT.select_data_treestore.foreach(self.toggle_children, data)
        else:
            self.CURRENT_PROJECT.select_data_treestore.foreach(self.toggle_parents, data)

    def toggle_children(self, model, path, iter, data):
        toggled_iter = data[0]
        ref_path = data[1]
        len_ref_path = len(ref_path)
        if len(path) > len_ref_path:
            if cmp(path[0:len_ref_path], ref_path) == 0:
                self.CURRENT_PROJECT.select_data_treestore.set(iter, 3, toggled_iter)

    def toggle_parents(self, model, path, iter, data):
        toggled_iter = data[0]
        ref_path = data[1]
        len_ref_path = len(ref_path)
        if len(path) < len_ref_path:
            if cmp(path, ref_path[0:len(path)]) == 0:
                self.CURRENT_PROJECT.select_data_treestore.set(iter, 3, toggled_iter)


