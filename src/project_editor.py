#!/usr/bin/env python

import pygtk
import gtk
import dill
import time
import datetime
import gobject
import os
import sqlite3
from subprocess import Popen, call
pygtk.require('2.0')

from lib.Project import Project
from lib.Pipeline import Pipeline
from pipeline_editor import PipelineEditor
from filenaming_rule_editor import FilenamingRuleEditor
from util.open import Open
from dialog import Dialog
import util.username as util_user

from util.db import DB

import pdb


class ProjectEditor():

    def close_window(self, window, data=None):
        window.destroy()
    
    def __close_subwindow(self, widget, subwindow):
        subwindow.destroy()
        
    def on_ABOUT_activate(self, widget, data=None):
        about = gtk.AboutDialog()
        about.set_program_name("EPItome-xl")
        about.set_version("1.0")
        about.set_copyright("Author: Cameron Boyce")
        about.set_comments("Workflow manager for MRI pre-processing.")
        about.set_website("http://www.cannlab.ca/epitome-xl")
        about.set_logo(gtk.gdk.pixbuf_new_from_file("img/epitome_logo.png"))
        about.run()
        about.destroy()
        
    def on_SET_FILENAMING_RULE_activated(self, widget, data=None):
        """ Set Project Filenaming Rule
        
        If self.fre still None, indicating we have not yet run this method,
        pass the default rule. Otherwise, pass the new rule stored in this object.
        """
        if self.fre is None:
            self.fre = FilenamingRuleEditor(self.CURRENT_PROJECT.name, self.CURRENT_PROJECT.rule, self.CURRENT_PROJECT)
        else:
            self.fre = FilenamingRuleEditor(self.CURRENT_PROJECT.name, self.fre.rule, self.CURRENT_PROJECT)
        self.CURRENT_PROJECT.save_project()

    def on_PREFERENCES_activated(self, widget, data=None):
        preferences_window = gtk.Window()
        preferences_window.connect("delete_event", self.close_window)
        preferences_window.set_title('Preferences - ' + self.CURRENT_PROJECT.name)
        preferences_window.set_border_width(4)
        #preferences_window.set_size_request(500, 280)
        
        notebook = gtk.Notebook()
        notebook.set_tab_pos(gtk.POS_TOP)
        
        vbox = gtk.VBox(False, 0)
        vbox.set_border_width(10)

        label = gtk.Label("Name: " + self.CURRENT_PROJECT.name)
        label.set_alignment(0, 0.5)
        vbox.pack_start(label, False, False, 0)
        
        label = gtk.Label("Directory: " + self.CURRENT_PROJECT.directory)
        label.set_alignment(0, 0.5)
        vbox.pack_start(label, False, False, 0)

        label = gtk.Label("Date Created: " + self.CURRENT_PROJECT.date_created)
        label.set_alignment(0, 0.5)
        vbox.pack_start(label, False, False, 0)

        label = gtk.Label("Owner: " + self.CURRENT_PROJECT.owner)
        label.set_alignment(0, 0.5)
        vbox.pack_start(label, False, False, 0)

        label = gtk.Label("Member(s): " + ", ".join([str(x) for x in self.CURRENT_PROJECT.members]))
        label.set_alignment(0, 0.5)
        label.set_line_wrap(True)
        vbox.pack_start(label, False, False, 0)
        
        label = gtk.Label("Project Information")
        
        notebook.append_page(vbox, label)
        
        vbox = gtk.VBox(False, 2)
        vbox.set_border_width(10)
        
        label = gtk.Label("Enter email(s) of group members in the entry box" +
            " below. Provided email(s) will be sent a notification upon" + 
            " module completion.\n\nSeparate multiple emails by white space.\n")
        label.set_line_wrap(True)
        #label.set_size_request(460, 60)
        label.set_justify(gtk.JUSTIFY_LEFT)
        #label.set_alignment(0, 0.5)
        vbox.pack_start(label, False, True, 4)
        
        label = gtk.Label("Email(s):")
        label.set_alignment(0, 0.5)
        label.set_size_request(100, 30)
        vbox.pack_start(label, False, False, 0)
        
        email_entry = gtk.Entry()
        email_entry.set_text(self.CURRENT_PROJECT.email)
        vbox.pack_start(email_entry, False, True, 0)

        hbox = gtk.HBox(False, 0)

        cancel = gtk.Button("     Close     ")
        cancel.connect("clicked", self.on_cancel_clicked, preferences_window)

        create = gtk.Button("     Apply     ")
        create.connect("clicked", self.apply, email_entry)

        hbox.pack_end(create, False, True, 2)
        hbox.pack_end(cancel, False, True, 2)

        vbox.pack_start(hbox, False, False, 0)
        
        label = gtk.Label("Email Notifications")
        
        notebook.append_page(vbox, label)

        preferences_window.add(notebook)
        preferences_window.show_all()
    
    def apply(self, widget, email_entry):
        self.CURRENT_PROJECT.email = email_entry.get_text()
        self.CURRENT_PROJECT.save_project()
        md = Dialog()
        md.on_info("Project email(s) updated successfully.")
    
    def on_NEW_PIPELINE_clicked(self, widget):
        new_pipeline_window = gtk.Window()
        new_pipeline_window.connect("delete_event", self.close_window)
        new_pipeline_window.set_title('New Pipeline')
        new_pipeline_window.set_border_width(10)
        new_pipeline_window.set_resizable(False)
        new_pipeline_window.set_deletable(False)

        vbox = gtk.VBox(True, 6)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Name:  ")
        label.set_alignment(1, 0.5)
        label.set_size_request(100, 30)
        hbox.pack_start(label, False, False, 0)
        name_entry = gtk.Entry()
        name_entry.set_size_request(200, 24)
        hbox.pack_start(name_entry, False, True, 0)

        vbox.pack_start(hbox, False, False, 0)

        hbox = gtk.HBox(False, 0)

        label = gtk.Label("Description:  ")
        label.set_alignment(1, 0.5)
        label.set_size_request(100, 30)
        hbox.pack_start(label, False, False, 0)
        desc_entry = gtk.Entry()
        desc_entry.set_size_request(200, 24)
        hbox.pack_start(desc_entry, False, True, 0)

        vbox.pack_start(hbox, False, False, 0)

        hbox = gtk.HBox(False, 0)

        cancel = gtk.Button("     Cancel     ")
        cancel.connect("clicked", self.on_cancel_clicked, new_pipeline_window)

        create = gtk.Button("     Create     ")
        create.connect("clicked", self.on_create_clicked, [name_entry, desc_entry, new_pipeline_window])

        hbox.pack_end(create, False, True, 2)
        hbox.pack_end(cancel, False, True, 2)

        vbox.pack_start(hbox, False, False, 0)

        new_pipeline_window.add(vbox)
        new_pipeline_window.show_all()

    def on_cancel_clicked(self, widget, window):
        window.hide()

    def on_create_clicked(self, widget, data):
        """Create Pipeline
        
        Keyword arguments:
        widget -- gtk.Button object
        data -- 3 argument array
        
        data[0] - gtk.Entry for name field
        data[1] - gtk.Entry for desc field
        data[2] - gtk.Window for new pipeline window
        
        Create pipeline object and write to pipelines database.
        """
        date_created = str(datetime.datetime.fromtimestamp(time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        last_edited = date_created
        
        name = data[0].get_text()
        desc = data[1].get_text()
        self.unique_name = True
        self.treestore.foreach(self.is_name_unique, name)
        md = Dialog()
        if len(name) > 0:
            # only alphabetic characters allowed for first character
            if name[0].isalpha():
                if self.unique_name:
                    if self.treestore.iter_n_children(None) == 0:
                        next_index = 0        
                    else:
                        next_index = int(self.treestore.get_value(self.treestore.iter_nth_child(None, self.treestore.iter_n_children(None) - 1), 0)) + 1
                    pipeline = Pipeline(next_index, self.CURRENT_PROJECT.name, name, desc, self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.select_data_treestore)
                    self.treestore.append(None, [next_index, name, desc, last_edited, util_user.get_username(), date_created, pipeline.filepath])
                    pipes = DB(self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.name, "pipelines")
                    if len(pipes.search_for_pipeline(next_index)) == 0:
                        pipes.pipeline_entry(next_index, name, desc, last_edited, util_user.get_username(), date_created, pipeline.filepath)
                    else:
                        pipes.update_pipeline(name, 
                            desc, 
                            last_edited,
                            util_user.get_username(),
                            date_created, 
                            pipeline.filepath)
                    self.CURRENT_PROJECT.save_project()
                    data[2].destroy()
                else:
                    md.on_warn("Project name must be unique!")
            else:
                md.on_warn("Pipeline name must start with an alphabetic character!")

    def is_name_unique(self, model, path, iterr, name):
        if name == model.get_value(iterr, 1):
            self.unique_name = False
            self.name_count += 1   
    
    def on_DELETE_PIPELINE_clicked(self, widget, data=None):
        """ Delete Pipeline Toolbar Button Clicked
        
        Keyword arguments:
        widget - gtk.ToolButton() [toolbar button that was clicked]
        
        Delete datasets associated with each module of the selected pipeline.        
        """
        mod, iterr = self.treeselection.get_selected()
        if iterr is not None:
            iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
            op = Open()
            #pdb.set_trace()
            self.pipeline = op.open_pipeline(self.treestore.get_value(iterr, 6))
            # display confimation dialog
            self.on_confirmation("Delete all intermediary files associated with pipeline modules?\n")
            if self.response == gtk.RESPONSE_ACCEPT and self.pipeline.module_index > 0:
                self.on_confirmation("All processed files excluding the original dataset will be lost! Proceed?\n")
                if self.response == gtk.RESPONSE_ACCEPT:
                    # read from DB all modules in selected pipeline
                    modules = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
                    pipe_data = modules.read()
                    self.no_of_files = 0
                    for stage in pipe_data:
                        module = op.open_module(stage[6])
                        if module.dataset is not None and module.stage != 1 and module.deletable:
                            self.no_of_files += len(module.dataset)
                    if not self.pipeline.is_current_dataset_in_use:
                        self.no_of_files += len(self.pipeline.current_dataset)
                    self.fraction = 0
                    self.moving_avg = []
                    # iterate through each module
                    self.__delete_files_window(self.pipeline)
                    for stage in pipe_data:
                        module = op.open_module(stage[6])
                        if module.dataset is not None and module.stage != 1 and module.deletable:
                            gobject.idle_add(self.__delete_files(module.dataset, False).next)  
                    if not self.pipeline.is_current_dataset_in_use:
                        gobject.idle_add(self.__delete_files(self.pipeline.current_dataset, True).next)
                    else:
                        self.dereference_db()  
                        self.df_window.destroy()                      
                    self.CURRENT_PROJECT.save_project()
            else:
                self.dereference_db()
        else:
            md = Dialog()
            md.on_warn("Please select a pipeline.")
                  
    def dereference_db(self):
        # remove modules db reference
        mods = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
        db = mods.read()
        for data in db:
            Id = data[0]
            mods.delete_module_entry(Id)
        # remove pipeline db reference
        all(['rm', '-r', self.pipeline.path])
        pipes = DB(self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.name, "pipelines")
        mod, iterr = self.treeselection.get_selected()
        iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
        pipes.delete_pipeline_entry(self.treestore.get_value(iterr, 0))
        self.treestore.remove(iterr)
        md = Dialog()
        md.on_info("Pipeline deletion successful!")
        self.CURRENT_PROJECT.save_project()   
    
    def on_confirmation(self, message):
        label = gtk.Label(message)
        self.dialog = gtk.Dialog("Confirm",
                                 None,
                                 gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                                 (gtk.STOCK_NO, gtk.RESPONSE_REJECT,
                                  gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        self.dialog.vbox.pack_start(label)
        self.dialog.set_border_width(10)
        label.show()
        self.response = self.dialog.run()
        self.dialog.destroy()
    
    def __update_pbar(self, filename, index):
        self.progress_bar.set_text("Deleting " + filename + "...")
        elapsed = time.time() - self.timer_start
        self.moving_avg.append(elapsed)
        elapsed = sum(self.moving_avg) / len(self.moving_avg)
        #pdb.set_trace()
        remaining = int(elapsed * (self.no_of_files - index))
        if remaining > 3600:
            hours = int(remaining / 3600)
            minutes = int((remaining - (hours * 3600))/60)
            seconds = int(remaining - (hours * 3600) - (minutes * 60))
            self.tr_label.set_text("Time remaining: {0:d} hours {1:d} minutes {2:d} seconds".format(hours, minutes, seconds))       
        elif remaining > 60:
            minutes = int(remaining / 60)
            seconds = int(remaining - (60 * minutes))
            self.tr_label.set_text("Time remaining: {0:d} minutes {1:d} seconds".format(minutes, seconds))
        else:
            seconds = remaining
            self.tr_label.set_text("Time remaining: {0:d} seconds".format(seconds))  
        self.fraction += 1/float(self.no_of_files)
        if self.fraction > 1:
            self.fraction = 1
        self.progress_bar.set_fraction(self.fraction)
    
    def __delete_files_window(self, pipeline):
        # Create a new window
        self.df_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.df_window.connect("delete_event", self.__close_subwindow, self.df_window)
        self.df_window.set_title("Deleting files from " + pipeline.name + "...")
        self.df_window.set_geometry_hints(self.df_window,
            min_width=560,
            min_height=100,
            max_width=400,
            max_height=-1,
            base_width=-1,
            base_height=-1,
            width_inc=-1,
            height_inc=-1,
            min_aspect=-1.0,
            max_aspect=-1.0)
        self.df_window.set_border_width(10)
        self.df_window.set_icon_from_file("img/epitome_icon.png")

        vbox = gtk.VBox(False, 1)
        self.tr_label = gtk.Label("Time Remaining: calculating...")
        self.tr_label.set_alignment(xalign=0, yalign=0.5)
        vbox.pack_start(self.tr_label, False, False, 1)
        self.progress_bar = gtk.ProgressBar()
        self.progress_bar.set_fraction(0.0)
        self.progress_bar.set_orientation(gtk.PROGRESS_LEFT_TO_RIGHT)
        self.progress_bar.set_text("Initializing module...")
        vbox.pack_start(self.progress_bar, True, True, 10)
             
        #buttons = gtk.HBox(False, 1)
        #cancel = gtk.Button("            Cancel            ")
        #cancel.connect("clicked", self.__close_subwindow, self.df_window)
        #cancel.set_alignment(xalign=1, yalign=0.5)
        #buttons.pack_end(cancel, False, False, 2)
        #vbox.pack_start(buttons, False, False, 2)
        self.df_window.add(vbox)
        self.df_window.show_all()
    
    def __delete_files(self, dataset, last):
        for index, datafile in enumerate(dataset):
            self.timer_start = time.time()
            call(['rm', datafile])
            self.__update_pbar(datafile, index)
            yield True
        if last:
            # remove modules db reference
            mods = DB(self.pipeline.project_path, self.pipeline.project, self.pipeline.name)
            db = mods.read()
            for data in db:
                Id = data[0]
                mods.delete_module_entry(Id)
            # remove pipeline db reference
            call(['rm', '-r', self.pipeline.path])
            pipes = DB(self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.name, "pipelines")
            mod, iterr = self.treeselection.get_selected()
            iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
            pipes.delete_pipeline_entry(self.treestore.get_value(iterr, 0))
            self.treestore.remove(iterr)
            md = Dialog()
            md.on_info("Project deletion successful!")
            self.df_window.destroy()    
        yield False
                      
    
    def on_EDIT_PIPELINE_clicked(self, widget, data=None):
        model, iterr = self.treeselection.get_selected()
        iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
        if iterr is not None:
            filepath = self.treestore.get_value(iterr, 6)
            op = Open()
            pipeline = op.open_pipeline(filepath)
            edit = PipelineEditor(pipeline)
        else:
            md = Dialog()
            md.on_warn("Please select a pipeline.")
    
    def select_data(self, widget, data=None):
        model, iterr = self.treeselection.get_selected()
        if iterr is not None:
            iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
            filepath = self.treestore.get_value(iterr, 6)
            op = Open()
            self.CURRENT_PIPELINE = op.open_pipeline(filepath)
            self.select_data_window = gtk.Window()
            self.select_data_window.connect(
                "delete_event", self.close_select_data_window)
            self.select_data_window.set_title("Select Data")
            self.select_data_window.set_position(gtk.WIN_POS_CENTER)
            self.select_data_window.set_geometry_hints(self.select_data_window,
                                                       min_width=360,
                                                       min_height=500,
                                                       max_width=-1,
                                                       max_height=-1,
                                                       base_width=-1,
                                                       base_height=-1,
                                                       width_inc=-1,
                                                       height_inc=-1,
                                                       min_aspect=-1.0,
                                                       max_aspect=-1.0)
            self.select_data_window.set_border_width(3)
            self.select_data_window.set_icon_from_file("img/epitome_icon.png")

            self.select_data_vbox = gtk.VBox(False, 1)

            self.swH = gtk.ScrolledWindow()
            self.swH.set_shadow_type(gtk.SHADOW_ETCHED_IN)
            self.swH.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

            self.select_data_treeview = gtk.TreeView(self.CURRENT_PIPELINE.select_data_treestore)
            self.select_data_treeview.set_enable_tree_lines(True)

            self.column0 = gtk.TreeViewColumn('Project Hierarchy')

            self.cell0 = gtk.CellRendererText()
            self.column0.pack_start(self.cell0, True)
            self.column0.add_attribute(self.cell0, 'text', 0)
            self.column0.set_min_width(260)

            self.select_data_treeview.append_column(self.column0)

            self.cell3 = gtk.CellRendererToggle()
            self.cell3.set_radio(False)
            self.cell3.set_activatable(True)
            self.cell3.set_active(True)
            self.cell3.set_sensitive(True)
            self.cell3.set_visible(True)
            self.cell3.connect('toggled', self.toggled)

            self.column3 = gtk.TreeViewColumn('Selection', self.cell3)
            self.column3.set_fixed_width(6)
            self.column3.add_attribute(self.cell3, 'active', 3)
            self.column3.add_attribute(self.cell3, 'visible', 4)
            self.select_data_treeview.append_column(self.column3)

            self.swH.add(self.select_data_treeview)

            self.buttons = gtk.HBox(False, 1)
            self.cancel = gtk.Button("   Cancel   ")
            self.cancel.connect("clicked", self.sd_cancel_button_clicked)
            self.cancel.set_alignment(xalign=1, yalign=0.5)
            self.apply = gtk.Button("   Apply   ")
            self.apply.connect("clicked", self.sd_apply_button_clicked)
            self.apply.set_alignment(xalign=1, yalign=0.5)

            self.buttons.pack_end(self.apply, False, False, 2)
            self.buttons.pack_end(self.cancel, False, False, 2)

            self.select_data_vbox.pack_start(self.swH, True, True, 0)
            self.select_data_vbox.pack_start(self.buttons, False, False, 4)

            self.select_data_window.add(self.select_data_vbox)

            self.select_data_treeview.expand_all()

            self.select_data_window.show_all()
        else:
            md = Dialog()
            md.on_warn("Please select a pipeline.")

    def close_select_data_window(self, widget, data=None):
        self.select_data_window.destroy()

    def sd_apply_button_clicked(self, widget, data=None):
        self.CURRENT_PIPELINE.save()
        self.select_data_window.destroy()
        md = Dialog()
        md.on_info("Dataset of " + self.CURRENT_PIPELINE.name + " pipeline successfully updated.")

    def sd_cancel_button_clicked(self, widget, data=None):
        self.select_data_window.destroy()

    def get_children(self, root_iter):
        children = []
        for i in range(0, self.treestore.iter_n_children(root_iter)):
            children.append(self.treestore.iter_nth_child(root_iter, i))
        return children

    def toggled(self, widget, path, model=None):
        toggled_iter = self.CURRENT_PIPELINE.select_data_treestore.get_iter(path)
        iter_check = not self.CURRENT_PIPELINE.select_data_treestore.get_value(
            toggled_iter, 3)
        self.CURRENT_PIPELINE.select_data_treestore.set(toggled_iter, 3, iter_check)
        # path passed as non-standard format i.e., ('0':'0':...)
        # convert to standard path format => (0:0:...)
        path = tuple(path.replace(":", ""))
        tup = ()
        for char in path:
            tup += (int(char),)
        data = [iter_check, tup]
        # check tree position to determine if DFS required to toggle
        # children/parents
        if len(tup) < 5:
            self.CURRENT_PIPELINE.select_data_treestore.foreach(self.toggle_children, data)
        else:
            self.CURRENT_PIPELINE.select_data_treestore.foreach(self.toggle_parents, data)

    def toggle_children(self, model, path, iter, data):
        toggled_iter = data[0]
        ref_path = data[1]
        len_ref_path = len(ref_path)
        if len(path) > len_ref_path:
            if cmp(path[0:len_ref_path], ref_path) == 0:
                self.CURRENT_PIPELINE.select_data_treestore.set(iter, 3, toggled_iter)

    def toggle_parents(self, model, path, iter, data):
        toggled_iter = data[0]
        ref_path = data[1]
        len_ref_path = len(ref_path)
        if len(path) < len_ref_path:
            if cmp(path, ref_path[0:len(path)]) == 0:
                self.CURRENT_PIPELINE.select_data_treestore.set(iter, 3, toggled_iter)

    def duplicate_pipeline(self, widget, data=None):
        """ Duplicate Pipeline
        
        Copy pipeline, leave in current state so processing can continue where it was left off.
        """
        model, iterr = self.treeselection.get_selected()
        md = Dialog()
        if iterr is not None:
            iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
            filepath = self.treestore.get_value(iterr, 6)
            op = Open()
            pipeline = op.open_pipeline(filepath)
            orig_pipe = pipeline
            walk_path = pipeline.path
            self.unique_name = True
            self.name_count = 0
            self.treestore.foreach(self.is_name_in, pipeline.name)
            if not self.unique_name:
                export_name = pipeline.name + "_copy" + str(self.name_count)
            else:
                export_name = pipeline.name + "_copy"   
            new_path = self.CURRENT_PROJECT.path + "/pipelines/" + export_name + "/"
            try:
                call(['mkdir', '-p', new_path])
                last_edited = str(datetime.datetime.fromtimestamp(time.time()).strftime('%b-%d-%Y %H:%M:%S'))
                pipes = DB(self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.name, "pipelines")
                for (dirpath, dirnames, filenames) in os.walk(walk_path):
                    for filename in filenames:
                        if filename.endswith('.pipe'):
                            #pdb.set_trace()
                            call(['cp', '-T', pipeline.path + filename, new_path + export_name + ".pipe"])
                            pipeline = op.open_pipeline(new_path + export_name + ".pipe")
                            pipeline.name = export_name
                            pipeline.select_data_treestore = self.CURRENT_PROJECT.select_data_treestore
                            pipeline.project = self.CURRENT_PROJECT.name
                            pipeline.project_path = self.CURRENT_PROJECT.path
                            pipeline.path = self.CURRENT_PROJECT.path + "/pipelines/" + pipeline.name + "/"
                            pipeline.filepath = self.CURRENT_PROJECT.path + "/pipelines/" + pipeline.name + ".pipe"
                            pipeline.duplicated = False
                            pipeline.is_current_dataset_in_use = True
                            if self.treestore.iter_n_children(None) == 0:
                                next_index = 0        
                            else:
                                next_index = int(self.treestore.get_value(self.treestore.iter_nth_child(None, self.treestore.iter_n_children(None) - 1), 0)) + 1
                            pipeline.Id = next_index
                            self.treestore.append(None, [next_index, 
                                pipeline.name, 
                                pipeline.desc, 
                                last_edited, 
                                util_user.get_username(), 
                                pipeline.date_created, 
                                pipeline.filepath])
                            pipes.pipeline_entry(next_index, 
                                pipeline.name, 
                                pipeline.desc, 
                                util_user.get_username(), 
                                pipeline.date_created, 
                                pipeline.filepath)
                            pipeline.save()
                        
                        elif filename.endswith('.mod'):
                            #pdb.set_trace()
                            module = op.open_module(walk_path + filename)
                            if module.state > 0:
                                module.deletable = False
                            mods = DB(orig_pipe.project_path, orig_pipe.project, orig_pipe.name)
                            # update DB
                            mods.update_module(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename)           
                pipes.close()
                pipeline = op.open_pipeline(filepath)
                for (dirpath, dirnames, filenames) in os.walk(walk_path):
                    for filename in filenames:            
                        if filename.endswith('.mod'):
                            call(['cp', pipeline.path + filename, new_path])
                            module = op.open_module(new_path + filename)
                            module.filename = pipeline.path + module.name + '.mod'
                            if module.state > 0:
                                module.deletable = False
                            mods = DB(pipeline.project_path, pipeline.project, export_name)
                            # update DB
                            mods.module_entry(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename)
                            module.save()
                pipeline.duplicated = True
                pipeline.save()
                self.CURRENT_PROJECT.save_project() 
                md.on_info(pipeline.name + " duplicated successfully!")
            except IOError:
                md.on_warn("An error occured.")
            except sqlite3.Error as e:
                md.on_warn("A database error occured.\n\nError Message: " + str(e))
        else:
            md.on_warn("Please select a pipeline.")
    
    def is_name_in(self, model, path, iterr, name):
        if name in model.get_value(iterr, 1):
            self.unique_name = False
            self.name_count += 1 
    
    def export_pipeline(self, widget, data=None):
        """ Export Pipeline
         
        Package pipeline for export, reset appropiate attributes in prep for future import.
        """
        model, iterr = self.treeselection.get_selected()
        md = Dialog()
        if iterr is not None:
            iterr = self.sorted_treestore.convert_iter_to_child_iter(None, iterr)
            filepath = self.treestore.get_value(iterr, 6)
            op = Open()
            pipeline = op.open_module(filepath)
            dialog = gtk.FileChooserDialog("Export Pipeline",
                                       None, gtk.FILE_CHOOSER_ACTION_SAVE,
                                       (gtk.STOCK_CANCEL,
                                        gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_SAVE,
                                        gtk.RESPONSE_OK))
            dialog.set_default_response(gtk.RESPONSE_OK)
            dialog.set_current_name(pipeline.name + "_ex")
            response = dialog.run()
            if response == gtk.RESPONSE_OK:
                export_path = dialog.get_filename()
                export_name = export_path.split("/")
                export_name = export_name[len(export_name) - 1]
                temp_path = self.CURRENT_PROJECT.path + "/bin/tmp/" + export_name + "/"
                call(['mkdir', '-p', temp_path])
                for (dirpath, dirnames, filenames) in os.walk(pipeline.path):
                    cmd = ['tar', 'cf', export_path + ".pipe.z", '-C', temp_path]
                    for filename in filenames:
                        if filename.endswith('.pipe'):
                            call(['cp', '-T', pipeline.path + filename, temp_path + export_name + '.pipe'])
                            #copied_pipeline = op.open_pipeline(temp_path + export_name + '.pipe')
                            cmd.append(export_name + '.pipe')
                        elif filename.endswith('.mod'):
                            call(['cp', pipeline.path + filename, temp_path])
                            cmd.append(filename)
                call(cmd)
                dialog.destroy()
                md.on_info(pipeline.name + " exported successfully!")
            else:
                dialog.destroy()      
        else:
            md.on_warn("Please select a pipeline to export.")
            
    def import_pipeline(self, widget, data=None):
        """ Import Pipeline
        
        Import pipeline for use in current project.
        """
        dialog = gtk.FileChooserDialog("Import Pipeline",
                                       None, gtk.FILE_CHOOSER_ACTION_OPEN,
                                       (gtk.STOCK_CANCEL,
                                        gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN,
                                        gtk.RESPONSE_OK))
        dialog.set_default_response(gtk.RESPONSE_OK)
        response = dialog.run()
        md = Dialog()
        op = Open()
        if response == gtk.RESPONSE_OK:
            filename = dialog.get_filename()
            pipeline_name = filename.split("/")
            pipeline_name = pipeline_name[len(pipeline_name) - 1].split(".")
            pipeline_name = pipeline_name[0]
            self.unique_name = True
            self.treestore.foreach(self.is_name_unique, pipeline_name)
            if not self.unique_name:
                dialog.destroy()
                string = ("Pipeline names must be unique. Please change the "
                         "name(s) of existing project pipelines which match the "
                         "name of the pipeline you are trying to import.")
                md.on_warn(string)
            # extract project
            else:
                try:
                    path = self.CURRENT_PROJECT.path + "/pipelines/" + pipeline_name + "/"
                    call(['mkdir', '-p', path])
                    call(['tar', '-xf', filename, '-C', path])
                    last_edited = str(datetime.datetime.fromtimestamp(time.time()).strftime('%b-%d-%Y %H:%M:%S'))
                    for (dirpath, dirnames, filenames) in os.walk(path):
                        pipes = DB(self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.name, "pipelines")
                        for filename in filenames:
                            if filename.endswith('.pipe'):
                                pipeline = op.open_pipeline(path + filename)
                                name = filename.split(".")
                                # reset attributes
                                pipeline.name = name[0]
                                if not self.unique_name:
                                    pipeline.name += "_copy"
                                pipeline.select_data_treestore = self.CURRENT_PROJECT.select_data_treestore
                                pipeline.current = None
                                pipeline.project = self.CURRENT_PROJECT.name
                                pipeline.project_path = self.CURRENT_PROJECT.path
                                pipeline.path = self.CURRENT_PROJECT.path + "/pipelines/" + pipeline.name + "/"
                                pipeline.filepath = self.CURRENT_PROJECT.path + "/pipelines/" + pipeline.name + ".pipe"
                                pipeline.module_index = 0
                                if self.treestore.iter_n_children(None) == 0:
                                    next_index = 0        
                                else:
                                    next_index = int(self.treestore.get_value(self.treestore.iter_nth_child(None, self.treestore.iter_n_children(None) - 1), 0)) + 1
                                pipeline.Id = next_index
                                self.treestore.append(None, [next_index, 
                                    pipeline.name, 
                                    pipeline.desc, 
                                    last_edited, 
                                    util_user.get_username(),
                                    pipeline.date_created, 
                                    pipeline.filepath])
                                pipes.pipeline_entry(next_index, 
                                    pipeline.name, 
                                    pipeline.desc,
                                    util_user.get_username(),
                                    pipeline.date_created, 
                                    pipeline.filepath)
                                pipeline.save()
                        pipes.close()                                             
                    for (dirpath, dirnames, filenames) in os.walk(path):
                        for filename in filenames:
                            if filename.endswith('.mod'):
                                module = op.open_module(path + filename)
                                module.filename = pipeline.path + module.name + '.mod'
                                module.state = 0
                                mods = DB(pipeline.project_path, pipeline.project, pipeline.name)
                                # update DB
                                mods.module_entry(module.Id, module.stage, module.state, module.function, module.name, module.suffix, module.filename)
                                module.save()
                    pipeline.save()
                    self.CURRENT_PROJECT.save_project() 
                    dialog.destroy()
                    md.on_info(pipeline_name + " imported successfully!")
                except IOError:
                    dialog.destroy()
                    md.on_warn("An error occured.")
                except sqlite3.Error as e:
                    dialog.destroy()
                    md.on_warn("A database error occured.\n\nError Message: " + str(e))
        elif response == gtk.RESPONSE_CANCEL:
            dialog.destroy()
        
    def populate_treestore(self):
        pipes = DB(self.CURRENT_PROJECT.path, self.CURRENT_PROJECT.name, "pipelines")
        db = pipes.read()
        for index, data in enumerate(db):
            self.treestore.insert(None, index, [data[0], data[1], data[2], data[3], data[4], data[5], data[6]])
        pipes.close()
    
    def __init__(self, project):

        self.CURRENT_PROJECT = project
        self.fre = None
        
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.close_window)
        self.window.set_title(self.CURRENT_PROJECT.name + " - Project Editor")
        self.window.set_size_request(680, 240)
        self.window.set_border_width(0)
        self.window.set_icon_from_file("img/epitome_icon.png")

        main_vbox = gtk.VBox(False, 1)

        # MenuBar
        
        menubar = gtk.MenuBar()

        accelerator_01 = gtk.AccelGroup()
        self.window.add_accel_group(accelerator_01)

        projectmenu = gtk.Menu()

        filep = gtk.MenuItem("_Project")
        filep.set_right_justified(False)
        filep.set_submenu(projectmenu)

        MI__SET_FILENAMING_RULE = gtk.ImageMenuItem(gtk.STOCK_EDIT, accelerator_01)
        MI__SET_FILENAMING_RULE.get_children()[0].set_label('Set Filenaming Rule')

        key, mod = gtk.accelerator_parse("F")

        MI__SET_FILENAMING_RULE.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        MI__SET_FILENAMING_RULE.connect("activate", self.on_SET_FILENAMING_RULE_activated)
        projectmenu.append(MI__SET_FILENAMING_RULE)
        
        MI__PREFERENCES = gtk.ImageMenuItem(gtk.STOCK_PREFERENCES, accelerator_01)
        MI__PREFERENCES.get_children()[0].set_label('Preferences')

        key, mod = gtk.accelerator_parse("P")

        MI__PREFERENCES.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        MI__PREFERENCES.connect("activate", self.on_PREFERENCES_activated)
        projectmenu.append(MI__PREFERENCES)

        pipelinemenu = gtk.Menu()
        
        filepl = gtk.MenuItem("_Pipeline")
        filepl.set_right_justified(False)
        filepl.set_submenu(pipelinemenu)
        
        new_menu_item = gtk.ImageMenuItem(gtk.STOCK_NEW, accelerator_01)
        new_menu_item.get_children()[0].set_label('New')
        key, mod = gtk.accelerator_parse("<Control>N")
        
        new_menu_item.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        new_menu_item.connect("activate", self.on_NEW_PIPELINE_clicked)
        pipelinemenu.append(new_menu_item)
        
        edit_menu_item = gtk.ImageMenuItem(gtk.STOCK_EDIT, accelerator_01)
        edit_menu_item.get_children()[0].set_label('Open in Pipeline Editor')
        key, mod = gtk.accelerator_parse("<Control>E")
        
        edit_menu_item.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        edit_menu_item.connect("activate", self.on_EDIT_PIPELINE_clicked)
        pipelinemenu.append(edit_menu_item)
        
        duplicate_menu_item = gtk.ImageMenuItem(gtk.STOCK_COPY, accelerator_01)
        duplicate_menu_item.get_children()[0].set_label('Duplicate')
        key, mod = gtk.accelerator_parse("<Control>C")
        
        duplicate_menu_item.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        duplicate_menu_item.connect("activate", self.duplicate_pipeline)
        pipelinemenu.append(duplicate_menu_item)
        
        delete_menu_item = gtk.ImageMenuItem(gtk.STOCK_DELETE, accelerator_01)
        delete_menu_item.get_children()[0].set_label('Delete')
        key, mod = gtk.accelerator_parse("<Control>D")
        
        delete_menu_item.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        delete_menu_item.connect("activate", self.on_DELETE_PIPELINE_clicked)
        pipelinemenu.append(delete_menu_item)
        
        sep = gtk.SeparatorMenuItem()
        pipelinemenu.append(sep)
        
        import_menu_item = gtk.ImageMenuItem(gtk.STOCK_GO_DOWN, accelerator_01)
        import_menu_item.get_children()[0].set_label('Import')

        key, mod = gtk.accelerator_parse("I")

        import_menu_item.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        import_menu_item.connect("activate", self.import_pipeline)
        pipelinemenu.append(import_menu_item)

        export = gtk.ImageMenuItem(gtk.STOCK_GO_UP, accelerator_01)
        export.get_children()[0].set_label('Export')

        key, mod = gtk.accelerator_parse("E")

        export.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        export.connect("activate", self.export_pipeline)
        pipelinemenu.append(export)
        
        helpmenu = gtk.Menu()

        fileh = gtk.MenuItem("_Help")
        fileh.set_right_justified(True)
        fileh.set_submenu(helpmenu)

        MI__ABOUT = gtk.ImageMenuItem(gtk.STOCK_ABOUT, accelerator_01)
        MI__ABOUT.get_children()[0].set_label('About')

        key, mod = gtk.accelerator_parse("<Control>A")

        MI__ABOUT.add_accelerator(
            "activate", accelerator_01, key, mod, gtk.ACCEL_VISIBLE)
        MI__ABOUT.connect("activate", self.on_ABOUT_activate)
        helpmenu.append(MI__ABOUT)

        menubar.append(filep)
        menubar.append(filepl)
        menubar.append(fileh)

        filemenubox = gtk.VBox(False, 2)
        filemenubox.pack_start(menubar, True, True, 0)

        self.swH = gtk.ScrolledWindow()
        self.swH.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.treestore = gtk.TreeStore(int, str, str, str, str, str, str)
        
        self.sorted_treestore = gtk.TreeModelSort(self.treestore)
        self.sorted_treestore.set_sort_column_id(3, gtk.SORT_DESCENDING)
        
        self.populate_treestore()
        self.treeview = gtk.TreeView(self.sorted_treestore)

        #id_column = gtk.TreeViewColumn('Id')
        name_column = gtk.TreeViewColumn('Pipeline')
        name_column.set_min_width(100)
        description_column = gtk.TreeViewColumn('Description')
        description_column.set_min_width(160)
        last_edited = gtk.TreeViewColumn('Last Edited')
        last_user = gtk.TreeViewColumn('By')
        last_user.set_min_width(100)
        date_created = gtk.TreeViewColumn('Date Created')

        #self.treeview.append_column(id_column)
        self.treeview.append_column(name_column)
        self.treeview.append_column(description_column)
        self.treeview.append_column(last_edited)
        self.treeview.append_column(last_user)
        self.treeview.append_column(date_created)

        #cell0 = gtk.CellRendererText()
        cell1 = gtk.CellRendererText()
        cell2 = gtk.CellRendererText()
        cell3 = gtk.CellRendererText()
        cell4 = gtk.CellRendererText()
        cell5 = gtk.CellRendererText()

        #id_column.pack_start(cell0, False)
        name_column.pack_start(cell1, False)
        description_column.pack_start(cell2, False)
        last_edited.pack_start(cell3, False)
        last_user.pack_start(cell5, False)
        date_created.pack_start(cell4, False)

        #id_column.add_attribute(cell0, 'text', 0)
        name_column.add_attribute(cell1, 'text', 1)
        description_column.add_attribute(cell2, 'text', 2)
        last_edited.add_attribute(cell3, 'text', 3)
        last_user.add_attribute(cell5, 'text', 4)
        date_created.add_attribute(cell4, 'text', 5)
        
        name_column.set_sort_column_id(1)
        description_column.set_sort_column_id(2)
        last_edited.set_sort_column_id(3)
        last_user.set_sort_column_id(4)
        date_created.set_sort_column_id(5)

        self.treeselection = self.treeview.get_selection()
        self.treeselection.set_mode(gtk.SELECTION_SINGLE)
 
        # Toolbar

        toolbar = gtk.Toolbar()
        toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
        toolbar.set_style(gtk.TOOLBAR_ICONS)

        new = gtk.ToolButton(gtk.STOCK_NEW)
        new.connect("clicked", self.on_NEW_PIPELINE_clicked)
        new.set_tooltip_markup('<b>New: </b>Create a new'
            + ' pipeline.')
        toolbar.insert(new, 0)

        edit = gtk.ToolButton(gtk.STOCK_EDIT)
        edit.connect("clicked", self.on_EDIT_PIPELINE_clicked)
        edit.set_tooltip_markup('<b>Edit: </b>Open'
            + ' pipeline in Pipeline Editor.')
        toolbar.insert(edit, 1)
        
        duplicate = gtk.ToolButton(gtk.STOCK_COPY)
        duplicate.connect("clicked", self.duplicate_pipeline)
        duplicate.set_tooltip_markup('<b>Duplicate: </b>Copy'
            + ' pipeline in its current state.')
        toolbar.insert(duplicate, 2)
        
        delete = gtk.ToolButton(gtk.STOCK_DELETE)
        delete.connect("clicked", self.on_DELETE_PIPELINE_clicked)
        delete.set_tooltip_markup('<b>Delete: </b>Delete pipeline and any associated processed files.')
        toolbar.insert(delete, 3)

        space = gtk.SeparatorToolItem()
        space.set_expand(True)
        space.set_draw(False)
        toolbar.insert(space, 4)

        select_data = gtk.ToolButton(gtk.STOCK_INDEX)
        select_data.connect("clicked", self.select_data)
        select_data.set_tooltip_markup('<b>Select Data: </b> Assign'
            + ' dataset to pipeline.')
        toolbar.insert(select_data, 5)
        
        # Popup menu

        popup_menu = gtk.Menu()

        new = gtk.MenuItem("New Pipeline")
        new.connect("activate", self.on_NEW_PIPELINE_clicked)
        new.show()

        edit = gtk.MenuItem("Open in Pipeline Editor...")
        edit.connect("activate", self.on_EDIT_PIPELINE_clicked)
        edit.show()

        select = gtk.MenuItem("Select Data")
        select.connect(
            "activate", self.select_data)
        select.show()
        
        edit_meta = gtk.MenuItem("Edit Meta...")
        edit_meta.connect("activate", self.on_EDIT_PIPELINE_clicked)
        edit_meta.show()

        delete = gtk.MenuItem("Delete")
        delete.connect(
            "activate", self.on_DELETE_PIPELINE_clicked)
        delete.show()

        popup_menu.append(new)
        popup_menu.append(select)
        popup_menu.append(edit)
        popup_menu.append(delete)

        self.treeview.connect("button-press-event",
                              self.menu_activated, popup_menu)

        self.swH.add(self.treeview)
        main_vbox.pack_start(filemenubox, False, True, 0)
        main_vbox.pack_start(toolbar, False, True, 0)
        main_vbox.pack_start(self.swH, True, True, 0)
        gtk.STOCK_ABOUT
        self.window.add(main_vbox)

        self.window.show_all()

    def menu_activated(self, widget, event, menu):
        # right click
        if event.button == 3:
            menu.popup(None, None, None, event.button,
                       event.get_time(), data=None)
            pass

