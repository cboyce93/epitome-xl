import sqlite3
import subprocess
import time
import datetime
import pwd
import os
from os.path import expanduser


class Projects:

    def __init__(self):
        self.conn = sqlite3.connect("/usr/local/epitome-xl/src/bin/projects.db")
        self.c = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.c.execute("""CREATE TABLE IF NOT EXISTS projects (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL,
                                    root text NOT NULL,
                                    edited text NOT NULL,
                                    lastuser text NOT NULL,
                                    created text NOT NULL,
                                    path text
                                )""")

    def entry(self, Id, name, root, lastuser, created, path):
        edited = str(datetime.datetime.fromtimestamp(
            time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        self.c.execute("INSERT INTO projects(Id, name, root, edited, lastuser, created, path) VALUES (?, ?, ?, ?, ?, ?, ?)",
                       (Id, name, root, edited, lastuser, created, path))
        self.conn.commit()

    def search(self, Id):
        self.c.execute("SELECT * FROM projects WHERE id IN ('" + str(Id) + "')")
        return self.c.fetchall()
    
    def update(self, Id, name, root, lastuser, created, path):
        edited = str(datetime.datetime.fromtimestamp(
            time.time()).strftime('%b-%d-%Y %H:%M:%S'))
        lastuser = self.get_username()
        self.c.execute("UPDATE projects SET edited=? WHERE Id=?", (edited, Id))
        self.c.execute("UPDATE projects SET lastuser=? WHERE Id=?", (lastuser, Id))
        self.conn.commit()
    
    def read(self):
        self.c.execute("SELECT * from projects")
        return self.c.fetchall()

    def close(self):
        self.conn.close()
        
    def get_username(self):
        return pwd.getpwuid(os.getuid())[0]


